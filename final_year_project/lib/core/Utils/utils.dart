import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Utils {
  // Constants constants = Constants();
  static toastMessages(String title) {
    Fluttertoast.showToast(
        msg: title,
        backgroundColor: Colors.black,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white);
  }

  static snackBar(String title, String message) {
    Get.snackbar(title, message,
        duration: Duration(seconds: 3),
        borderRadius: 10,
        backgroundColor: Colors.white,
        colorText: Colors.black);
  }

  static defaultDialogAlert(
      String title, dynamic content, dynamic cancel, dynamic confirm) {
    return Get.defaultDialog(
        title: title,
        onCancel: cancel,
        titleStyle: TextStyle(fontFamily: 'BubblegumSans'),
        onConfirm: confirm,
        actions: content,
        titlePadding: EdgeInsets.all(5),
        contentPadding: EdgeInsets.all(5));
  }

  static easyLoading(String message) {
    return EasyLoading.show(status: message);
  }
}
