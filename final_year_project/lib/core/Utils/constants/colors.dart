import 'package:flutter/material.dart';

class Constants {
  //App Colours
  Color whiteColor = Colors.white;
  // Color darkRedColor = Color(0xFFDB3022);
  // Color darkRedColor = Colors.green;
  Color darkRedColor = Colors.blue.withOpacity(1);
  // Color darkRedColor = Colors.grey;

  var lightGrey = Colors.grey[1];
  Color darkGreenColor = Color.fromRGBO(40, 96, 73, 1.0);
  Color darkGreenColor2 = Color.fromARGB(255, 25, 73, 53);
  Color greyColor = const Color.fromARGB(255, 222, 215, 215);
  Color textFormFieldFilledColor = Color.fromRGBO(242, 243, 241, 1.0);
  Color yellowColor = Colors.yellow;
  Color orangeColor = Colors.deepOrange;
  Color blackColor = Colors.black;
  Color greenColor = Colors.green;
  var lightGreyColor = Colors.grey[700];
  var creamColor = Colors.white70;
  Color lightBlueColor = Colors.blue;
  Color scaffoldBackgroundColor = Color(0xFFF9F9F9);
  //Font Sizes
  double appbarFontSize = 21;
  double simpleFontSize = 18;
  double categoryLabelFontSize = 20;
  double detailsTitleFontSize = 20;
}
