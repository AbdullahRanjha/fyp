// import 'dart:io';

import 'package:final_year_project/core/Utils/utils.dart';
// import 'package:final_year_project/screens/view/favourite_screen.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/get_rx.dart';
import 'package:image_picker/image_picker.dart';

class LogicsController extends GetxController {
  RxBool isOpen = false.obs;

  RxBool isLoading = false.obs;
  RxString isSelectedValue = 'Please Select Catagory'.obs;
  RxList<String> dropdownValues =
      ["Donation", 'Eco-Mart', "Swapping", 'Re-Cycling'].obs;
  RxBool isFavourite = false.obs;

  // List<bool> isFavourite = false as dynamic;

  favouriteItem() {
    isFavourite.value = !isFavourite.value;
    print(isFavourite.value.toString());
  }

  RxString image = ''.obs;
  final picker = ImagePicker().obs;
  getCameraImage() async {
    final pickedImage = await picker.value
        .pickImage(source: ImageSource.camera, imageQuality: 40);
    try {
      if (pickedImage != null) {
        image.value = pickedImage.path.toString();

        Utils.toastMessages('Image Picked');
      }
    } catch (e) {
      Utils.toastMessages('${e.toString()}');
    }
  }

  getGalleryImage() async {
    final pickedImage = await picker.value
        .pickImage(source: ImageSource.gallery, imageQuality: 40);
    try {
      if (pickedImage != null) {
        image.value = pickedImage.path.toString();

        Utils.toastMessages('Image Picked');
      }
    } catch (e) {
      Utils.toastMessages('${e.toString()}');
    }
  }

  void settingValues(int index) {
    isSelectedValue.value = dropdownValues[index].toString();
    isOpen.value = !isOpen.value;
  }

  void dropdownFun() {
    isOpen.value = !isOpen.value;
  }

  var isVissiblePassword = false.obs;
}
