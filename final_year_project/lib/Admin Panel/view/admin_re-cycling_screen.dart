import 'dart:developer';

import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_card/image_card.dart';

import '../../Models/product_model.dart';
import '../../core/Utils/utils.dart';
import '../../screens/view/donation/details_donationScreen.dart';

class AdminRe_cyclingScreen extends StatefulWidget {
  const AdminRe_cyclingScreen({super.key});

  @override
  State<AdminRe_cyclingScreen> createState() => _AdminRe_cyclingScreenState();
}

class _AdminRe_cyclingScreenState extends State<AdminRe_cyclingScreen> {
  Constants constants = Constants();
  // final ref = FirebaseDatabase.instance.ref("All Posts/Re-Cycling");
  final swappingRef = FirebaseFirestore.instance
      .collection('All Posts')
      .where('category', isEqualTo: 'Re-Cycling');
  double height = Get.height;
  double width = Get.width;
  Future deleteProduct(var productId, var userId) async {
    await FirebaseFirestore.instance
        .collection('User')
        .doc(userId)
        .collection('myPosts')
        .doc(productId)
        .delete()
        .then((value) => log('deleted successfully'))
        .onError((error, stackTrace) => null);
    await FirebaseFirestore.instance
        .collection('All Posts')
        .doc(productId)
        .delete()
        .then((value) => Utils.toastMessages('Product deleted'))
        .onError((error, stackTrace) => null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppbar(
          title: 'Re-Cycling',
          automaticallyImplyLeading: true,
          backgroundColor: Constants().darkRedColor,
          textColor: Constants().whiteColor,
        ),
        body: StreamBuilder(
            stream: swappingRef.snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(
                    color: constants.darkRedColor,
                  ),
                );
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }

              var data = snapshot.data!.docs;
              return Container(
                height: height,
                width: width,
                child: Column(
                  children: [
                    Expanded(
                      child: GridView.builder(
                          itemCount: data.length,
                          scrollDirection: Axis.vertical,
                          physics: ScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                 crossAxisSpacing: 5,
                                  mainAxisSpacing: 5,
                                  mainAxisExtent: 190,
                                  crossAxisCount: 2),
                          itemBuilder: (context, index) {
                            ProductModel productModel = ProductModel(
                                image: data[index]['image'],
                                price: data[index]['price'],
                                name: data[index]['name'],
                                rating:
                                    (data[index]['rating'] as num).toDouble(),
                                description: data[index]['description'],
                                productId: data[index]['id'],
                                category: data[index]['category'],
                                userName: data[index]['userName'],
                                userId: data[index]['userId'],
                                email: data[index]['email'],
                                city: data[index]['city'],
                                state: data[index]['state'],
                                country: data[index]['country'],
                                conditionDropDownValue: data[index]
                                    ['condition'],
                                organization: data[index]['organization'],
                                serviceName: data[index]['serviceName'],
                                amTime: data[index]['amTime'],
                                pmTime: data[index]['pmTime']);

                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 4.0),
                              child: Container(
                                width: width * 0.35,
                                child: Padding(
                                  padding: const EdgeInsets.all(0.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Get.to(() => DonationDetailsScreen(
                                                title: productModel.name,
                                                rating: productModel.rating,
                                                price: productModel.price,
                                                category: productModel.category,
                                                image: productModel.image,
                                                description:
                                                    productModel.description,
                                                userName: productModel.userName,
                                                email: productModel.email,
                                                productId:
                                                    productModel.productId,
                                                userId: productModel.userId,
                                                condition: productModel
                                                    .conditionDropDownValue,
                                                country: productModel.country,
                                                city: productModel.city,
                                                state: productModel.state,
                                                organization:
                                                    productModel.organization,
                                                serviceName:
                                                    productModel.serviceName,
                                              ));
                                          // Get.toNamed(RouteName
                                          //     .swappingDetailsScreen);
                                        },
                                        child: Container(
                                          child: FillImageCard(
                                              borderRadius: 10,
                                              contentPadding:
                                                  EdgeInsets.only(left: 5.0),
                                              height: 150,
                                              // footer:
                                              heightImage: 85,
                                              width: width * 0.35,
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Container(
                                                        width: width * 0.25,
                                                        child: Text(
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          productModel.name,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: height * 0.04,
                                                        width: width * 0.04,
                                                        child: PopupMenuButton(
                                                            icon: Icon(
                                                                Icons.more_vert,
                                                                color: Constants()
                                                                    .blackColor,
                                                                size: 15),
                                                            itemBuilder:
                                                                (context) {
                                                              return [
                                                                PopupMenuItem(
                                                                    onTap: () {
                                                                      deleteProduct(
                                                                          productModel
                                                                              .productId
                                                                              .toString(),
                                                                          productModel
                                                                              .userId
                                                                              .toString());
                                                                    },
                                                                    value: 0,
                                                                    child: Text(
                                                                        'Delete'))
                                                              ];
                                                            }),
                                                      )
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 7.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: height * 0.03,
                                                          width: width * 0.15,
                                                          child:
                                                              AnimatedRatingBar(
                                                                  initialRating:
                                                                      productModel
                                                                          .rating
                                                                          .toDouble(),
                                                                  activeFillColor:
                                                                      constants
                                                                          .yellowColor,
                                                                  strokeColor:
                                                                      constants
                                                                          .blackColor,
                                                                  onRatingUpdate:
                                                                      (val) async {
                                                                    final auth =
                                                                        FirebaseAuth
                                                                            .instance
                                                                            .currentUser;
                                                                    final postRef = await FirebaseFirestore
                                                                        .instance
                                                                        .collection(
                                                                            'All Posts')
                                                                        .doc(productModel
                                                                            .productId)
                                                                        .update({
                                                                      'rating':
                                                                          val.toInt()
                                                                    }).then(
                                                                            (value) async {
                                                                      final auth2 =
                                                                          FirebaseFirestore
                                                                              .instance;
                                                                      await auth2
                                                                          .collection(
                                                                              'User')
                                                                          .doc(productModel
                                                                              .userId)
                                                                          .collection(
                                                                              'myPosts')
                                                                          .doc(productModel
                                                                              .productId)
                                                                          .update({
                                                                        'rating':
                                                                            val.toInt()
                                                                      });
                                                                    }).onError((error,
                                                                                stackTrace) =>
                                                                            null);
                                                                  }),
                                                        ),
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            3),
                                                                color: Constants()
                                                                    .darkRedColor),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(1.5),
                                                              child: Text(
                                                                '${productModel.category}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        11,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ))
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              imageProvider: NetworkImage(
                                                  productModel.image)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              );
            }));
  }
}
