
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_donation_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_ecoMart_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_re-cycling_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_swapping_screen.dart';
import 'package:final_year_project/Models/product_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/screens/view/addPost_screen.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/screens/view/donation/details_donationScreen.dart';
import 'package:final_year_project/screens/view/eco-mart/details_eco-martScreen.dart';
// import 'package:final_year_project/screens/view/favourite_screen.dart';
import 'package:final_year_project/screens/view/re-cycling/details_re-cyclingScreen.dart';
import 'package:final_year_project/screens/view/swapping/details_swapping_screen.dart';
import 'package:final_year_project/services/firebase_services/Authentication/logout_services.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/custom_catagoryName.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:final_year_project/screens/view/donation/donation_screen.dart';
import 'package:final_year_project/screens/view/eco-mart/eco-market_screen.dart';
import 'package:final_year_project/screens/view/re-cycling/re-cycling_screen.dart';
import 'package:final_year_project/screens/view/swapping/swapping_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';
import 'package:image_card/image_card.dart';

import 'package:get/get_core/src/get_main.dart';
// import 'package:flutter/services.dart';

class AdminAllPostScreen extends StatefulWidget {
  const AdminAllPostScreen({super.key});

  @override
  State<AdminAllPostScreen> createState() => _AdminAllPostScreenState();
}

class _AdminAllPostScreenState extends State<AdminAllPostScreen> {
  Constants constants = Constants();

  Stream<QuerySnapshot> querySnapshot(String category) {
    return FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: category)
        .snapshots();
  }

  Future deleteProduct(var productId, var userId) async {
    await FirebaseFirestore.instance
        .collection('User')
        .doc(userId)
        .collection('myPosts')
        .doc(productId)
        .delete()
        .then((value) => log('deleted successfully'))
        .onError((error, stackTrace) => null);
    await FirebaseFirestore.instance
        .collection('All Posts')
        .doc(productId)
        .delete()
        .then((value) => Utils.toastMessages('Product deleted'))
        .onError((error, stackTrace) => null);
  }

  // UserCredential? credential;

  LogoutController logoutController = Get.put(LogoutController());
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: 'All Posts',
        automaticallyImplyLeading: true,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: height * 0.15,
                  width: width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage(
                            'assets/drawer_header.jpg',
                          ),
                          fit: BoxFit.cover)),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Swapping',
                      style: TextStyle(
                          fontFamily: 'JosefinSans',
                          fontSize: constants.categoryLabelFontSize,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        onPressed: () {
                          Get.to(() => AdminSwappingScreen());
                        },
                        child: Text('view all'))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: height * 0.23,
                  width: width,
                  child: StreamBuilder(
                      stream: querySnapshot('Swapping'),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                          case ConnectionState.none:
                            return Center(
                              child: CircularProgressIndicator(
                                color: constants.darkRedColor,
                              ),
                            );
                          case ConnectionState.done:
                          case ConnectionState.active:
                            if (!snapshot.hasData) {
                              return Center(
                                child: Text('No item found'),
                              );
                            } else {
                              log('true');
                              return Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    primary: true,
                                    physics: ScrollPhysics(),
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) {
                                      var data = snapshot.data!.docs;
                                      ProductModel productModel = ProductModel(
                                          image: data[index]['image'],
                                          price: data[index]['price'],
                                          name: data[index]['name'],
                                          rating: (data[index]['rating'] as num)
                                              .toDouble(),
                                          description: data[index]
                                              ['description'],
                                          productId: data[index]['id'],
                                          category: data[index]['category'],
                                          userName: data[index]['userName'],
                                          userId: data[index]['userId'],
                                          email: data[index]['email'],
                                          city: data[index]['city'],
                                          state: data[index]['state'],
                                          country: data[index]['country'],
                                          conditionDropDownValue: data[index]
                                              ['condition'],
                                          organization: data[index]
                                              ['organization'],
                                          serviceName: data[index]
                                              ['serviceName'],
                                          amTime: data[index]['amTime'],
                                          pmTime: data[index]['pmTime']);
                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          child: FillImageCard(
                                              borderRadius: 10,
                                              contentPadding:
                                                  EdgeInsets.only(left: 5.0),
                                              height: height * 0.150,
                                              // footer:
                                              heightImage: height * 0.11,
                                              width: width * 0.35,
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Container(
                                                        width: width * 0.25,
                                                        child: Text(
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          productModel.name,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: height * 0.04,
                                                        width: width * 0.04,
                                                        child: PopupMenuButton(
                                                            icon: Icon(
                                                                Icons.more_vert,
                                                                color: Constants()
                                                                    .blackColor,
                                                                size: 15),
                                                            itemBuilder:
                                                                (context) {
                                                              return [
                                                                PopupMenuItem(
                                                                    onTap: () {
                                                                      deleteProduct(
                                                                          productModel
                                                                              .productId
                                                                              .toString(),
                                                                          productModel
                                                                              .userId
                                                                              .toString());
                                                                    },
                                                                    value: 0,
                                                                    child: Text(
                                                                        'Delete'))
                                                              ];
                                                            }),
                                                      )
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 7.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: height * 0.03,
                                                          width: width * 0.15,
                                                          child:
                                                              AnimatedRatingBar(
                                                                  initialRating:
                                                                      productModel
                                                                          .rating
                                                                          .toDouble(),
                                                                  activeFillColor:
                                                                      constants
                                                                          .yellowColor,
                                                                  strokeColor:
                                                                      constants
                                                                          .blackColor,
                                                                  onRatingUpdate:
                                                                      (val) async {
                                                                    final auth =
                                                                        FirebaseAuth
                                                                            .instance
                                                                            .currentUser;
                                                                    final postRef = await FirebaseFirestore
                                                                        .instance
                                                                        .collection(
                                                                            'All Posts')
                                                                        .doc(productModel
                                                                            .productId)
                                                                        .update({
                                                                      'rating':
                                                                          val.toInt()
                                                                    }).then(
                                                                            (value) async {
                                                                      final auth2 =
                                                                          FirebaseFirestore
                                                                              .instance;
                                                                      await auth2
                                                                          .collection(
                                                                              'User')
                                                                          .doc(productModel
                                                                              .userId)
                                                                          .collection(
                                                                              'myPosts')
                                                                          .doc(productModel
                                                                              .productId)
                                                                          .update({
                                                                        'rating':
                                                                            val.toInt()
                                                                      });
                                                                    }).onError((error,
                                                                                stackTrace) =>
                                                                            null);
                                                                  }),
                                                        ),
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            3),
                                                                color: Constants()
                                                                    .darkRedColor),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(1.5),
                                                              child: Text(
                                                                '${productModel.category}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        11,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ))
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              imageProvider: NetworkImage(
                                                  productModel.image)),
                                        ),
                                      );
                                    }),
                              );
                            }

                          // break;
                          default:
                        }
                        return Container();
                      }),
                )

                // //For Swapping Containers Code
                ,
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Re-Cycling',
                      style: TextStyle(
                          fontFamily: 'JosefinSans',
                          fontSize: constants.categoryLabelFontSize,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        onPressed: () {
                          Get.to(() => AdminRe_cyclingScreen());
                        },
                        child: Text('view all'))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: height * 0.22,
                  width: width,
                  child: StreamBuilder(
                      stream: querySnapshot('Re-Cycling'),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                          case ConnectionState.none:
                            return Center(
                              child: CircularProgressIndicator(
                                color: constants.darkRedColor,
                              ),
                            );
                          case ConnectionState.done:
                          case ConnectionState.active:
                            if (!snapshot.hasData) {
                              return Center(
                                child: Text('No item found'),
                              );
                            } else {
                              log('true');
                              return Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    primary: true,
                                    physics: ScrollPhysics(),
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) {
                                      var data = snapshot.data!.docs;
                                      ProductModel productModel = ProductModel(
                                          image: data[index]['image'],
                                          price: data[index]['price'],
                                          name: data[index]['name'],
                                          rating: (data[index]['rating'] as num)
                                              .toDouble(),
                                          description: data[index]
                                              ['description'],
                                          productId: data[index]['id'],
                                          category: data[index]['category'],
                                          userName: data[index]['userName'],
                                          userId: data[index]['userId'],
                                          email: data[index]['email'],
                                          city: data[index]['city'],
                                          state: data[index]['state'],
                                          country: data[index]['country'],
                                          // conditionDropDownValue: data[index]
                                          //     ['condition'],
                                          conditionDropDownValue: data[index]
                                              ['condition'],
                                          organization: data[index]
                                              ['organization'],
                                          serviceName: data[index]
                                              ['serviceName'],
                                          amTime: data[index]['amTime'],
                                          pmTime: data[index]['pmTime']);

                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10.0),
                                        child: Container(
                                          child: FillImageCard(
                                              borderRadius: 10,
                                              contentPadding:
                                                  EdgeInsets.only(left: 5.0),
                                              height: height * 0.150,
                                              // footer:
                                              heightImage: height * 0.11,
                                              width: width * 0.35,
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Container(
                                                        width: width * 0.25,
                                                        child: Text(
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          productModel.name,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: height * 0.04,
                                                        width: width * 0.04,
                                                        child: PopupMenuButton(
                                                            icon: Icon(
                                                                Icons.more_vert,
                                                                color: Constants()
                                                                    .blackColor,
                                                                size: 15),
                                                            itemBuilder:
                                                                (context) {
                                                              return [
                                                                PopupMenuItem(
                                                                    onTap: () {
                                                                      deleteProduct(
                                                                          productModel
                                                                              .productId
                                                                              .toString(),
                                                                          productModel
                                                                              .userId
                                                                              .toString());
                                                                    },
                                                                    value: 0,
                                                                    child: Text(
                                                                        'Delete'))
                                                              ];
                                                            }),
                                                      )
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 7.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: height * 0.03,
                                                          width: width * 0.15,
                                                          child:
                                                              AnimatedRatingBar(
                                                                  initialRating:
                                                                      productModel
                                                                          .rating
                                                                          .toDouble(),
                                                                  activeFillColor:
                                                                      constants
                                                                          .yellowColor,
                                                                  strokeColor:
                                                                      constants
                                                                          .blackColor,
                                                                  onRatingUpdate:
                                                                      (val) async {
                                                                    final auth =
                                                                        FirebaseAuth
                                                                            .instance
                                                                            .currentUser;
                                                                    final postRef = await FirebaseFirestore
                                                                        .instance
                                                                        .collection(
                                                                            'All Posts')
                                                                        .doc(productModel
                                                                            .productId)
                                                                        .update({
                                                                      'rating':
                                                                          val.toInt()
                                                                    }).then(
                                                                            (value) async {
                                                                      final auth2 =
                                                                          FirebaseFirestore
                                                                              .instance;
                                                                      await auth2
                                                                          .collection(
                                                                              'User')
                                                                          .doc(productModel
                                                                              .userId)
                                                                          .collection(
                                                                              'myPosts')
                                                                          .doc(productModel
                                                                              .productId)
                                                                          .update({
                                                                        'rating':
                                                                            val.toInt()
                                                                      });
                                                                    }).onError((error,
                                                                                stackTrace) =>
                                                                            null);
                                                                  }),
                                                        ),
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            3),
                                                                color: Constants()
                                                                    .darkRedColor),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(1.5),
                                                              child: Text(
                                                                '${productModel.category}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        11,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ))
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              imageProvider: NetworkImage(
                                                  productModel.image)),
                                        ),
                                      );
                                    }),
                              );
                            }

                          // break;
                          default:
                        }
                        return Container();
                      }),
                )

                // //For Re-Cycling Containers (Code)

                ,
                SizedBox(
                  height: 20,
                ),
                CustomCatagoryLabel(
                    title:
                        'This is a which provided us detail about Banner about what is Eco-Mart and Donation',
                    borderRadius: 10,
                    height: height * 0.15,
                    width: width,
                    fontFamily: 'JosefinSans',
                    fontSize: constants.simpleFontSize,
                    backgroundColor: constants.greyColor,
                    borderColor: constants.greyColor,
                    borderWidth: 2),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Donation',
                      style: TextStyle(
                          fontFamily: 'JosefinSans',
                          fontSize: constants.categoryLabelFontSize,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        onPressed: () {
                          Get.to(() => AdminDonationScreen());
                        },
                        child: Text('view all'))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: height * 0.23,
                  width: width,
                  child: StreamBuilder(
                      stream: querySnapshot('Donation'),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                          case ConnectionState.none:
                            return Center(
                              child: CircularProgressIndicator(
                                color: constants.darkRedColor,
                              ),
                            );
                          case ConnectionState.done:
                          case ConnectionState.active:
                            if (!snapshot.hasData) {
                              return Center(
                                child: Text('No item found'),
                              );
                            } else {
                              log('true');
                              return Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    primary: true,
                                    physics: ScrollPhysics(),
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) {
                                      var data = snapshot.data!.docs;
                                      ProductModel productModel = ProductModel(
                                          image: data[index]['image'],
                                          price: data[index]['price'],
                                          name: data[index]['name'],
                                          rating: (data[index]['rating'] as num)
                                              .toDouble(),
                                          description: data[index]
                                              ['description'],
                                          productId: data[index]['id'],
                                          category: data[index]['category'],
                                          userName: data[index]['userName'],
                                          userId: data[index]['userId'],
                                          email: data[index]['email'],
                                          city: data[index]['city'],
                                          state: data[index]['state'],
                                          country: data[index]['country'],
                                          conditionDropDownValue: data[index]
                                              ['condition'],
                                          organization: data[index]
                                              ['organization'],
                                          serviceName: data[index]
                                              ['serviceName'],
                                          amTime: data[index]['amTime'],
                                          pmTime: data[index]['pmTime']);

                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          child: FillImageCard(
                                              borderRadius: 10,
                                              contentPadding:
                                                  EdgeInsets.only(left: 5.0),
                                              height: height * 0.180,
                                              // footer:
                                              heightImage: height * 0.11,
                                              width: width * 0.35,
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Container(
                                                        width: width * 0.25,
                                                        child: Text(
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          productModel.name,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: height * 0.04,
                                                        width: width * 0.04,
                                                        child: PopupMenuButton(
                                                            icon: Icon(
                                                                Icons.more_vert,
                                                                color: Constants()
                                                                    .blackColor,
                                                                size: 15),
                                                            itemBuilder:
                                                                (context) {
                                                              return [
                                                                PopupMenuItem(
                                                                    onTap: () {
                                                                      deleteProduct(
                                                                          productModel
                                                                              .productId
                                                                              .toString(),
                                                                          productModel
                                                                              .userId
                                                                              .toString());
                                                                    },
                                                                    value: 0,
                                                                    child: Text(
                                                                        'Delete'))
                                                              ];
                                                            }),
                                                      )
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 7.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: height * 0.03,
                                                          width: width * 0.15,
                                                          child:
                                                              AnimatedRatingBar(
                                                                  initialRating:
                                                                      productModel
                                                                          .rating
                                                                          .toDouble(),
                                                                  activeFillColor:
                                                                      constants
                                                                          .yellowColor,
                                                                  strokeColor:
                                                                      constants
                                                                          .blackColor,
                                                                  onRatingUpdate:
                                                                      (val) async {
                                                                    final auth =
                                                                        FirebaseAuth
                                                                            .instance
                                                                            .currentUser;
                                                                    final postRef = await FirebaseFirestore
                                                                        .instance
                                                                        .collection(
                                                                            'All Posts')
                                                                        .doc(productModel
                                                                            .productId)
                                                                        .update({
                                                                      'rating':
                                                                          val.toInt()
                                                                    }).then(
                                                                            (value) async {
                                                                      final auth2 =
                                                                          FirebaseFirestore
                                                                              .instance;
                                                                      await auth2
                                                                          .collection(
                                                                              'User')
                                                                          .doc(productModel
                                                                              .userId)
                                                                          .collection(
                                                                              'myPosts')
                                                                          .doc(productModel
                                                                              .productId)
                                                                          .update({
                                                                        'rating':
                                                                            val.toInt()
                                                                      });
                                                                    }).onError((error,
                                                                                stackTrace) =>
                                                                            null);
                                                                  }),
                                                        ),
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            3),
                                                                color: Constants()
                                                                    .darkRedColor),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(1.5),
                                                              child: Text(
                                                                '${productModel.category}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        11,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ))
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              imageProvider: NetworkImage(
                                                  productModel.image)),
                                        ),
                                      );
                                    }),
                              );
                            }

                          // break;
                          default:
                        }
                        return Container();
                      }),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Eco-Mart',
                      style: TextStyle(
                          fontFamily: 'JosefinSans',
                          fontSize: constants.categoryLabelFontSize,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        onPressed: () {
                          Get.to(() => AdminEcomartScreen());
                        },
                        child: Text('view all'))
                  ],
                ),
                Container(
                  height: height * 0.24,
                  width: width,
                  child: StreamBuilder(
                      stream: querySnapshot('Eco-Mart'),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.waiting:
                          case ConnectionState.none:
                            return Center(
                              child: CircularProgressIndicator(
                                color: constants.darkRedColor,
                              ),
                            );
                          case ConnectionState.done:
                          case ConnectionState.active:
                            if (!snapshot.hasData) {
                              return Center(
                                child: Text('No item found'),
                              );
                            } else {
                              log('true');
                              return Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    primary: true,
                                    physics: ScrollPhysics(),
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) {
                                      var data = snapshot.data!.docs;
                                      ProductModel productModel = ProductModel(
                                          image: data[index]['image'],
                                          price: data[index]['price'],
                                          name: data[index]['name'],
                                          rating: (data[index]['rating'] as num)
                                              .toDouble(),
                                          description: data[index]
                                              ['description'],
                                          productId: data[index]['id'],
                                          category: data[index]['category'],
                                          userName: data[index]['userName'],
                                          userId: data[index]['userId'],
                                          email: data[index]['email'],
                                          city: data[index]['city'],
                                          state: data[index]['state'],
                                          country: data[index]['country'],
                                          conditionDropDownValue: data[index]
                                              ['condition'],
                                          organization: data[index]
                                              ['organization'],
                                          serviceName: data[index]
                                              ['serviceName'],
                                          amTime: data[index]['amTime'],
                                          pmTime: data[index]['pmTime']);

                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: InkWell(
                                          onTap: () {
                                            // Get.to(() => DonationDetailsScreen(
                                            //       title: productModel.name,
                                            //       rating: productModel.rating,
                                            //       price: productModel.price,
                                            //       category:
                                            //           productModel.category,
                                            //       image: productModel.image,
                                            //       description:
                                            //           productModel.description,
                                            //       userName:
                                            //           productModel.userName,
                                            //       email: productModel.email,
                                            //       productId:
                                            //           productModel.productId,
                                            //       userId: productModel.userId,
                                            //       condition: productModel
                                            //           .conditionDropDownValue,
                                            //       country: productModel.country,
                                            //       city: productModel.city,
                                            //       state: productModel.state,
                                            //       organization:
                                            //           productModel.organization,
                                            //       serviceName:
                                            //           productModel.serviceName,
                                            //     ));
                                            // ;
                                          },
                                          child: Container(
                                            child: FillImageCard(
                                                borderRadius: 10,
                                                contentPadding:
                                                    EdgeInsets.only(left: 5.0),
                                                height: 150,
                                                // footer:
                                                heightImage: 85,
                                                width: width * 0.35,
                                                title: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Container(
                                                          width: width * 0.25,
                                                          child: Text(
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            productModel.name,
                                                            style: TextStyle(
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: height * 0.04,
                                                          width: width * 0.04,
                                                          child:
                                                              PopupMenuButton(
                                                                  icon: Icon(
                                                                      Icons
                                                                          .more_vert,
                                                                      color: Constants()
                                                                          .blackColor,
                                                                      size: 15),
                                                                  itemBuilder:
                                                                      (context) {
                                                                    return [
                                                                      PopupMenuItem(
                                                                          onTap:
                                                                              () {
                                                                            deleteProduct(productModel.productId.toString(),
                                                                                productModel.userId.toString());
                                                                          },
                                                                          value:
                                                                              0,
                                                                          child:
                                                                              Text('Delete'))
                                                                    ];
                                                                  }),
                                                        )
                                                      ],
                                                    ),
                                                    Text(
                                                        'Price: ${productModel.price} Rs'),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 7.0),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            height:
                                                                height * 0.03,
                                                            width: width * 0.15,
                                                            child:
                                                                AnimatedRatingBar(
                                                                    initialRating:
                                                                        productModel
                                                                            .rating
                                                                            .toDouble(),
                                                                    activeFillColor:
                                                                        constants
                                                                            .yellowColor,
                                                                    strokeColor:
                                                                        constants
                                                                            .blackColor,
                                                                    onRatingUpdate:
                                                                        (val) async {
                                                                      final auth = FirebaseAuth
                                                                          .instance
                                                                          .currentUser;
                                                                      final postRef = await FirebaseFirestore
                                                                          .instance
                                                                          .collection(
                                                                              'All Posts')
                                                                          .doc(productModel
                                                                              .productId)
                                                                          .update({
                                                                        'rating':
                                                                            val.toInt()
                                                                      }).then(
                                                                              (value) async {
                                                                        // Utils.toastMessages(
                                                                        //     'Rating update successfully');
                                                                        final auth2 =
                                                                            FirebaseFirestore.instance;
                                                                        await auth2
                                                                            .collection(
                                                                                'User')
                                                                            .doc(productModel
                                                                                .userId)
                                                                            .collection(
                                                                                'myPosts')
                                                                            .doc(productModel
                                                                                .productId)
                                                                            .update({
                                                                          'rating':
                                                                              val.toInt()
                                                                        });
                                                                      }).onError((error, stackTrace) =>
                                                                              null);
                                                                    }),
                                                          ),
                                                          Container(
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              3),
                                                                  color: Constants()
                                                                      .darkRedColor),
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(
                                                                        1.5),
                                                                child: Text(
                                                                  '${productModel.category}',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          11,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                              ))
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                imageProvider: NetworkImage(
                                                    productModel.image)),
                                          ),
                                        ),
                                      );
                                    }),
                              );
                            }

                          // break;
                          default:
                        }
                        return Container();
                      }),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
