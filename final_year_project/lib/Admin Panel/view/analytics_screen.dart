import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:final_year_project/Admin%20Panel/controllers/bar_chartController.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/customText.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';

import '../BarChart/myBarChart_widget.dart';

class AnalyticsScreen extends StatefulWidget {
  const AnalyticsScreen({super.key});

  @override
  State<AnalyticsScreen> createState() => _AnalyticsScreenState();
}

class _AnalyticsScreenState extends State<AnalyticsScreen> {
  @override
  // BarChartcontroller barChartcontroller = BarChartcontroller();
  int allPostData = 0;
  int swappingPosts = 0;
  int donationPosts = 0;
  int re_cyclingPosts = 0;
  int eco_martPosts = 0;

  @override
  void initState() {
    // TODO: implement initState
    getAllPostData();

    super.initState();
  }

  Future<void> getAllPostData() async {
    QuerySnapshot allPostDatadata =
        await FirebaseFirestore.instance.collection('All Posts').get();
    QuerySnapshot _swappingPosts = await FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: 'Swapping')
        .get();
    QuerySnapshot _donationPosts = await FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: 'Donation')
        .get();
    QuerySnapshot _re_cyclingPosts = await FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: 'Re-Cycling')
        .get();
    QuerySnapshot _eco_martPosts = await FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: 'Eco-Mart')
        .get();
    // allPostDatadata.size;
    setState(() {
      allPostData = allPostDatadata.size;
      swappingPosts = _swappingPosts.size;
      donationPosts = _donationPosts.size;
      re_cyclingPosts = _re_cyclingPosts.size;
      eco_martPosts = _eco_martPosts.size;
    });
    print("All Posts${allPostData}");
    print("Swappingzzzz${swappingPosts}");
    print(donationPosts);
    print(re_cyclingPosts);
    print(eco_martPosts);
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> postsData = [
      allPostData.toDouble(),
      swappingPosts.toDouble(),
      re_cyclingPosts.toDouble(),
      donationPosts.toDouble(),
      eco_martPosts.toDouble()
    ];
    return Scaffold(
        appBar: CustomAppbar(
          title: 'Analytics',
          automaticallyImplyLeading: true,
          backgroundColor: Constants().darkRedColor,
          textColor: Constants().whiteColor,
        ),
        body: SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start
            // ,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              MyBarGraph(postsData: postsData),
              // StreamBuilder(
              //     stream: barChartcontroller.getAllPostData(), builder: builder)
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    // CustomText(fontSize: 14, title: 'All Posts:  ${allPostData}')
                    Row(
                      children: [
                        customTextWidget('All Posts:'),
                        CustomText(
                          fontSize: 16,
                          title: '        ${allPostData}',
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      children: [
                        customTextWidget('Swapping:'),
                        CustomText(
                          fontSize: 16,
                          title: '        ${swappingPosts}',
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      children: [
                        customTextWidget('Re-Cycling:'),
                        CustomText(
                          fontSize: 16,
                          title: '        ${re_cyclingPosts}',
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      children: [
                        customTextWidget('Donation:'),
                        CustomText(
                          fontSize: 16,
                          title: '        ${donationPosts}',
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      children: [
                        customTextWidget('Eco-Mart:'),
                        CustomText(
                          fontSize: 16,
                          title: '        ${eco_martPosts}',
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

Widget customTextWidget(String title) {
  return Container(
      // height: ,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
          color: Constants().darkRedColor),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Text(
          '${title}',
          style: TextStyle(
              color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500),
        ),
      ));
}
