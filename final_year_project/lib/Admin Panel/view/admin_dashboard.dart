import 'package:final_year_project/Admin%20Panel/view/admin_allPost_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/all_complains.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/all_users_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/analytics_screen.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/screens/authentications/login_screen.dart';
import 'package:final_year_project/screens/view/Users/profile_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:floating_bottom_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AdminDashboard extends StatefulWidget {
  const AdminDashboard({super.key});

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  List<dynamic> iconsList = [
    Icons.event_note_sharp,
    CupertinoIcons.person_3_fill,
    Icons.analytics,
    Icons.receipt_long_sharp,

    // CupertinoIcons.profile_circled,
    // CupertinoIcons.settings,
  ];
  List<String> titles = [
    'All Posts',
    'All users',
    'Analytics',
    'All Complains'
    // 'Your Profile',
    // 'Settings'
  ];
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Admin Dashboard',
        automaticallyImplyLeading: false,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
        fontFamily: 'JosefinSans',
        trailing: [
          IconButton(
              onPressed: () async {
                await FirebaseAuth.instance.signOut().then((value) {
                  Get.offAll(() => LoginScreen());
                  Utils.toastMessages('Logout successfully');
                });
              },
              icon: Icon(
                Icons.logout,
                color: Constants().whiteColor,
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: height * 0.07,
            ),
            GridView.builder(
                scrollDirection: Axis.vertical,
                itemCount: 4,
                primary: true,
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 30, right: 30, top: 50),
                physics: BouncingScrollPhysics(
                    decelerationRate: ScrollDecelerationRate.fast),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 1.1,
                    crossAxisSpacing: height * 0.05,
                    mainAxisSpacing: 45,
                    crossAxisCount: 2),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      switch (index) {
                        case 0:
                          Get.to(() => AdminAllPostScreen());
                        case 1:
                          Get.to(() => AllUsersScreen());
                        case 2:
                          Get.to(() => AnalyticsScreen());
                        case 3:
                          Get.to(() => AllComplainScreen());

                          break;
                        default:
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 10,
                                blurRadius: 15,
                                color:
                                    Constants().darkRedColor.withOpacity(0.2))
                          ],
                          borderRadius: BorderRadius.circular(10),
                          color: Constants().darkRedColor),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          children: [
                            Icon(
                              iconsList[index],
                              color: Constants().whiteColor,
                              size: 50,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              titles[index],
                              style: TextStyle(
                                  color: Constants().whiteColor,
                                  fontFamily: 'JosefinSans',
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
