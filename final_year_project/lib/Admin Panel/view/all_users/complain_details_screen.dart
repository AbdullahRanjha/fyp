import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/Utils/constants/colors.dart';
import '../../../resources/widgets/appbar.dart';

class ComplainDetailsScreen extends StatefulWidget {
  String image, subject, description, email, name;
  ComplainDetailsScreen({
    super.key,
    required this.description,
    required this.email,
    required this.image,
    required this.name,
    required this.subject,
  });

  @override
  State<ComplainDetailsScreen> createState() => _ComplainDetailsScreenState();
}

class _ComplainDetailsScreenState extends State<ComplainDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'All Complains',
        automaticallyImplyLeading: true,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
        centerTile: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: Get.height * 0.5,
              width: double.infinity,
              child: Image.network(
                widget.image,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        widget.subject,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        widget.description,
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "SubmittedBy: " + widget.name,
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "User Email: " + widget.email,
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
