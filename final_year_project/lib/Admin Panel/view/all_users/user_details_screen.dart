import 'package:cached_network_image/cached_network_image.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/photo_viewr_scree.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class UserDetailScreen extends StatelessWidget {
  String name, email, address, Phone, image, userId;
  UserDetailScreen(
      {super.key,
      required this.email,
      required this.Phone,
      required this.address,
      required this.userId,
      required this.image,
      required this.name});

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    final auth = FirebaseAuth.instance.currentUser;

    return Scaffold(
      backgroundColor: Constants().whiteColor,
      appBar: CustomAppbar(
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
        fontFamily: 'JosefinSans',
        title: name.toString(),
        automaticallyImplyLeading: true,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Constants().whiteColor,
            )),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: height * 0.08,
            ),
            Container(
              height: height * 0.6,
              // color: Colors.grey,
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Positioned(
                    top: 70,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Constants().whiteColor,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 40,
                                  spreadRadius: 30,
                                  color:
                                      Constants().darkRedColor.withOpacity(0.1))
                            ]),
                        alignment: Alignment.topCenter,
                        height: height * 0.6,
                        width: width * 0.85,
                        child: Column(
                          children: [
                            //here
                            if (auth!.uid == 'Vc3ND3aBXGYOndp0812Xpgxaw9g1')
                              Padding(
                                padding: const EdgeInsets.only(top: 12.0),
                                child: Container(
                                  height: height * 0.05,
                                  margin: EdgeInsets.only(left: 230),
                                  decoration: BoxDecoration(
                                      color: Constants().darkRedColor,
                                      shape: BoxShape.circle),
                                  child: IconButton(
                                      color: Constants().darkRedColor,
                                      onPressed: () {
                                        Get.defaultDialog(
                                            contentPadding: EdgeInsets.all(15),
                                            confirm: CustomButton(
                                              title: 'delete',
                                              backGroundColor:
                                                  Constants().darkRedColor,
                                              onTap: () async {
                                                // Get.back();
                                                // await FirebaseAuth.instance.
                                              },
                                              radius: 10,
                                              width: width * 0.2,
                                              buttonTextColor:
                                                  Constants().whiteColor,
                                              height: height * 0.05,
                                            ),
                                            cancel: CustomButton(
                                              title: 'cancel',
                                              onTap: () {
                                                Get.back();
                                              },
                                              radius: 10,
                                              width: width * 0.2,
                                              height: height * 0.05,
                                            ),
                                            title: 'Delete',
                                            content: Column(
                                              children: [
                                                Text(
                                                    'Are you sure you want to delete this user?')
                                              ],
                                            ));
                                      },
                                      icon: Icon(
                                        Icons.edit,
                                        color: Constants().whiteColor,
                                      )),
                                ),
                              )
                            else
                              SizedBox(
                                height: height * 0.06,
                              ),
                            SizedBox(
                              height: height * 0.04,
                            ),
                            ListTile(
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Name',
                                    style: TextStyle(
                                      fontFamily: 'JosefinSans',
                                    ),
                                  ),
                                  Container(
                                    width: width * 0.5,
                                    child: Center(
                                      child: Text(
                                        name,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            ListTile(
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Email',
                                    style: TextStyle(
                                      fontFamily: 'JosefinSans',
                                    ),
                                  ),
                                  Container(
                                    width: width * 0.5,
                                    child: Center(
                                      child: Text(
                                        email,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            ListTile(
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Address',
                                    style: TextStyle(
                                      fontFamily: 'JosefinSans',
                                    ),
                                  ),
                                  Container(
                                    width: width * 0.5,
                                    child: Center(
                                      child: Text(
                                        address,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            ListTile(
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Phone',
                                    style: TextStyle(
                                      fontFamily: 'JosefinSans',
                                    ),
                                  ),
                                  Container(
                                    width: width * 0.5,
                                    child: Center(
                                      child: Text(
                                        Phone,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  image == ''
                      ? Center(
                          child: Container(
                            alignment: Alignment.topCenter,
                            child: CircleAvatar(
                              backgroundColor: Constants().whiteColor,
                              radius: 70,
                              child: Icon(
                                CupertinoIcons.profile_circled,
                                color: Colors.black.withOpacity(0.2),
                                size: 160,
                              ),
                            ),
                          ),
                        )
                      : Center(
                          child: Container(
                            alignment: Alignment.topCenter,
                            child: InkWell(
                              onTap: () {
                                Get.to(() => PhotoViewrScreen(
                                      image: image,
                                      title: name,
                                    ));
                              },
                              child: CircleAvatar(
                                radius: 70,
                                backgroundImage: NetworkImage(
                                  image,
                                ),
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
