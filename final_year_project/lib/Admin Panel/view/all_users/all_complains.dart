import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/complain_details_screen.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
// import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';

class AllComplainScreen extends StatefulWidget {
  const AllComplainScreen({super.key});

  @override
  State<AllComplainScreen> createState() => _AllComplainScreenState();
}

class _AllComplainScreenState extends State<AllComplainScreen> {
  // Future<Doc>
  final complainsRef =
      FirebaseFirestore.instance.collection('complains').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'All Complains',
        automaticallyImplyLeading: true,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
        centerTile: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            StreamBuilder(
                stream: complainsRef,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.none:
                      return Center(
                        child: CircularProgressIndicator(
                          color: Constants().darkRedColor,
                        ),
                      );
                    case ConnectionState.done:
                    case ConnectionState.active:
                      var data = snapshot.data!.docs;

                      return ListView.builder(
                          shrinkWrap: true,
                          primary: true,
                          physics: BouncingScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: data.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Get.to(
                                  () => ComplainDetailsScreen(
                                    description: data[index]['description'],
                                    email: data[index]['email'],
                                    image: data[index]['image'],
                                    subject: data[index]['subject'],
                                    name: data[index]['submittedBy'],
                                  ),
                                );
                              },
                              child: Card(
                                elevation: 5,
                                child: ListTile(
                                  title: Text(data[index]['subject']),
                                  leading: Image.network(data[index]['image']),
                                  subtitle: Text(data[index]['description']),
                                ),
                              ),
                            );
                          });

                    // break;
                    // default:
                  }
                })
          ],
        ),
      ),
    );
  }
}
