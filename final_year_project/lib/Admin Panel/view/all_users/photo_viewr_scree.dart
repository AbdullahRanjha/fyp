import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewrScreen extends StatelessWidget {
  String image;
  String title;
  PhotoViewrScreen({super.key, required this.image, required this.title});

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: title,
        automaticallyImplyLeading: true,
        backgroundColor: Constants().blackColor,
        textColor: Constants().whiteColor,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Constants().whiteColor,
            )),
      ),
      backgroundColor: Colors.black,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: height * 0.5,
              width: width,
              child: PhotoView(
                // enableRotation: true
                basePosition: Alignment.center,
                filterQuality: FilterQuality.high,
                // backgroundDecoration: BoxDecoration(),
                imageProvider: NetworkImage(image),
                enableRotation: false,

                customSize: Size(width, height),
              ))
        ],
      ),
    );
  }
}
