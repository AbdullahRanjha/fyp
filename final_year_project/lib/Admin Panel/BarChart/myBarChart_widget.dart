import 'package:final_year_project/Admin%20Panel/BarChart/bar_data.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyBarGraph extends StatelessWidget {
  List postsData;
  MyBarGraph({super.key, required this.postsData});

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;

    BarData myBarData = BarData(
        allPosts: postsData[0],
        swappinPosts: postsData[1],
        re_cyclingPosts: postsData[2],
        donationPosts: postsData[3],
        eco_martPosts: postsData[4]);
    myBarData.initializedBarData();
    return Container(
        height: Get.height * 0.5,
        width: Get.width,
        child: BarChart(
          BarChartData(
              // gridData: FlGridData(),
              titlesData: FlTitlesData(
                bottomTitles: AxisTitles(
                    sideTitles: SideTitles(
                        showTitles: true,
                        getTitlesWidget: getBottomTitlesData)),
                // borderData: FlBorderData(show: true),
              ),
              maxY: 50,
              minY: 0,
              barGroups: myBarData.barData
                  .map((data) => BarChartGroupData(x: data.x, barRods: [
                        BarChartRodData(
                            toY: data.y.toDouble(),
                            width: 15,
                            borderRadius: BorderRadius.circular(2))
                      ]))
                  .toList()),
          swapAnimationCurve: Curves.bounceOut,
        ));
  }
}

Widget getBottomTitlesData(double value, TitleMeta meta) {
  const style = TextStyle(fontWeight: FontWeight.bold, fontSize: 10);
  Widget text;
  switch (value.toInt()) {
    case 0:
      text = const Text(
        'All Posts',
        style: style,
      );
      break;
    case 1:
      text = const Text(
        'Swapping',
        style: style,
      );
      break;

    case 2:
      text = const Text(
        'Re-Cycling',
        style: style,
      );
      break;

    case 3:
      text = const Text(
        'Donation',
        style: style,
      );
      break;

    case 4:
      text = const Text(
        'Eco-Mart',
        style: style,
      );
      break;
    default:
      text = const Text(
        '',
        style: style,
      );
  }
  return SideTitleWidget(child: text, axisSide: meta.axisSide);

  // return SideTitleWidget(child: text as dynamic, axisSide: meta.axisSide);
  // return
}
