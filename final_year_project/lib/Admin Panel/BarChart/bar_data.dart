import 'package:final_year_project/Admin%20Panel/BarChart/individual_barChart.dart';

class BarData {
  final double allPosts;
  final double swappinPosts;
  final double re_cyclingPosts;
  final double donationPosts;
  final double eco_martPosts;

  BarData(
      {required this.allPosts,
      required this.swappinPosts,
      required this.re_cyclingPosts,
      required this.donationPosts,
      required this.eco_martPosts});

  List<IndividualBar> barData = [];

  void initializedBarData() {
    barData = [
      IndividualBar(x: 0, y: allPosts),
      IndividualBar(x: 1, y: swappinPosts),
      IndividualBar(x: 2, y: re_cyclingPosts),
      IndividualBar(x: 3, y: donationPosts),
      IndividualBar(x: 4, y: eco_martPosts),
    ];
  }
}
