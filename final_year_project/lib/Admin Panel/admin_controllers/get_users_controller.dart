import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class GetAllusersController extends GetxController {
  final auth = FirebaseAuth.instance.currentUser;

  Stream<QuerySnapshot<Map<String, dynamic>>> getAllUsers() {
    return FirebaseFirestore.instance
        .collection('User')
        .where('userId', isNotEqualTo: auth!.uid)
        .snapshots();
  }

  Future<List<QueryDocumentSnapshot>> getImagesList() async {
    final getImagesList = await FirebaseFirestore.instance
        .collection('User')
        .where('userId', isNotEqualTo: auth!.uid)
        .get();
    return getImagesList.docs;
  }
}
