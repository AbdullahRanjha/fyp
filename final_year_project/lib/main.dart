import 'package:final_year_project/firebase_options.dart';
import 'package:final_year_project/resources/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  // FirebaseMessaging.onBackgroundMessage(firebaseBackgroundMessageHandler);
  runApp(MyApp());
}

// @pragma('vm:emtry-point')
// Future<void> firebaseBackgroundMessageHandler(RemoteMessage message) async {
//   await Firebase.initializeApp();
//   print(message.notification!.title.toString());
// }
User? currentUser;

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GetMaterialApp(
        theme: ThemeData(
            iconButtonTheme: IconButtonThemeData(
                style: ButtonStyle(
                    iconColor: MaterialStatePropertyAll(Colors.white)))),
        // theme: lightTheme,
        title: 'EcoSwap',
        debugShowCheckedModeBanner: false,
        getPages: AppRoutes.appRoutes(),
      ),
    );
  }
}
