import 'dart:async';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_dashboard.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/main.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:final_year_project/screens/view/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class SplashServices {
  final auth = FirebaseAuth.instance;
  final userRef = FirebaseFirestore.instance.collection('User/');
  Future<List<QueryDocumentSnapshot>> userData() async {
    final QuerySnapshot usersData =
        await userRef.where('userId', isEqualTo: auth.currentUser!.uid).get();
    return usersData.docs;
  }

  // User? currentUser;
  void isLogin() async {
    try {
      auth.authStateChanges().listen((User? user) {
        // setState(() {
        currentUser = user;
      });
      if (auth.currentUser != null) {
        if (auth.currentUser!.emailVerified) {
          var usersData = await userData();

          Timer(Duration(seconds: 3), () {
            if (usersData[0]['isAdmin'] == true) {
              log(usersData[0]['isAdmin'].toString());
              Get.offAll(() => AdminDashboard());
            } else {
              Get.offAll(() => MainScreen());
            }
          });
        } else {
          Timer(Duration(seconds: 3), () {
            Get.toNamed(RouteName.loginScreen);
          });
        }
      } else {
        Timer(Duration(seconds: 3), () {
          Get.toNamed(RouteName.loginScreen);
        });
      }
    } catch (e) {
      Utils.toastMessages('${e.toString()}');
    }
  }
}
