import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:final_year_project/Models/message_model.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/main.dart';
import 'package:firebase_auth/firebase_auth.dart';

// import 'package:flutter/material.dart';
class APIs {
  // static final auth = FirebaseAuth.instance.currentUser;
  static final userRef = FirebaseFirestore.instance.collection('User/');
  static final messagesCollections =
      FirebaseFirestore.instance.collection('messages');

  static UsersModel me = UsersModel();

  void getUserCurrentData() async {
    await userRef.doc(currentUser!.uid).get().then((user) {
      if (user.exists) {
        me = UsersModel.fromJson(user.data()!);
        // user[0]['']
        log(me.toString());
      }
    });
    // await userRef.doc(auth!.uid).get().asStream().map((event) =>me!.toJson());
  }

  static void updateProfile() async {
    await userRef
        .doc(currentUser!.uid)
        .update({
          'address': me!.address.toString(),
          'email': me!.email.toString(),
          'name': me!.name.toString(),
          'phone': me!.phone.toString()
        })
        .then((value) => Utils.toastMessages('Profile update successfully'))
        .onError((error, stackTrace) => Utils.toastMessages(error.toString()));
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getMessageList(
      var fromId, var toId) {
    return FirebaseFirestore.instance
        .collection('messages/')
        .where("fromId", isEqualTo: fromId)
        .where('toId', isEqualTo: toId)
        .orderBy("orderBy", descending: true)
        .snapshots();
  }

  // static String getConversationId(String id) =>
  //     auth!.uid.hashCode <= id.hashCode
  //         ? '${auth!.uid}_$id'
  //         : '${id}_${auth!.uid}';
  // void sendMessage ()async{}

  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllUsers() {
    return FirebaseFirestore.instance
        .collection('User/')
        .where("userId", isNotEqualTo: currentUser!.uid)
        .snapshots();
  }

  // static Stream<QuerySnapshot<Map<String, dynamic>>> getAllMessages(
  //     String toId) {
  //   return FirebaseFirestore.instance
  //       .collection('chat/${getConversationId(toId.toString())}/messages/')
  //       .snapshots();
  // }

  // static Future<void> sendMessage(UsersClass user, String msg) async {
  //   final time = DateTime.now().millisecondsSinceEpoch.toString();
  //   final ref = FirebaseFirestore.instance.collection(
  //       'chat/${getConversationId(user.userId.toString())}/messages');
  //   Message messages = Message(
  //       msg: msg.toString(),
  //       toId: user.userId.toString(),
  //       read: 'true',
  //       type: Type.text,
  //       sent: 'true',
  //       fromId: auth!.uid.toString(),
  //       timestamp: FieldValue.serverTimestamp());
  //   await ref
  //       .doc(time)
  //       .set(messages.toJson())
  //       .then((value) => log('Message send successfully'))
  //       .onError((error, stackTrace) => log('${error.toString()}'));
  // }
  // void sendMessage(String toId, String fromId, String text) async {
  //   Message messages = Message(
  //     msg: text.toString(),
  //     toId: toId.toString(),
  //     read: 'true',
  //     // type: Type.text,
  //     sent: 'true',
  //     fromId: fromId,
  //   );
  //   await messagesCollections.add(messages.toJson()).then((value) {
  //     // messageController.clear();

  //     log('message sent successfully');
  //   }).onError((error, stackTrace) {
  //     log(error.toString());
  //   });

  //   // if (messageController.text.trim() == '' ||
  //   //     messageController.text.trim().isEmpty) {
  //   //   return Utils.toastMessages("Empty message can't be send");
  //   // } else {

  //   // }
  //   // FocusScope.of(context as BuildContext).unfocus();

  //   // submit message
  //   // messageController.clear();
  //   // setState(() {});
  // }
}
