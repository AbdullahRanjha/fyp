import 'dart:developer';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Models/product_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/routes/routes.dart';
import 'package:final_year_project/screens/view/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class PostServices extends GetxController {
  var controller = Get.find<LogicsController>();
  final auth = FirebaseAuth.instance.currentUser;

  final userRef = FirebaseFirestore.instance.collection('User');

  Future<List<QueryDocumentSnapshot>> userData() async {
    var data = await userRef
        .where(
          'userId',
          isEqualTo: auth!.uid.toString(),
        )
        .get();
    return data.docs;
  }

  Future addPost(
      String name,
      String price,
      String category,
      String description,
      String image,
      String country,
      String state,
      String city,
      String condition,
      String organization,
      String serviceName,
      String amTime,
      String pmTime,
      int rating) async {
    RxString id = DateTime.now().millisecondsSinceEpoch.toString().obs;
    var usersData = await userData();
    rating = 1;
    final myPostRef = FirebaseFirestore.instance
        .collection('User')
        .doc(auth!.uid.toString())
        .collection('myPosts')
        .doc(id.value.toString());

    final allPostRef = FirebaseFirestore.instance.collection('All Posts');
    // .collection("All Posts/${category}/${id.value.toString()}");

    firebase_storage.Reference reference = firebase_storage
        .FirebaseStorage.instance
        .ref("Posts" + "${category}/" + "${id.value.toString()}");
    try {
      controller.isLoading.value = true;
      firebase_storage.UploadTask uploadTask =
          reference.putFile(File(controller.image.toString()));
      await Future.value(uploadTask)
          .then((value) {})
          .onError((error, stackTrace) {
        print(error.toString());

        Utils.snackBar("Image", "${error.toString()}");
      });
      var newUrl = await reference.getDownloadURL();
      controller.isLoading.value = true;

      var allPostResponse = allPostRef.doc(id.value).set({
        "id": id.value,
        "name": name,
        "price": price,
        "category": category,
        "description": description,
        "image": newUrl.toString(),
        "rating": rating,
        "email": usersData[0]['email'],
        'userId': auth!.uid,
        "userName": usersData[0]['name'],
        "country": country,
        "state": state,
        "city": city,
        'condition': condition,
        'organization': organization,
        'serviceName': serviceName,
        'amTime': amTime,
        'pmTime': pmTime,
      }).then((value) {
        var myPostResponse = myPostRef.set({
          "id": id.value,
          "name": name,
          "price": price,
          "category": category,
          "description": description,
          "image": newUrl.toString(),
          "rating": rating,
          "email": usersData[0]['email'],
          'userId': auth!.uid,
          "userName": usersData[0]['name'],
          "country": country,
          "state": state,
          "city": city,
          'condition': condition,
          'organization': organization,
          'serviceName': serviceName,
          'amTime': amTime,
          'pmTime': pmTime,
        }).then((value) {
          log('successfully added in my post screen');
        }).onError((error, stackTrace) {
          Utils.toastMessages('${error.toString()}');
        });
        controller.isLoading.value = false;

        Utils.snackBar("Post", "Post Upload Successfully😍");
        navigator!.pushReplacement(
            MaterialPageRoute(builder: (context) => MainScreen()));
      }).onError((error, stackTrace) {
        controller.isLoading.value = false;
        print(error.toString());

        Utils.snackBar("Post", error.toString());
      });
    } catch (e) {
      controller.isLoading.value = false;
      print(e.toString());
      Utils.snackBar("Post", e.toString());
    }
  }

  Future updatePost(
      String name,
      String price,
      // String category,
      String description,
      String image,
      String id) async {
    // RxString id = DateTime.now().millisecondsSinceEpoch.toString().obs;
    var usersData = await userData();
    // rating = 1;
    final myPostRef = FirebaseFirestore.instance
        .collection('User')
        .doc(auth!.uid.toString())
        .collection('myPosts')
        .doc(id.toString());

    final allPostRef = FirebaseFirestore.instance.collection('All Posts');
    // .collection("All Posts/${category}/${id.value.toString()}");

    firebase_storage.Reference reference = firebase_storage
        .FirebaseStorage.instance
        .ref("Posts" + "${id.toString()}");
    try {
      controller.isLoading.value = true;
      firebase_storage.UploadTask uploadTask =
          reference.putFile(File(controller.image.toString()));
      await Future.value(uploadTask)
          .then((value) {})
          .onError((error, stackTrace) {
        print(error.toString());

        Utils.snackBar("Image", "${error.toString()}");
      });
      var newUrl = await reference.getDownloadURL();
      controller.isLoading.value = true;

      var allPostResponse = allPostRef.doc(id).update({
        "id": id,
        "name": name,
        "price": price,
        // "category": category,
        "description": description,
        "image": newUrl.toString(),
        // "rating": rating,
        // "email": usersData[0]['email'],
        // 'userId': auth!.uid,
        // "userName": usersData[0]['name'],
        // "country": country,
        // "state": state,
        // "city": city,
        // 'condition': condition,
        // 'organization': organization,
        // 'serviceName': serviceName,
        // 'amTime': amTime,
        // 'pmTime': pmTime,
      }).then((value) {
        var myPostResponse = myPostRef.update({
          "id": id,
          "name": name,
          "price": price,
          // "category": category,
          "description": description,
          "image": newUrl.toString(),
          // // "rating": rating,
          // "email": usersData[0]['email'],
          // 'userId': auth!.uid,
          // "userName": usersData[0]['name'],
          // "country": country,
          // "state": state,
          // "city": city,
          // 'condition': condition,
          // 'organization': organization,
          // 'serviceName': serviceName,
          // 'amTime': amTime,
          // 'pmTime': pmTime,
        }).then((value) {
          log('successfully added in my post screen');
        }).onError((error, stackTrace) {
          Utils.toastMessages('${error.toString()}');
        });
        controller.isLoading.value = false;

        Utils.snackBar("uPDATE", "Post UPDATED Successfully😍");
        navigator!.pushReplacement(
            MaterialPageRoute(builder: (context) => MainScreen()));
      }).onError((error, stackTrace) {
        controller.isLoading.value = false;
        print(error.toString());

        Utils.snackBar("Post", error.toString());
      });
    } catch (e) {
      controller.isLoading.value = false;
      print(e.toString());
      Utils.snackBar("Post", e.toString());
    }
  }
}
