import 'dart:developer';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/Models/message_modal.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class ChatServices {
  final auth = FirebaseAuth.instance.currentUser;

  final fireStore = FirebaseFirestore.instance;

  // Format time
  String formattedTime = DateFormat('hh:mm a').format(DateTime.now());
  // static String image = '';
  final picker = ImagePicker();

  // final imageRef = FirebaseDatabase.instance.
  Future<void> sendTextMessage(
      String recieverId, message, String recieverName) async {
    //get current userInfo
    final String currentUserID = auth!.uid;

    MessageModal newMessag = MessageModal(
      recieverId: recieverId,
      senderId: auth!.uid,
      message: message,
      timestamp: Timestamp.now(),
      time: formattedTime,
      recieverName: recieverName,
      type: "text",
    );
    // create a new users
    List<String> ids = [currentUserID, recieverId];
    ids.sort();
    String chatRoomIds = ids.join('_');
    await fireStore
        .collection('chat_rooms')
        .doc(chatRoomIds)
        .collection('messages')
        .add(newMessag.toMap());
  }

  Stream<QuerySnapshot> getMessages(String userId, String otherUserId) {
    List<String> ids = [userId, otherUserId];
    ids.sort();
    String chatRoomId = ids.join('_');

    return fireStore
        .collection('chat_rooms')
        .doc(chatRoomId)
        .collection('messages')
        .orderBy("timestamp", descending: true)
        .snapshots();
  }

  Future<void> sendImages(String recieverId, String recieverName) async {
    try {
      final String currentUserID = auth!.uid;
      // String time = DateTime.now().toString();
      // var updatedTime = time.substring(11, 19);
      List<String> ids = [currentUserID, recieverId];
      ids.sort();
      String chatRoomIds = ids.join('_');
      String image = '';

      final pickedImage =
          await picker.pickImage(source: ImageSource.gallery, imageQuality: 40);
      // try {
      if (pickedImage != null) {
        image = pickedImage.path.toString();

        Utils.toastMessages('Image Picked');
      }

      firebase_storage.Reference imageRef = await firebase_storage
          .FirebaseStorage.instance
          .ref("chat_images/")
          .child(chatRoomIds)
          .child('messages');
      firebase_storage.UploadTask uploadTask =
          imageRef.putFile(File(image.toString()));
      await uploadTask.whenComplete(() => log('url get successfully'));
      var newUrl = await imageRef.getDownloadURL();
      print(newUrl.toString());
      // va/r newUrl = await uploadTask
      MessageModal newMessag = MessageModal(
          recieverId: recieverId,
          senderId: auth!.uid,
          recieverName: recieverName,
          message: "",
          timestamp: Timestamp.now(),
          time: formattedTime.toString(),
          type: newUrl.toString());

      if (image.isEmpty || image == '') {
        Utils.toastMessages(
            'Empty message cannot be send. First select image please ');
      } else if (image.isNotEmpty || image != null) {
        await fireStore
            .collection('chat_rooms')
            .doc(chatRoomIds)
            .collection('messages')
            .add(newMessag.toMap())
            .then((value) {
          log('image sent successfully');
          // print(value.)
        }).onError(
                (error, stackTrace) => Utils.toastMessages(error.toString()));
      }
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> sendCameraImages(String recieverId, String recieverName) async {
    try {
      final String currentUserID = auth!.uid;

      List<String> ids = [currentUserID, recieverId];
      ids.sort();
      String chatRoomIds = ids.join('_');
      String image = '';

      final pickedImage =
          await picker.pickImage(source: ImageSource.camera, imageQuality: 40);
      // try {
      if (pickedImage != null) {
        image = pickedImage.path.toString();

        Utils.toastMessages('Image Picked');
      }

      firebase_storage.Reference imageRef = await firebase_storage
          .FirebaseStorage.instance
          .ref("chat_images/")
          .child(chatRoomIds)
          .child('messages');
      firebase_storage.UploadTask uploadTask =
          imageRef.putFile(File(image.toString()));
      await uploadTask.whenComplete(() => log('url get successfully'));
      var newUrl = await imageRef.getDownloadURL();
      print(newUrl.toString());
      // va/r newUrl = await uploadTask
      MessageModal newMessag = MessageModal(
          recieverId: recieverId,
          recieverName: recieverName,
          senderId: auth!.uid,
          message: "",
          timestamp: Timestamp.now(),
          time: formattedTime.toString(),
          type: newUrl.toString());

      if (image.isEmpty || image == '') {
        Utils.toastMessages(
            'Empty message cannot be send. First select image please ');
      } else if (image.isNotEmpty || image != null) {
        await fireStore
            .collection('chat_rooms')
            .doc(chatRoomIds)
            .collection('messages')
            .add(newMessag.toMap())
            .then((value) {
          log('image sent successfully');
          // print(value.)
        }).onError(
                (error, stackTrace) => Utils.toastMessages(error.toString()));
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
