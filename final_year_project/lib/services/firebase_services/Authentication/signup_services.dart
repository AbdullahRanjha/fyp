import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_database/firebase_database.dart';
// import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignupControllers extends GetxController {
  RxBool isLoading = false.obs;
  final auth = FirebaseAuth.instance.obs;
  final profilePictureController =
      FirebaseFirestore.instance.collection('User/');

  final FirebaseFirestore userReference = FirebaseFirestore.instance;

  // DatabaseReference userReference =
  //     FirebaseDatabase.instance.ref().child('User');
  // final auth = FirebaseAuth.instance;
  User get user => auth.value.currentUser!;

  final FirebaseFirestore userRef = FirebaseFirestore.instance;

  void signUp(String name, String email, String phone, String address,
      String password) async {
    try {
      isLoading.value = true;
      final userCredentials = await auth.value
          .createUserWithEmailAndPassword(email: email, password: password)
          // await userCredentials.
          .then((value) async {
        // await userCredentials
        await user.sendEmailVerification();
        isLoading.value = false;
        Utils.snackBar('Email Verification',
            "We have send you a link please verify your email first");

        final userId = value.user!.uid.toString();
        print("Current user id is=" + userId);

        userReference.collection('User/').doc(value.user!.uid.toString()).set({
          "name": name,
          "email": email,
          "phone": phone,
          "address": address,
          "password": password,
          "userId": value.user!.uid.toString(),
          "isOnline": "",
          'isAdmin': false,
          'image': ''
        }).then((value) {
          // String id = DateTime.now().millisecondsSinceEpoch.toString();
          profilePictureController
              .doc(userId)
              .collection('picture')
              .doc(userId)
              .set({
            "image": "",
          });

          // return Utils.toastMessages('User register successfully');
        }).onError((error, stackTrace) {
          return Utils.toastMessages('${error.toString()}');
        });

        Get.toNamed(RouteName.loginScreen);
      }).onError((error, stackTrace) {
        isLoading.value = false;
        print(error.toString());

        return Utils.toastMessages(error.toString());
      });
      print("User credentials are =" + userCredentials.toString());
    } on FirebaseAuthException catch (e) {
      isLoading.value = false;
      if (e.code == "email-already-in-use") {
        Utils.toastMessages(e.message ?? "Authentication failed");
      }
      print(e.toString());
      Utils.toastMessages(e.toString());
    }
  }
}
