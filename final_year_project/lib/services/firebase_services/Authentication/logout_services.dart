import 'dart:math';

import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LogoutController extends GetxController {
  final auth = FirebaseAuth.instance;
  void logoutUser() async {
    try {
      await auth.signOut().then((value) {
        Utils.toastMessages("Logout successfully");
        Get.toNamed(RouteName.loginScreen);
      }).onError((error, stackTrace) {
        Utils.toastMessages(error.toString());
        // dispose()
      });
    } catch (e) {
      Utils.toastMessages(e.toString());
    }
  }
}
