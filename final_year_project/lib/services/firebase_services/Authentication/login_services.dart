import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/admin_dashboard.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/main.dart';
import 'package:final_year_project/screens/view/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import '../../../resources/routes/routes_name.dart';

class LoginController extends GetxController {
  RxBool isLoading = false.obs;
  final auth = FirebaseAuth.instance;
  final userRef = FirebaseFirestore.instance.collection('User');
  Future<List<QueryDocumentSnapshot>> userData() async {
    final QuerySnapshot usersData =
        await userRef.where('userId', isEqualTo: auth.currentUser!.uid).get();
    return usersData.docs;
  }

  void login(String email, String password) async {
    try {
      isLoading.value = true;
      final UserCredential credential = await auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        isLoading.value = false;
        if (auth.currentUser != null) {
          if (auth.currentUser!.emailVerified) {
            var data = await userData();
            if (data[0]['isAdmin'] == true) {
              Get.offAll(() => AdminDashboard());
            } else {
              Get.offAll(() => MainScreen());
            }
            return Utils.toastMessages('Login successfully');
          } else {
            Utils.snackBar(
                'Email not verified', 'Please verify your email first');
          }
        }
      }).onError((error, stackTrace) {
        isLoading.value = false;
        print(error.toString());
        return Utils.toastMessages('${error.toString()}');
      });
      currentUser = credential.user;
      print(credential.toString());
    } catch (e) {
      isLoading.value = false;
      print(e.toString());
      return Utils.toastMessages('${e.toString()}');
    }
    // return ;
  }
  // return Container();
}
