class RouteName {
  static const String splashScreen = '/';
  static const String mainScreen = '/main_screen';
  static const String loginScreen = '/login_screen';
  static const String signupScreen = '/signup_screen';
  static const String homeScreen = '/home_screen';
  static const String swappingScreen = '/swapping_screen';
  static const String recyclingScreen = '/re-cycling_screen';
  static const String donationScreen = '/donation_screen';
  static const String ecoMarketScreen = '/ecoMarket_screen';
  static const String swappingDetailsScreen = '/swapping_details_screen';
  static const String reCyclingDetailsScreen = '/re-cycling_details_screen';
  static const String donationDetailsScreen = '/donationDetailsScreen';
  static const String EcoMartDetailsScreen = '/eco-MartDetails_Screen';
  static const String addPostScreen = '/addPostScreen';
  static const String favouriteScreen = '/favourite_screen';
  static const String chatScreen = '/chat_Screen';
  static const String settingsScreen = '/settings_screen';
  static const String profileScreen = '/profile_screen';
  static const String messageScreen = '/message_screen';
  static const String allUsers = '/users_screen.dart';
}
