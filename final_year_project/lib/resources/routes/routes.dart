// ignore_for_file: unused_import, prefer_const_constructors

import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:final_year_project/screens/authentications/login_screen.dart';
import 'package:final_year_project/screens/authentications/signup_screen.dart';
import 'package:final_year_project/screens/view/Users/users_screen.dart';
import 'package:final_year_project/screens/view/addPost_screen.dart';
import 'package:final_year_project/screens/view/chat/chat_screen.dart';
import 'package:final_year_project/screens/view/chat/messages.dart';
import 'package:final_year_project/screens/view/dashboard/dashboardController.dart';
import 'package:final_year_project/screens/view/donation/details_donationScreen.dart';
import 'package:final_year_project/screens/view/donation/donation_screen.dart';
import 'package:final_year_project/screens/view/eco-mart/details_eco-martScreen.dart';
import 'package:final_year_project/screens/view/eco-mart/eco-market_screen.dart';
// import 'package:final_year_project/screens/view/favourite_screen.dart';
import 'package:final_year_project/screens/view/main_screen.dart';
import 'package:final_year_project/screens/view/Users/profile_screen.dart';
import 'package:final_year_project/screens/view/re-cycling/details_re-cyclingScreen.dart';
import 'package:final_year_project/screens/view/re-cycling/re-cycling_screen.dart';
import 'package:final_year_project/screens/view/setting_screen.dart';
import 'package:final_year_project/screens/view/splash_screen.dart';
import 'package:final_year_project/screens/view/swapping/details_swapping_screen.dart';
import 'package:final_year_project/screens/view/swapping/swapping_screen.dart';
import 'package:get/get.dart';

UsersModel user = UsersModel();

class AppRoutes {
  // UsersClass user = UsersClass();
  static appRoutes() => [
        GetPage(
          name: RouteName.splashScreen,
          page: () => const SplashScreen(),
        ),
        GetPage(name: RouteName.mainScreen, page: () => MainScreen()),
        GetPage(
          name: RouteName.loginScreen,
          page: () => const LoginScreen(),
        ),
        GetPage(
          name: RouteName.signupScreen,
          page: () => const SignupScreen(),
          // transition: Transition.leftToRight,
          // transitionDuration: Duration(milliseconds: 500)
        ),
        GetPage(
          name: RouteName.homeScreen,
          page: () => const DashboardController(),
          // transition: Transition.leftToRight,
          // transitionDuration:const Duration(milliseconds: 500)
        ),
        GetPage(
          name: RouteName.swappingScreen,
          page: () => const SwappingScreen(),
          // transition: Transition.leftToRight,
          // transitionDuration:const Duration(milliseconds: 500)
        ),
        GetPage(
          name: RouteName.recyclingScreen,
          page: () => const Re_CyclingScreen(),
          // transition: Transition.leftToRight,
          // transitionDuration: Duration(milliseconds: 500)
        ),
        GetPage(
          name: RouteName.donationScreen,
          page: () => DonationScreen(),
          // transition: Transition.leftToRight,
          // transitionDuration: Duration(milliseconds: 500)
        ),

        GetPage(
          name: RouteName.messageScreen,
          page: () => MessageScreen(
              // user: user,
              ),
          // transition: Transition.leftToRight,
          // transitionDuration: Duration(milliseconds: 500)
        ),

        GetPage(
          name: RouteName.ecoMarketScreen,
          page: () => Eco_MarketScreen(),
          // transition: Transition.leftToRight,
          // transitionDuration: Duration(milliseconds: 500)
        ),
        // GetPage(
        //     name: RouteName.swappingDetailsScreen,
        //     page: () => SwappingDetailsScreen(),
        //     transition: Transition.leftToRight,
        //     transitionDuration: Duration(milliseconds: 500)),
        // GetPage(
        //     name: RouteName.reCyclingDetailsScreen,
        //     page: () => Re_CyclingDetailsScreen(),
        //     transition: Transition.leftToRight,
        //     transitionDuration: Duration(milliseconds: 500)),
        // GetPage(
        //     name: RouteName.donationDetailsScreen,
        //     page: () => DonationDetailsScreen(),
        //     transition: Transition.leftToRight,
        //     transitionDuration: Duration(milliseconds: 500)),
        // GetPage(
        //     name: RouteName.EcoMartDetailsScreen,
        //     page: () => Eco_MartDetailsScreen(),
        //     transition: Transition.leftToRight,
        //     transitionDuration: Duration(milliseconds: 500)),
        GetPage(
            name: RouteName.addPostScreen,
            page: () => AddPostScreen(),
            transition: Transition.leftToRight,
            transitionDuration: Duration(milliseconds: 500)),
        GetPage(
            name: RouteName.chatScreen,
            page: () => ChatScreen(),
            transition: Transition.leftToRight,
            transitionDuration: Duration(milliseconds: 500)),
        GetPage(
            name: RouteName.allUsers,
            page: () => UsersScreen(),
            transition: Transition.leftToRight,
            transitionDuration: Duration(milliseconds: 500)),
        // GetPage(
        //   name: RouteName.favouriteScreen,
        //   page: () => FavouriteScreen(),
        // ),
        GetPage(
          name: RouteName.profileScreen,
          page: () => ProfileScreen(),
        ),
        GetPage(
          name: RouteName.settingsScreen,
          page: () => SettingsScreen(),
        ),
      ];
}
