
import 'package:flutter/material.dart';

import '../../core/Utils/constants/colors.dart';

class CustomText extends StatelessWidget {
  Constants constants = Constants();
  String title;
  AlignmentGeometry? alignmet;
  double fontSize;
  FontWeight? fontWeight;
  Color? textColor;
  String? fontFamily;
  TextDecoration? textDecoration;
  CustomText(
      {super.key,
      required this.fontSize,
      required this.title,
      this.fontFamily,
      this.fontWeight,
      this.alignmet,
      this.textDecoration});

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: alignmet,
        child: Text(
          title,
          style: TextStyle(
              fontSize: fontSize,
              color: textColor,
              fontFamily: fontFamily,
              decoration: textDecoration,
              fontWeight: fontWeight),
        ));
  }
}
