import 'package:final_year_project/resources/widgets/customText.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
// import 'package:firebase_chat_app/resources/components/customText.dart';
// import 'package:firebase_chat_app/resources/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomCatagoryLabel extends StatelessWidget {
  Constants constants = Constants();
  String title;
  double borderRadius;
  Color? backgroundColor;
  double fontSize;
  double? height;
  double? width;

  FontWeight? fontWeight;
  double borderWidth;
  Color borderColor;
  String? fontFamily;
  CustomCatagoryLabel(
      {super.key,
      required this.title,
      required this.borderRadius,
      required this.fontSize,
      this.fontWeight,
      this.fontFamily,
      this.height,
      required this.borderColor,
      this.width,
      required this.borderWidth,
      this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    // double height = Get.height;
    // double width = Get.width;
    return Container(
      // height: height * 0.06,
      // width: width * 0.3,
      height: height,
      width: width,
      decoration: BoxDecoration(
          // boxShadow:,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 5),
              blurRadius: 10,
              spreadRadius: 1,
              color: Colors.grey.withOpacity(0.2),
            ),
          ],
          borderRadius: BorderRadius.circular(borderRadius),
          border: Border.all(color: borderColor, width: borderWidth),
          color: backgroundColor),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: CustomText(
              fontSize: fontSize,
              fontFamily: fontFamily,
              fontWeight: fontWeight,
              title: title),
        ),
      ),
    );
  }
}
