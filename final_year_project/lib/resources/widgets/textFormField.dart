import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomTextFormField extends StatelessWidget {
  Constants constants = Constants();
  TextEditingController? controller;
  String? hintText, labelText, initialValue;
  Color? filledColor;
  Widget? prefixIcon;
  String? fontFamily;
  int? maxLinse;
  Widget? suffixIcon;
  bool obscureText = false;
  TextInputType? keyboardType;
  FormFieldValidator<String>? validator;
  CustomTextFormField(
      {super.key,
      this.maxLinse,
      this.controller,
      this.hintText,
      this.fontFamily,
      this.prefixIcon,
      this.initialValue,
      this.keyboardType,
      this.validator,
      this.labelText,
      this.suffixIcon,
      this.obscureText = false,
      this.filledColor});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        initialValue: initialValue,
        validator: validator,
        maxLines: maxLinse,
        controller: controller,
        obscureText: obscureText,
        keyboardType: keyboardType,
        decoration: InputDecoration(
            suffixIcon: suffixIcon,
            filled: true,
            fillColor: constants.textFormFieldFilledColor,
            prefixIcon: prefixIcon,
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            enabledBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            focusedBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            hintText: '${hintText}',
            labelStyle: TextStyle(fontFamily: fontFamily),
            hintStyle: TextStyle(fontFamily: fontFamily),
            label: Text(labelText.toString())));
  }
}
