import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
  String title;
  double? fontsize;
  Color? textColor;
  bool automaticallyImplyLeading;
  Color? backgroundColor;
  FontWeight? fontWeight;
  Widget? leading;
  String? fontFamily;
  bool? centerTile;
  // VoidCallback drawerTapp;
  List<Widget>? trailing;
  CustomAppbar(
      {super.key,
      required this.title,
      this.backgroundColor,
      this.trailing,
      this.fontFamily,
      this.fontsize,
      required this.automaticallyImplyLeading,
      this.fontWeight,
      this.centerTile,
      this.leading,
      this.textColor});
  @override
  Size get preferredSize => Size.fromHeight(56.0);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: automaticallyImplyLeading,
      backgroundColor: backgroundColor,
      leading: leading,
      actions: trailing,
      elevation: 0,
      // shape: RoundedRectangleBorder(
      //     borderRadius: BorderRadius.only(
      //   bottomLeft: Radius.circular(20),
      //   bottomRight: Radius.circular(20),
      // )),
      centerTitle: centerTile,
      title: Text(
        title,
        style: TextStyle(
            fontSize: fontsize,
            fontWeight: fontWeight,
            color: textColor,
            fontFamily: fontFamily),
      ),
    );
  }

  // @override
  // Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
