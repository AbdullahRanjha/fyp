import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class PickImageWidget extends StatefulWidget {
  bool isLoading;
  void Function(File fileImage) onPickedImage;

  PickImageWidget(
      {super.key, required this.onPickedImage, required this.isLoading});

  @override
  State<PickImageWidget> createState() => _PickImageWidgetState();
}

class _PickImageWidgetState extends State<PickImageWidget> {
  File? pickedFile;

  void userPickImage() async {
    final picker = await ImagePicker().pickImage(
        source: ImageSource.camera, imageQuality: 100, maxWidth: 150);
    if (picker != null) {
      setState(() {
        pickedFile = File(picker.path.trim().toString());
      });
      widget.onPickedImage(pickedFile!);
    } else {
      return null;
    }
  }

  final auth = FirebaseAuth.instance.currentUser;
  final pictureReference = FirebaseFirestore.instance.collection("User/");
  @override
  Widget build(BuildContext context) {
    final height = Get.height;
    final width = Get.width;
    return StreamBuilder(
        stream: pictureReference
            .doc(auth!.uid)
            .collection('picture')
            .doc(auth!.uid)
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Center(
                  child: Container(
                child: CircularProgressIndicator(
                  color: Constants().darkRedColor,
                ),
              ));
            case ConnectionState.active:
            case ConnectionState.done:
              var data = snapshot.data!.data();
              print(data!['image']);

              return Container(
                child: Column(
                  children: [
                    Container(
                        height: 120,
                        width: 120,
                        decoration: BoxDecoration(

                            // image: ,
                            color: Constants().greyColor,
                            shape: BoxShape.circle),
                        child: widget.isLoading == true
                            ? Center(
                                child: CircularProgressIndicator(
                                  color: Constants().darkRedColor,
                                ),
                              )
                            : (data['image'] == ''
                                ? Center(
                                    child: Icon(
                                      Icons.add_a_photo,
                                      size: 40,
                                    ),
                                  )
                                : Container(
                                    clipBehavior: Clip.antiAlias,
                                    decoration:
                                        BoxDecoration(shape: BoxShape.circle),
                                    child: CachedNetworkImage(
                                      imageUrl: data['image'],
                                      fit: BoxFit.fill,
                                    ),
                                  )
                            // : ClipRRect(
                            //     clipBehavior: Clip.antiAlias,
                            //     child: Image(
                            //       image: NetworkImage(
                            //         data['image'],
                            //       ),
                            //       fit: BoxFit.fill,
                            //     ),
                            //   )),

                            //       ),
                            )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: width * 0.35,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Constants().darkRedColor.withOpacity(0.15),
                              // offset: Offset(0.7, 0.7),
                              blurRadius: 3,
                              spreadRadius: 0.5,
                            )
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () {
                            userPickImage();
                          },
                          child: Container(
                              // onPressed: () {
                              //   userPickImage();
                              // },
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Add Image',
                                style: TextStyle(
                                    fontSize: 17,
                                    fontFamily: 'JosefinSans',
                                    color: Colors.black),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                Icons.add_photo_alternate_outlined,
                                color: Constants().darkRedColor,
                              ),
                            ],
                          )),
                        ),
                      ),
                    )
                  ],
                ),
              );

              // break;
              // default:
              return Container();
          }
        });
  }
}
