import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  String title;
  double? width, height, fontSize;
  double radius;
  Color? buttonTextColor, backGroundColor;
  VoidCallback onTap;
  bool? isLoading = false;
  String? fontFamily;
  CustomButton(
      {super.key,
      required this.title,
      this.height,
      this.isLoading,
      this.fontSize,
      this.fontFamily,
      required this.onTap,
      required this.radius,
      this.backGroundColor,
      this.buttonTextColor,
      this.width});

  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              offset: Offset(0.5, 0.5),
              blurRadius: 2,
              spreadRadius: 7,
              blurStyle: BlurStyle.inner,
              color: Constants().darkRedColor.withOpacity(0.1))
        ], color: backGroundColor, borderRadius: BorderRadius.circular(radius)),
        child: isLoading != true
            ? Center(
                child: Text(
                title,
                style: TextStyle(
                    color: buttonTextColor,
                    fontSize: fontSize,
                    fontFamily: fontFamily),
              ))
            : Center(
                child: CircularProgressIndicator(
                  color: constants.whiteColor,
                ),
              ),
        height: height,
        width: width,
      ),
    );
  }
}
