import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class SwappingController extends GetxController {
  Stream<QuerySnapshot<Map<String, dynamic>>> swappingProducts() {
    return FirebaseFirestore.instance
        .collection('All Posts')
        .where('category', isEqualTo: 'swapping')
        .snapshots();
  }
}
