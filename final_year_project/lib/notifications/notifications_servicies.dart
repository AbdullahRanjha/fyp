// import 'dart:math';

// import 'package:app_settings/app_settings.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// class NotificationsServices {
//   FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
//   final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//       FlutterLocalNotificationsPlugin();

//   NotificationsServices() {
//     _initializeNotifications();
//   }

//   void requestNotificationsPermission() async {
//     NotificationSettings notificationSettings =
//         await firebaseMessaging.requestPermission(
//       alert: true,
//       announcement: true,
//       badge: true,
//       sound: true,
//       carPlay: true,
//       criticalAlert: true,
//       provisional: true,
//     );

//     if (notificationSettings.authorizationStatus ==
//         AuthorizationStatus.authorized) {
//       print('Permission is authorized');
//     } else if (notificationSettings.authorizationStatus ==
//         AuthorizationStatus.provisional) {
//       print('Provisional Permission is authorized');
//     } else {
//       AppSettings.openAppSettings();
//       print('User permission denied');
//     }
//   }

//   void _initializeNotifications() async {
//     var androidInitialization =
//         AndroidInitializationSettings('@mipmap/ic_launcher');
//     var initializationSettings = InitializationSettings(
//       android: androidInitialization,
//       iOS: DarwinInitializationSettings(),
//     );

//     await flutterLocalNotificationsPlugin.initialize(
//       initializationSettings,
//       onDidReceiveNotificationResponse: (payload) {},
//     );
//   }

//   void showNotifications(RemoteMessage message) async {
//     if (message.notification == null) return;

//     AndroidNotificationChannel channel = AndroidNotificationChannel(
//         Random.secure().nextInt(1000).toString(),
//         'High Importance notifications',
//         importance: Importance.max);

//     AndroidNotificationDetails androidNotificationDetails =
//         AndroidNotificationDetails(
//             channel.id.toString(), channel.name.toString(),
//             channelDescription: 'your channel description',
//             importance: Importance.high,
//             priority: Priority.high,
//             ticker: 'ticker');

//     DarwinNotificationDetails darwinNotificationDetails =
//         DarwinNotificationDetails(
//             presentAlert: true, presentBadge: true, presentSound: true);

//     NotificationDetails notificationDetails = NotificationDetails(
//       android: androidNotificationDetails,
//       iOS: darwinNotificationDetails,
//     );

//     await flutterLocalNotificationsPlugin.show(
//       0,
//       message.notification!.title ?? '',
//       message.notification!.body ?? '',
//       notificationDetails,
//     );
//   }

//   void firebaseInit() {
//     FirebaseMessaging.onMessage.listen((message) {
//       if (kDebugMode) {
//         print(message.notification?.title ?? 'No title');
//         print(message.notification?.body ?? 'No body');
//       }

//       showNotifications(message);
//     });
//   }

//   Future<String> getDeviceToken() async {
//     String? token = await firebaseMessaging.getToken();
//     return token!;
//   }

//   void onTokenRefresh() {
//     firebaseMessaging.onTokenRefresh.listen((event) {
//       event.toString();
//     });
//   }
// }
