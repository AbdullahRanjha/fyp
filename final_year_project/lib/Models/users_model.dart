class UsersModel {
  String? password;
  String? address;
  String? phone;
  String? name;
  String? email;
  String? isOnline;
  String? userId;
  String? image;

  UsersModel(
      {this.password,
      this.address,
      this.phone,
      this.image,
      this.name,
      this.email,
      this.isOnline,
      this.userId});

  UsersModel.fromJson(Map<String, dynamic> json) {
    password = json['password'] ?? '';
    address = json['address'] ?? '';
    phone = json['phone'] ?? '';
    name = json['name'] ?? '';
    email = json['email'] ?? '';
    userId = json['userId'] ?? '';
    isOnline = json['isOnline'] ?? '';
    image = json['image'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['address'] = this.address;
    data['phone'] = this.phone;
    data['name'] = this.name;
    data['email'] = this.email;
    data['userId'] = this.userId;
    data['isOnline'] = this.isOnline;
    data['image'] = this.image;

    return data;
  }
}
