
class ProductModel {
  late String image;
  late String price;
  late String name;
  late double rating;
  late String description;
  late String productId;
  late String category;
  late String userName;
  late String userId;
  late String email;
  late String? country;
  late String? state;
  late String? city;
  late String? conditionDropDownValue;
  late String? organization;
  late String? serviceName;
  late String? amTime;
  late String? pmTime;

  ProductModel(
      {required this.image,
      required this.price,
      required this.name,
      required this.rating,
      required this.description,
      required this.productId,
      required this.category,
      required this.userName,
      required this.userId,
      required this.email,
      this.country,
      this.city,
      this.state,
      required this.conditionDropDownValue,
      required this.organization,
      required this.serviceName,
      required this.amTime,
      required this.pmTime});

  ProductModel.fromJson(Map<String, dynamic> json) {
    image = json['image'] ?? '';
    price = json['price'] ?? '';
    name = json['name'] ?? '';
    rating = json['rating'] ?? '';
    description = json['description'] ?? '';
    productId = json['id'] ?? '';
    category = json['category'] ?? '';
    userName = json['userName'] ?? '';
    userId = json['userId'] ?? '';
    email = json['email'] ?? '';
    country = json['country'] ?? '';
    state = json['state'] ?? '';
    city = json['city'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['price'] = this.price;
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['description'] = this.description;
    data['id'] = this.productId;
    data['category'] = this.category;
    data['userName'] = this.userName;
    data['userId'] = this.userId;
    data['email'] = this.email;
    data['country'] = this.country;
    data['state'] = this.state;
    data['city'] = this.city;
    return data;
  }
}
