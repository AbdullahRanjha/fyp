import 'package:cloud_firestore/cloud_firestore.dart';

class MessageModal {
  String? recieverId;
  String? senderId;
  String? message;
  String? recieverName;
  String? time;
  Timestamp? timestamp;
  String? type;

  MessageModal({
    required this.recieverId,
    required this.senderId,
    required this.message,
    required this.time,
    required this.timestamp,
    required this.recieverName,
    required this.type,
  });

  Map<String, dynamic> toMap() {
    return {
      "senderId": senderId,
      "recieverId": recieverId,
      "message": message,
      "time": time,
      'timestamp': timestamp,
      "type": type,
      'recieverName': recieverName
    };
  }

  MessageModal.fromJson(Map<String, dynamic> json) {
    recieverName = json['recieverName'].toString();
    senderId = json['senderId'].toString();
    recieverId = json['recieverId'].toString();
    message = json['message'].toString();
    time = json['time'].toString();
    timestamp = json['timestamp'];
    type = json['type'].toString();
  }
}
