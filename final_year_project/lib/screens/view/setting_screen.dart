import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Constants constants = Constants();
  bool isOne = false;

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        fontFamily: 'JosefinSans',
        title: 'Settings',
        fontWeight: FontWeight.bold,
        fontsize: constants.appbarFontSize,
        automaticallyImplyLeading: true,
        backgroundColor: constants.darkRedColor,
        textColor: constants.whiteColor,
      ),
      body: WillPopScope(
        onWillPop: () async {
          SystemNavigator.pop();
          return false;
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0, 5),
                            blurRadius: 10,
                            spreadRadius: 9,
                            blurStyle: BlurStyle.normal,
                            color: constants.darkRedColor.withOpacity(0.1))
                      ]),
                  // decoration: BoxDecoration(boxShadow: []),
                  child: ListTile(
                    trailing: Switch(
                        value: isOne,
                        onChanged: (value) {
                          isOne = value;
                          setState(() {});
                        }),
                    leading: Icon(
                      Icons.notifications_active_outlined,
                      color: constants.darkRedColor,
                    ),
                    title: Text(
                      'Notifications',
                      style: TextStyle(fontFamily: 'JosefinSans', fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
