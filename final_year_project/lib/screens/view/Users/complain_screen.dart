import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/resources/widgets/textFormField.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../core/Utils/utils.dart';
import '../../../getx_logics/logics.dart';

class ComplainScreen extends StatefulWidget {
  const ComplainScreen({super.key});

  @override
  State<ComplainScreen> createState() => _ComplainScreenState();
}

class _ComplainScreenState extends State<ComplainScreen> {
  TextEditingController subjectController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  LogicsController controller = Get.put(LogicsController());
  Constants constants = Constants();
  final ref = FirebaseFirestore.instance;
  Future<void> uploadComplains() async {
    Utils.easyLoading('Please wait...');
    // EasyLoading.show()

    firebase_storage.Reference reference =
        firebase_storage.FirebaseStorage.instance.ref("Complains");
    // Utils.easyLoading('Please wait...');

    // controller.isLoading.value = true;
    firebase_storage.UploadTask uploadTask =
        reference.putFile(File(controller.image.toString()));
    await Future.value(uploadTask)
        .then((value) {})
        .onError((error, stackTrace) {
      print(error.toString());

      Utils.snackBar("Image", "${error.toString()}");
    });
    var newUrl = await reference.getDownloadURL();
    Utils.easyLoading('Please wait...');

    await ref.collection('complains').add({
      'subject': subjectController.text,
      'description': descriptionController.text,
      'image': '${newUrl.toString()}',
      'submittedBy': nameController.text,
      'email': emailController.text,
    }).then((val) {
      // EasyLoading.dismiss();
      Utils.snackBar('Complain', 'Complain submitted successfully');
    }).onError((error, stackTrace) {
      Utils.snackBar('Error', '${error}');
      // EasyLoading.dismiss();
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Add Complains',
        automaticallyImplyLeading: true,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              CustomTextFormField(
                hintText: 'Subject',
                labelText: 'Subject',
                controller: subjectController,
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextFormField(
                hintText: 'Description',
                labelText: 'Description',
                controller: descriptionController,
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextFormField(
                hintText: 'Your Name',
                labelText: 'Name',
                controller: nameController,
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextFormField(
                hintText: 'Your Email',
                labelText: 'Email',
                controller: emailController,
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller.getGalleryImage();
                },
                child: Obx(() {
                  if (controller.image.value.isEmpty) {
                    return Container(
                      height: height * 0.35,
                      width: width,
                      decoration: BoxDecoration(
                          color: constants.greyColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              CupertinoIcons.photo,
                              size: 40,
                            ),
                            Text(
                              'No Image Selected',
                              style: TextStyle(
                                  fontSize: 22, fontFamily: 'JosefinSans'),
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Container(
                      child: Image.file(
                        File(controller.image.value.toString()),
                        fit: BoxFit.fill,
                        height: height * 0.35,
                        width: width,
                      ),
                    );
                  }
                }),
              ),
              SizedBox(
                height: 20,
              ),
              CustomButton(
                title: 'Submit',
                onTap: () {
                  String subject = subjectController.text.toString();
                  String description = descriptionController.text.toString();
                  if (subject.isEmpty ||
                      description.isEmpty ||
                      controller.image.value.isEmpty) {
                    Utils.snackBar('Error', 'All Firelds are required');
                  } else {
                    uploadComplains();
                  }
                },
                radius: 10,
                fontSize: 16,
                height: 45,
                backGroundColor: constants.darkRedColor,
                buttonTextColor: constants.whiteColor,
              )
            ],
          ),
        ),
      ),
    );
  }
}
