// import 'dart:convert';

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_print, unnecessary_string_interpolations, avoid_unnecessary_containers, unnecessary_import, unused_import

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/main.dart';
import 'package:final_year_project/screens/view/chat/chat_screen.dart';
import 'package:final_year_project/screens/view/chat/messages.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
// import 'package:final_year_project/newChat/new_chat_screen.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
// import 'package:final_year_project/resources/widgets/users_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../Admin Panel/view/all_users/user_details_screen.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({super.key});

  @override
  State<UsersScreen> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  Constants constants = Constants();
  // List<U>
  // List<UsersModel> list = [];
  String? search = '';
  final auth = FirebaseAuth.instance.currentUser;
  TextEditingController searchController = TextEditingController();
  // final userRef = FirebaseFirestore.instance
  //     .collection('User/')
  //     .where("userId", isNotEqualTo: APIs.auth!.uid)
  //     .snapshots();
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        backgroundColor: Constants().darkRedColor,
        title: 'All Users',
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Constants().whiteColor,
            )),
        textColor: Constants().whiteColor,
        fontFamily: 'JosefinSans',
        fontWeight: FontWeight.bold,
        fontsize: Constants().appbarFontSize,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: SearchBar(
              hintText: 'Search',
              controller: searchController,
              onChanged: (value) {
                setState(() {
                  search = value.toString();
                });
              },
            ),
          ),
          Expanded(
            child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('User/')
                    .where("userId", isNotEqualTo: currentUser!.uid)
                    .snapshots(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Container(
                          child: Center(
                        child: CircularProgressIndicator(
                          color: Constants().darkRedColor,
                        ),
                      ));
                    case ConnectionState.active:
                    case ConnectionState.done:
                      // break;

                      var data = snapshot.data!.docs;
                      return Container(
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            primary: true,
                            physics: const ScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: data.length,
                            itemBuilder: (context, index) {
                              UsersModel usersModel = UsersModel(
                                  password: data[index]['password'],
                                  name: data[index]['name'],
                                  email: data[index]['email'],
                                  isOnline: data[index]['isOnline'],
                                  userId: data[index]['userId'],
                                  address: data[index]['address'],
                                  image: data[index]['image'],
                                  phone: data[index]['phone']);
                              late String searchTitle =
                                  usersModel.name.toString();
                              if (searchController.text.isEmpty) {
                                return Padding(
                                    padding: EdgeInsets.only(
                                        bottom: 0,
                                        left: 15,
                                        right: 15,
                                        top: 15),
                                    child: InkWell(
                                      onTap: () {
                                        Get.to(() => UserDetailScreen(
                                            email: usersModel.email.toString(),
                                            Phone: usersModel.phone.toString(),
                                            address:
                                                usersModel.address.toString(),
                                            userId:
                                                usersModel.userId.toString(),
                                            image: usersModel.image.toString(),
                                            name: usersModel.name.toString()));
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: constants.whiteColor,
                                            boxShadow: [
                                              BoxShadow(
                                                  offset: Offset(0, 5),
                                                  blurRadius: 10,
                                                  spreadRadius: 9,
                                                  blurStyle: BlurStyle.normal,
                                                  color: constants.darkRedColor
                                                      .withOpacity(0.1))
                                            ],
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: ListTile(
                                              // trailing: InkWell(
                                              //   onTap: () {
                                              //     // Get.to(() => MessageScreen(
                                              //     //       email: usersModel.email,
                                              //     //       name: usersModel.name,
                                              //     //       recieverId: usersModel.userId,
                                              //     //     ));
                                              //   },
                                              //   child: Icon(
                                              //     Icons.chat,
                                              //     color: constants.greenColor,
                                              //   ),
                                              // ),
                                              title: Text(
                                                overflow: TextOverflow.ellipsis,
                                                " ${usersModel.name.toString()}",
                                                style: TextStyle(
                                                    fontFamily: 'JosefinSans',
                                                    fontSize: 20),
                                              ),
                                              subtitle: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    width: width * 0.4,
                                                    child: Text(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      '${usersModel.email.toString()}',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'JosefinSans',
                                                      ),
                                                    ),
                                                  ),
                                                  // Container(
                                                  //   child: Text(
                                                  //     'Today',
                                                  //     style: TextStyle(
                                                  //         fontFamily:
                                                  //             'JosefinSans'),
                                                  //   ),
                                                  // )
                                                ],
                                              ),
                                              leading: usersModel.image == ''
                                                  ? Container(
                                                      child: Icon(
                                                        CupertinoIcons
                                                            .profile_circled,
                                                        size: 40,
                                                      ),
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle),
                                                    )
                                                  : Container(
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle),
                                                      child: CachedNetworkImage(
                                                        imageUrl: usersModel
                                                            .image
                                                            .toString(),
                                                        fit: BoxFit.fill,
                                                        height: 50,
                                                        width: 50,
                                                      ),
                                                    ))),
                                    ));
                              } else if (searchTitle.toLowerCase().contains(
                                  searchController.text.toLowerCase())) {
                                return Padding(
                                    padding: EdgeInsets.only(
                                        bottom: 0,
                                        left: 15,
                                        right: 15,
                                        top: 15),
                                    child: InkWell(
                                      onTap: () {
                                        Get.to(() => UserDetailScreen(
                                            email: usersModel.email.toString(),
                                            Phone: usersModel.phone.toString(),
                                            address:
                                                usersModel.address.toString(),
                                            userId:
                                                usersModel.userId.toString(),
                                            image: usersModel.image.toString(),
                                            name: usersModel.name.toString()));
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: constants.whiteColor,
                                            boxShadow: [
                                              BoxShadow(
                                                  offset: Offset(0, 5),
                                                  blurRadius: 10,
                                                  spreadRadius: 9,
                                                  blurStyle: BlurStyle.normal,
                                                  color: constants.darkRedColor
                                                      .withOpacity(0.1))
                                            ],
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: ListTile(
                                              title: Text(
                                                overflow: TextOverflow.ellipsis,
                                                " ${searchTitle.toString()}",
                                                style: TextStyle(
                                                    fontFamily: 'JosefinSans',
                                                    fontSize: 20),
                                              ),
                                              subtitle: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    width: width * 0.4,
                                                    child: Text(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      '${usersModel.email.toString()}',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'JosefinSans',
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              leading: usersModel.image == ''
                                                  ? Container(
                                                      child: Icon(
                                                        CupertinoIcons
                                                            .profile_circled,
                                                        size: 40,
                                                      ),
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle),
                                                    )
                                                  : Container(
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle),
                                                      child: CachedNetworkImage(
                                                        imageUrl: usersModel
                                                            .image
                                                            .toString(),
                                                        fit: BoxFit.fill,
                                                        height: 50,
                                                        width: 50,
                                                      ),
                                                    ))),
                                    ));
                              } else {
                                return Container();
                              }
                            }),
                      );

                    default:
                      return Container();
                  }
                }),
          ),
        ],
      ),
    );
  }
}
