import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Models/product_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/screens/view/Users/update_product.dart';
import 'package:final_year_project/screens/view/eco-mart/details_eco-martScreen.dart';
import 'package:final_year_project/screens/view/swapping/details_swapping_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../core/Utils/utils.dart';
import '../donation/details_donationScreen.dart';

class MyPostScreen extends StatefulWidget {
  const MyPostScreen({super.key});

  @override
  State<MyPostScreen> createState() => _MyPostScreenState();
}

class _MyPostScreenState extends State<MyPostScreen> {
  final auth = FirebaseAuth.instance.currentUser;

  final myPostsRef = FirebaseFirestore.instance.collection('User');
  Future moveToDonation(var id) async {
    final postRef = FirebaseFirestore.instance.collection('All Posts');
    final myPostsRef = FirebaseFirestore.instance.collection('User');

    var response = await postRef
        .doc(id)
        .update({'category': 'Donation'})
        .then((value) =>
            {Utils.toastMessages('Product successfully added to "Donations"')})
        .onError((error, stackTrace) => Utils.toastMessages(error.toString()));
    var response2 = await myPostsRef
        .doc(FirebaseAuth.instance.currentUser!.uid.toString())
        .collection('myPosts')
        .doc(id)
        .update({'category': 'Donation'})
        .then((value) => null)
        .onError((error, stackTrace) => null);
  }

  Future deleteProduct(
    var productId,
  ) async {
    await FirebaseFirestore.instance
        .collection('User')
        .doc(auth!.uid)
        .collection('myPosts')
        .doc(productId)
        .delete()
        .then((value) => log('deleted successfully'))
        .onError((error, stackTrace) => null);
    await FirebaseFirestore.instance
        .collection('All Posts')
        .doc(productId)
        .delete()
        .then((value) => Utils.toastMessages('Product deleted'))
        .onError((error, stackTrace) => null);
  }

  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;

    return Scaffold(
        appBar: CustomAppbar(
            title: 'My Posts',
            fontsize: Constants().appbarFontSize,
            fontFamily: 'JosefinSans',
            centerTile: true,
            automaticallyImplyLeading: true,
            backgroundColor: Constants().darkRedColor,
            textColor: Constants().whiteColor),
        body: Padding(
          padding: const EdgeInsets.all(3.0),
          child: StreamBuilder(
              stream: myPostsRef
                  .doc(auth!.uid.toString())
                  .collection('myPosts')
                  .snapshots(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(
                            color: Constants().darkRedColor,
                          ),
                        ],
                      ),
                    );
                  case ConnectionState.done:
                  case ConnectionState.active:
                    if (snapshot.hasData) {
                      var data = snapshot.data!.docs
                          as List<QueryDocumentSnapshot<Map<String, dynamic>>>;
                      return Container(
                          height: height,
                          child: ListView.builder(
                              itemCount: data.length,
                              // shrinkWrap: true,
                              // primary: true,
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                var data = snapshot.data!.docs;
                                ProductModel productModel = ProductModel(
                                    image: data[index]['image'],
                                    price: data[index]['price'],
                                    name: data[index]['name'],
                                    rating: (data[index]['rating'] as num)
                                        .toDouble(),
                                    description: data[index]['description'],
                                    productId: data[index]['id'],
                                    category: data[index]['category'],
                                    userName: data[index]['userName'],
                                    userId: data[index]['userId'],
                                    email: data[index]['email'],
                                    conditionDropDownValue: data[index]
                                        ['condition'],
                                    organization: data[index]['organization'],
                                    serviceName: data[index]['serviceName'],
                                    amTime: data[index]['amTime'],
                                    pmTime: data[index]['pmTime']);
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    // height: height,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Constants()
                                                .darkRedColor
                                                .withOpacity(0.1),
                                            blurRadius: 0.5,
                                            spreadRadius: 0.5)
                                      ],
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        Get.to(() => DonationDetailsScreen(
                                              title: productModel.name,
                                              rating: productModel.rating,
                                              price: productModel.price,
                                              category: productModel.category,
                                              image: productModel.image,
                                              description:
                                                  productModel.description,
                                              userName: productModel.userName,
                                              email: productModel.email,
                                              productId: productModel.productId,
                                              userId: productModel.userId,
                                              condition: productModel
                                                  .conditionDropDownValue,
                                              country: productModel.country,
                                              city: productModel.city,
                                              state: productModel.state,
                                              organization:
                                                  productModel.organization,
                                              serviceName:
                                                  productModel.serviceName,
                                            ));
                                      },
                                      child: Container(
                                        // height: Get.height * 0.09,
                                        child: ListTile(
                                          trailing: Column(
                                            children: [
                                              Container(
                                                // alignment: Alignment,
                                                height: 20,
                                                child: PopupMenuButton(
                                                    surfaceTintColor:
                                                        constants.blackColor,
                                                    // iconColor:
                                                    iconColor: Colors.black,
                                                    icon: Icon(
                                                      Icons.more_vert,
                                                      color:
                                                          constants.blackColor,
                                                      size: 20,
                                                    ),
                                                    //     Colors.black,
                                                    // color: Colors.black,
                                                    onSelected: (value) async {
                                                      if (value == '0') {
                                                        moveToDonation(
                                                            productModel
                                                                .productId);
                                                        log(productModel
                                                            .productId
                                                            .toString());
                                                      } else if (value == '1') {
                                                        deleteProduct(
                                                            productModel
                                                                .productId);
                                                      } else if (value ==
                                                          '2') {}
                                                      print(value);
                                                    },
                                                    itemBuilder: (context) {
                                                      return [
                                                        PopupMenuItem(
                                                          value: '0',
                                                          onTap: () async {
                                                            moveToDonation(
                                                                productModel
                                                                    .productId);
                                                            log(productModel
                                                                .productId
                                                                .toString());
                                                          },
                                                          child: Container(
                                                            child: Text(
                                                                'Move to donations'),
                                                          ),
                                                        ),
                                                        PopupMenuItem(
                                                          value: '1',
                                                          onTap: () async {
                                                            deleteProduct(
                                                                productModel
                                                                    .productId
                                                                    .toString());
                                                          },
                                                          child: Container(
                                                            child:
                                                                Text('Delete'),
                                                          ),
                                                        ),
                                                        PopupMenuItem(
                                                          value: '2',
                                                          onTap: () async {
                                                            Get.to(() => UpdateProductScreen(
                                                                title: productModel
                                                                    .name,
                                                                price: productModel
                                                                    .price,
                                                                description:
                                                                    productModel
                                                                        .description,
                                                                category:
                                                                    productModel
                                                                        .category,
                                                                email: productModel
                                                                    .email,
                                                                image:
                                                                    productModel
                                                                        .image,
                                                                productId:
                                                                    productModel
                                                                        .productId,
                                                                userId:
                                                                    productModel
                                                                        .userId,
                                                                userName:
                                                                    productModel
                                                                        .userName));
                                                            // await  FirebaseDatabase.instance.
                                                          },
                                                          child: Container(
                                                            child:
                                                                Text('Update'),
                                                          ),
                                                        )
                                                      ];
                                                    }),
                                              ),
                                              SizedBox(
                                                height: Get.height * 0.017,
                                              ),
                                              Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              3),
                                                      color: Constants()
                                                          .darkRedColor),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            3.0),
                                                    child: Text(
                                                      '${productModel.category}',
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                          title: Text(
                                            productModel.name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500),
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                productModel.description,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                // crossAxisAlignment:
                                                //     CrossAxisAlignment,
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      'By: ${productModel.userName}',
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          leading: Container(
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                      productModel.image,
                                                    ),
                                                    fit: BoxFit.cover),
                                                // color: Colors.grey,
                                                // color: Colors.red,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            height: height,
                                            width: width * 0.28,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                                // return Padding(
                                //   padding: const EdgeInsets.all(3.0),
                                //   child: Card(
                                //     shadowColor: Constants()
                                //         .darkRedColor
                                //         .withOpacity(0.1),
                                //     elevation: 10,
                                //     child: Container(
                                //       decoration: BoxDecoration(
                                //           // color: Colors.grey,
                                //           borderRadius:
                                //               BorderRadius.circular(10)),
                                //       width: double.infinity,
                                //       height: Get.height * 0.1,
                                //       child: Row(
                                //         children: [
                                //           Container(
                                //             decoration: BoxDecoration(
                                //                 image: DecorationImage(
                                //                     image: NetworkImage(
                                //                       productModel.image,
                                //                     ),
                                //                     fit: BoxFit.cover),
                                //                 // color: Colors.grey,
                                //                 // color: Colors.red,
                                //                 borderRadius:
                                //                     BorderRadius.circular(10)),
                                //             height: height,
                                //             width: width * 0.28,
                                //           ),
                                //           Expanded(
                                //             child: Container(
                                //               height: height,
                                //               child: Row(
                                //                 mainAxisAlignment:
                                //                     MainAxisAlignment
                                //                         .spaceBetween,
                                //                 children: [
                                //                   Container(
                                //                     child: Column(
                                //                       children: [
                                //                         Padding(
                                //                           padding:
                                //                               const EdgeInsets
                                //                                   .only(
                                //                                   left: 10.0,
                                //                                   top: 5),
                                //                           child: Container(
                                //                             alignment: Alignment
                                //                                 .topLeft,
                                //                             child: Text(
                                //                               productModel.name,
                                //                               style: TextStyle(
                                //                                   fontWeight:
                                //                                       FontWeight
                                //                                           .w600),
                                //                             ),
                                //                           ),
                                //                         ),
                                //                         Row(
                                //                           children: [
                                //                             Container(
                                //                               width: Get.width *
                                //                                   0.35,
                                //                               child: Text(
                                //                                 '',
                                //                                 style: TextStyle(
                                //                                     overflow:
                                //                                         TextOverflow
                                //                                             .ellipsis),
                                //                                 overflow:
                                //                                     TextOverflow
                                //                                         .ellipsis,
                                //                               ),
                                //                             ),
                                //                             SizedBox(
                                //                               width: 5,
                                //                             ),
                                //                             Container(
                                //                                 decoration: BoxDecoration(
                                //                                     color: Constants()
                                //                                         .darkRedColor),
                                //                                 child: Text(
                                //                                   '${productModel.category}',
                                //                                   style: TextStyle(
                                //                                       color: Colors
                                //                                           .white),
                                //                                 ))
                                //                           ],
                                //                         )
                                //                       ],
                                //                     ),
                                //                   ),
                                //                   Container(
                                //                     child: IconButton(
                                //                         onPressed: () {},
                                //                         icon: Icon(
                                //                           Icons.menu,
                                //                           color: Colors.black,
                                //                         )),
                                //                   )
                                //                 ],
                                //               ),

                                //               // color: Colors.grey,
                                //             ),
                                //           )
                                //         ],
                                //       ),
                                //     ),
                                //   ),
                                // );
                              }));
                    } else {
                      return Container(
                        child: Text('Sorry no any post available'),
                      );
                    }

                  default:
                }
                return Container(
                  child: Text('You are not uploaded any post yet'),
                );
              }),
        ));
  }
}
