import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_state_city_picker/country_state_city_picker.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../Models/users_model.dart';
import '../../../getx_logics/logics.dart';
import '../../../resources/widgets/roundButton.dart';
import '../../../resources/widgets/textFormField.dart';
import '../../../services/post_services/post_services.dart';

class UpdateProductScreen extends StatefulWidget {
  var title;
  var description;
  var image;
  var productId;
  var price;
  var email;
  var userName;
  // var productId;
  var userId;
  var category;
  // var rating;
  UpdateProductScreen(
      {super.key,
      required this.title,
      required this.price,
      required this.description,
      required this.category,
      required this.email,
      required this.image,
      required this.productId,
      required this.userId,
      required this.userName});

  @override
  State<UpdateProductScreen> createState() => _UpdateProductScreenState();
}

class _UpdateProductScreenState extends State<UpdateProductScreen> {
  LogicsController controller = Get.put(LogicsController());
  PostServices services = Get.put(PostServices());
  String countryValue = '';
  String stateValue = '';
  String cityValue = '';
  Constants constants = Constants();
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final auth = FirebaseAuth.instance;

  final userRef = FirebaseFirestore.instance.collection('User');
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Update Product',
        automaticallyImplyLeading: true,
        backgroundColor: Constants().darkRedColor,
        textColor: Constants().whiteColor,
        centerTile: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              CustomTextFormField(
                controller: nameController,
                hintText: 'Title',
                fontFamily: 'JosefinSans',
                labelText: 'Ad Title',
              ),
              SizedBox(
                height: 10,
              ),
              // Obx(() {
              //   return InkWell(
              //     onTap: () {
              //       controller.dropdownFun();
              //     },
              //     child: Container(
              //       height: height * 0.07,
              //       width: width,
              //       decoration: BoxDecoration(
              //           color: constants.greyColor,
              //           borderRadius: BorderRadius.only(
              //               topLeft: Radius.circular(10),
              //               topRight: Radius.circular(10))),
              //       // borderRadius: BorderRadius.circular(10)),
              //       alignment: Alignment.centerLeft,
              //       child: Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 10.0),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Text(
              //               controller.isSelectedValue.toString(),
              //               style: TextStyle(fontFamily: 'JosefinSans'),
              //             ),
              //             controller.isOpen == true
              //                 ? Icon(Icons.keyboard_arrow_up)
              //                 : Icon(Icons.keyboard_arrow_down_rounded)
              //           ],
              //         ),
              //       ),
              //     ),
              //   );
              // }),

              // Obx(() {
              //   if (controller.isOpen == true) {
              //     return Container(
              //       height: height * 0.19,
              //       width: width,
              //       decoration: BoxDecoration(
              //           color: constants.greyColor,
              //           borderRadius: BorderRadius.only(
              //               bottomLeft: Radius.circular(10),
              //               bottomRight: Radius.circular(10))),
              //       child: ListView.builder(
              //           itemCount: controller.dropdownValues.length,
              //           shrinkWrap: true,
              //           primary: true,
              //           itemBuilder: (context, index) {
              //             return Padding(
              //               padding: const EdgeInsets.only(left: 10.0, top: 10),
              //               child: InkWell(
              //                 onTap: () {
              //                   controller.settingValues(index);
              //                 },
              //                 child: Text(
              //                   controller.dropdownValues[index].toString(),
              //                   style: TextStyle(fontFamily: 'JosefinSans'),
              //                 ),
              //               ),
              //             );
              //           }),
              //     );
              //   } else {
              //     return SizedBox();
              //   }
              // }),

              SizedBox(
                height: 10,
              ),
              if (controller.isSelectedValue.value == 'Swapping')
                Container(
                  child: Column(
                    children: [
                      Container(
                        child: SelectState(onCountryChanged: (countryVal) {
                          countryValue = countryVal;
                          setState(() {});
                        }, onStateChanged: (stateVal) {
                          setState(() {
                            stateValue = stateVal;
                          });
                        }, onCityChanged: (cityVal) {
                          setState(() {
                            cityValue = cityVal;
                          });
                        }),
                      )
                    ],
                  ),
                ),
              SizedBox(
                height: 10,
              ),
              if (widget.category == 'Eco-Mart')
                CustomTextFormField(
                  fontFamily: 'JosefinSans',
                  controller: priceController,
                  hintText: 'Price',
                  labelText: 'Price',
                ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  controller.getGalleryImage();
                },
                child: Obx(() {
                  if (controller.image.value.isEmpty) {
                    return Container(
                      height: height * 0.35,
                      width: width,
                      decoration: BoxDecoration(
                          color: constants.greyColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              CupertinoIcons.photo,
                              size: 40,
                            ),
                            Text(
                              'No Image Selected',
                              style: TextStyle(
                                  fontSize: 22, fontFamily: 'JosefinSans'),
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Container(
                      child: Image.file(
                        File(controller.image.value.toString()),
                        fit: BoxFit.fill,
                        height: height * 0.35,
                        width: width,
                      ),
                    );
                  }
                }),
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextFormField(
                controller: descriptionController,
                hintText: 'Description',
                fontFamily: 'JosefinSans',
                labelText: 'Description',
                maxLinse: 7,
              ),
              SizedBox(
                height: 20,
              ),

              SizedBox(
                height: 20,
              ),
              CustomButton(
                fontFamily: 'JosefinSans',
                title: 'Update',
                onTap: () {
                  services.updatePost(
                      nameController.text.toString(),
                      priceController.text.toString(),
                      // controller.isSelectedValue.value.toString(),
                      descriptionController.text.toString(),
                      controller.image.value.toString(),
                      widget.productId);
                },
                radius: 10,
                backGroundColor: constants.darkRedColor,
                buttonTextColor: constants.whiteColor,
                width: width,
                // isLoading: controller.isLoading.value,
                fontSize: 18,
                height: height * 0.06,
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
