// ignore_for_file: unused_import, unused_local_variable, unnecessary_import, prefer_typing_uninitialized_variables, avoid_unnecessary_containers

import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/main.dart';
import 'package:final_year_project/resources/widgets/textFormField.dart';
// import 'package:final_year_project/screens/view/Users/profile_data_screen.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/image_picker.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/screens/view/Users/user_profileDetails.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Constants constants = Constants();

  User? user;
  final auth = FirebaseAuth.instance.currentUser;
  final userRef = FirebaseFirestore.instance.collection('User/');
  File? imageFile;

  // final ref = FirebaseDatabase.instance.ref('User/');

  List<String> subtitle = [
    'Athar Khan',
    '+923001345678',
    'buyer123@gmail.com',
    'buyerAddress G-2 block Islamabad',
    'buyerPasswrord'
  ];
  List<String> title = ['Name', 'Phone', 'Address', 'Email', 'Password'];

  // getImageFromGallery() async {
  //   final image = await picker.pickImage(source: ImageSource.gallery);

  //   if (image != null) {
  //     imagePath = image.path.toString();
  //   }
  //   // ;
  //   setState(() {});
  // }
  bool isLoadig = false;
  // var auth;
  // void updateProfile(String name, String phone, String address, String email,
  //     String password) async {
  //   isLoadig = true;
  //   setState(() {});
  //   try {
  //     isLoadig = true;
  //     setState(() {});
  //     final response = await ref.child(auth!.uid).update({
  //       "address": address.toString(),
  //       "email": email.toString(),
  //       "name": name.toString(),
  //       "password": password.toString(),
  //       "phone": phone.toString()
  //     }).then((value) {
  //       isLoadig = false;
  //       setState(() {});
  //       return Utils.toastMessages('Profile update successfully');
  //     }).onError((error, stackTrace) {
  //       isLoadig = false;
  //       setState(() {});
  //       return Utils.toastMessages(error.toString());
  //     });
  //   } catch (e) {
  //     isLoadig = false;
  //     setState(() {});
  //     Utils.toastMessages(e.toString());
  //   }
  // }

  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  UsersModel usersClass = UsersModel();
  var userData;
  final _formKey = GlobalKey<FormState>();
  // bool isLoading = false;
  // Constants constants = Constants();
  // final auth = FirebaseAuth.instance.currentUser;
  // File? imageFile;
  String? initialValue;
  bool isLoading = false;
  bool isLoading1 = false;
  bool updateLoading = false;
  // @override
  // void initState() {
  //   super.initState();
  //   APIs().getUserCurrentData();
  // }

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return ModalProgressHUD(
      inAsyncCall: updateLoading,
      color: constants.greyColor,
      progressIndicator: CircularProgressIndicator(
        color: constants.darkRedColor,
      ),
      child: Scaffold(
          // appBar: AppBar(title: Text(''),),
          appBar: CustomAppbar(
            title: 'Profile',
            fontFamily: 'JosefinSans',
            automaticallyImplyLeading: false,
            backgroundColor: constants.darkRedColor,
            textColor: constants.whiteColor,
            // trailing: [
            //   PopupMenuButton(
            //     // initialValue:,
            //     iconColor: constants.whiteColor,
            //     itemBuilder: (context) {
            //       return [
            //         PopupMenuItem(
            //           child: Text(
            //             "Update Profile",
            //             style: TextStyle(fontFamily: 'BubblgumSans'),
            //           ),
            //           value: '\hello',
            //           onTap: () {
            //             Get.defaultDialog(
            //                 title: 'Profile',
            //                 textConfirm: 'Save',
            //                 onCancel: () {},
            //                 onConfirm: () {
            //                   Get.back();
            //                   // Navigator.pop(context);
            //                 },
            //                 titleStyle: TextStyle(
            //                     fontFamily: 'JosefinSans',
            //                     color: constants.darkRedColor),
            //                 content: Container(
            //                     height: Get.height * 0.35,
            //                     child: SingleChildScrollView(
            //                       child: Column(
            //                         children: [
            //                           CustomTextFormField(
            //                             controller: nameController,
            //                             hintText: 'Name',
            //                             labelText: 'Name',
            //                           ),
            //                           SizedBox(
            //                             height: 10,
            //                           ),
            //                           CustomTextFormField(
            //                               // ,
            //                               hintText: 'Phone',
            //                               labelText: 'Phone',
            //                               controller: phoneController),
            //                           SizedBox(
            //                             height: 10,
            //                           ),
            //                           CustomTextFormField(
            //                               // ,
            //                               hintText: 'Address',
            //                               labelText: 'Address',
            //                               controller: addressController),
            //                           SizedBox(
            //                             height: 10,
            //                           ),
            //                           CustomTextFormField(
            //                               // ,
            //                               hintText: 'email',
            //                               labelText: 'email',
            //                               controller: emailController),
            //                           SizedBox(
            //                             height: 10,
            //                           ),
            //                           CustomTextFormField(
            //                               // ,
            //                               hintText: 'Password',
            //                               labelText: 'Password',
            //                               controller: passwordController),
            //                           SizedBox(
            //                             height: 10,
            //                           ),
            //                         ],
            //                       ),
            //                     )));
            //           },
            //         ),
            //       ];
            //     },
            //   )
            // ],
          ),
          body: ModalProgressHUD(
              inAsyncCall: isLoadig,
              color: constants.darkRedColor,
              child: StreamBuilder(
                  stream: APIs.userRef.doc(currentUser!.uid).snapshots(),
                  builder: (context, snapshot) {
                    // data.map((e)=>Use)
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                      case ConnectionState.none:
                        return Center(
                          child: Container(
                            child: CircularProgressIndicator(
                              color: Constants().darkRedColor,
                            ),
                          ),
                        );
                      case ConnectionState.active:
                      case ConnectionState.done:
                        // userData = snapshot.data!.data();
                        // var newData = UsersClass.fromJson(userData);
                        var data = snapshot.data!.data();

                        if (snapshot.hasData) {
                          return Container(
                              child: SingleChildScrollView(
                            child: Form(
                              key: _formKey,
                              child: Column(children: [
                                Container(
                                    height: height,
                                    width: width,
                                    child: Column(children: [
                                      Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          PickImageWidget(
                                            isLoading: isLoading,
                                            onPickedImage: (fileImage) async {
                                              imageFile = fileImage;
                                              final postReference =
                                                  FirebaseFirestore.instance
                                                      .collection(
                                                          "User/${currentUser!.uid}/picture/");
                                              final postReference1 =
                                                  FirebaseFirestore.instance
                                                      .collection("User");

                                              firebase_storage.Reference
                                                  reference = firebase_storage
                                                      .FirebaseStorage.instance
                                                      .ref("User/Profile" +
                                                          "${currentUser!.uid.toString()}");
                                              try {
                                                setState(() {
                                                  isLoading = true;
                                                });
                                                // controller.isLoading.value = true;
                                                firebase_storage.UploadTask
                                                    uploadTask =
                                                    reference.putFile(File(
                                                        imageFile!.path
                                                            .toString()));
                                                await Future.value(uploadTask)
                                                    .then((value) {})
                                                    .onError(
                                                        (error, stackTrace) {
                                                  print(error.toString());

                                                  Utils.snackBar("Image",
                                                      "${error.toString()}");
                                                });
                                                var newUrl = await reference
                                                    .getDownloadURL();
                                                // controller.isLoading.value = true;
                                                await postReference
                                                    .doc(currentUser!.uid)
                                                    .update({
                                                  "image": newUrl.toString(),
                                                }).then((value) {
                                                  // controller.isLoading.value = false;
                                                  setState(() {
                                                    isLoading = false;
                                                  });
                                                  Utils.snackBar("Post",
                                                      "Profile Picture update Successfully😍");
                                                  // navigator!.pushReplacement(
                                                  //     MaterialPageRoute(builder: (context) => MainScreen()));
                                                }).onError((error, stackTrace) {
                                                  // controller.isLoading.value = false;
                                                  setState(() {
                                                    isLoading = false;
                                                  });
                                                  print(error.toString());

                                                  Utils.snackBar(
                                                      "Post", error.toString());
                                                });
                                                await postReference1
                                                    .doc(currentUser!.uid)
                                                    .update({
                                                  "image": newUrl.toString(),
                                                }).then((value) {
                                                  // controller.isLoading.value = false;
                                                  setState(() {
                                                    isLoading = false;
                                                  });
                                                  Utils.snackBar("Post",
                                                      "Profile Picture update Successfully😍");
                                                  // navigator!.pushReplacement(
                                                  //     MaterialPageRoute(builder: (context) => MainScreen()));
                                                }).onError((error, stackTrace) {
                                                  // controller.isLoading.value = false;
                                                  setState(() {
                                                    isLoading = false;
                                                  });
                                                  print(error.toString());

                                                  Utils.snackBar(
                                                      "Post", error.toString());
                                                });
                                              } catch (e) {
                                                // controller.isLoading.value = false;
                                                setState(() {
                                                  isLoading = false;
                                                });
                                                print(e.toString());
                                                Utils.snackBar(
                                                    "Post", e.toString());
                                              }
                                              setState(() {});
                                            },
                                          ),
                                          Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.48,
                                            child: SingleChildScrollView(
                                              child: Column(
                                                children: [
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text('Points: 1'),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Container(
                                                      child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: Container(
                                                              width: double
                                                                  .infinity,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10.0),
                                                                  color: Colors
                                                                      .white,
                                                                  boxShadow: [
                                                                    BoxShadow(
                                                                        offset: Offset(
                                                                            0,
                                                                            5),
                                                                        blurRadius:
                                                                            10,
                                                                        spreadRadius:
                                                                            9,
                                                                        blurStyle:
                                                                            BlurStyle
                                                                                .normal,
                                                                        color: constants
                                                                            .darkRedColor
                                                                            .withOpacity(0.1))
                                                                  ]),
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(
                                                                        18.0),
                                                                child: Row(
                                                                  children: [
                                                                    Icon(
                                                                      CupertinoIcons
                                                                          .person,
                                                                      color: constants
                                                                          .darkRedColor,
                                                                    ),
                                                                    SizedBox(
                                                                      width: 15,
                                                                    ),
                                                                    Text(data![
                                                                            'name']
                                                                        .toString()),
                                                                  ],
                                                                ),
                                                              )))),
                                                  Container(
                                                      child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: Container(
                                                              width: double
                                                                  .infinity,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10.0),
                                                                  color: Colors
                                                                      .white,
                                                                  boxShadow: [
                                                                    BoxShadow(
                                                                        offset: Offset(
                                                                            0,
                                                                            5),
                                                                        blurRadius:
                                                                            10,
                                                                        spreadRadius:
                                                                            9,
                                                                        blurStyle:
                                                                            BlurStyle
                                                                                .normal,
                                                                        color: constants
                                                                            .darkRedColor
                                                                            .withOpacity(0.1))
                                                                  ]),
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(
                                                                        18.0),
                                                                child: Row(
                                                                  children: [
                                                                    Icon(
                                                                      CupertinoIcons
                                                                          .phone,
                                                                      color: constants
                                                                          .darkRedColor,
                                                                    ),
                                                                    SizedBox(
                                                                      width: 15,
                                                                    ),
                                                                    Text(data[
                                                                            'phone']
                                                                        .toString()),
                                                                  ],
                                                                ),
                                                              )))),
                                                  Container(
                                                      child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: Container(
                                                        width: double.infinity,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10.0),
                                                            color: Colors.white,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                  offset:
                                                                      Offset(
                                                                          0, 5),
                                                                  blurRadius:
                                                                      10,
                                                                  spreadRadius:
                                                                      9,
                                                                  blurStyle:
                                                                      BlurStyle
                                                                          .normal,
                                                                  color: constants
                                                                      .darkRedColor
                                                                      .withOpacity(
                                                                          0.1))
                                                            ]),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(18.0),
                                                          child: Row(
                                                            children: [
                                                              Icon(
                                                                CupertinoIcons
                                                                    .location,
                                                                color: constants
                                                                    .darkRedColor,
                                                              ),
                                                              SizedBox(
                                                                width: 15,
                                                              ),
                                                              Container(
                                                                width:
                                                                    width * 0.7,
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: Text(
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  '${data['address'].toString()}',
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        )),
                                                  )),
                                                  Container(
                                                      child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: Container(
                                                        // height: MediaQuery.of(context).size.height * 0.5,
                                                        width: double.infinity,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10.0),
                                                            color: Colors.white,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                  offset:
                                                                      Offset(
                                                                          0, 5),
                                                                  blurRadius:
                                                                      10,
                                                                  spreadRadius:
                                                                      9,
                                                                  blurStyle:
                                                                      BlurStyle
                                                                          .normal,
                                                                  color: constants
                                                                      .darkRedColor
                                                                      .withOpacity(
                                                                          0.1))
                                                            ]),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(18.0),
                                                          child: Row(
                                                            children: [
                                                              Icon(
                                                                CupertinoIcons
                                                                    .mail,
                                                                color: constants
                                                                    .darkRedColor,
                                                              ),
                                                              SizedBox(
                                                                width: 15,
                                                              ),
                                                              Text(data['email']
                                                                  .toString()),
                                                            ],
                                                          ),
                                                        )),
                                                  )),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: CustomButton(
                                          height: height * 0.06,
                                          title: 'Update',
                                          buttonTextColor:
                                              Constants().whiteColor,
                                          onTap: () async {
                                            Get.defaultDialog(
                                                onCancel: () {},
                                                confirm: CustomButton(
                                                    backGroundColor:
                                                        constants.darkRedColor,
                                                    buttonTextColor:
                                                        constants.whiteColor,
                                                    width: width * 0.19,
                                                    height: height * 0.05,
                                                    title: 'Update',
                                                    onTap: () async {
                                                      String name =
                                                          nameController.text;
                                                      String phone =
                                                          phoneController.text;
                                                      String address =
                                                          addressController
                                                              .text;
                                                      String email =
                                                          emailController.text;
                                                      if (name.isEmpty ||
                                                          email.isEmpty ||
                                                          phone.isEmpty ||
                                                          address.isEmpty) {
                                                        return Utils.snackBar(
                                                            'Error',
                                                            'All fields are required');
                                                      } else {
                                                        Map<String, dynamic>
                                                            updateProfileData =
                                                            {
                                                          "address":
                                                              address.trim(),
                                                          'email': email.trim(),
                                                          'isOnline': '',
                                                          'name': name.trim(),
                                                          'password': APIs
                                                              .me!.password
                                                              .toString(),
                                                          'phone': phone.trim(),
                                                          'userId': auth!.uid
                                                              .toString()
                                                        };
                                                        setState(() {
                                                          updateLoading = true;
                                                        });
                                                        await userRef
                                                            .doc(currentUser!
                                                                .uid)
                                                            .update(
                                                                updateProfileData)
                                                            .then((value) {
                                                          setState(() {
                                                            updateLoading =
                                                                false;
                                                          });
                                                          Get.back();
                                                          Utils.snackBar(
                                                              'Congragulations',
                                                              'Profile update successfully');
                                                        });
                                                      }
                                                    },
                                                    radius: 20),
                                                title: 'Profile',
                                                titlePadding:
                                                    EdgeInsets.all(10),
                                                content: Flexible(
                                                  child: Container(
                                                    height: height * 0.45,
                                                    width: width,
                                                    child:
                                                        SingleChildScrollView(
                                                      child: Column(
                                                        children: [
                                                          CustomTextFormField(
                                                            hintText: 'Name',
                                                            labelText: 'Name',
                                                            controller:
                                                                nameController,
                                                            prefixIcon: Icon(
                                                              CupertinoIcons
                                                                  .person,
                                                              color: constants
                                                                  .darkRedColor,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          CustomTextFormField(
                                                            hintText: 'Phone',
                                                            labelText: 'Phone',
                                                            controller:
                                                                phoneController,
                                                            prefixIcon: Icon(
                                                              CupertinoIcons
                                                                  .phone,
                                                              color: constants
                                                                  .darkRedColor,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          CustomTextFormField(
                                                            hintText: 'Address',
                                                            labelText:
                                                                'Address',
                                                            controller:
                                                                addressController,
                                                            prefixIcon: Icon(
                                                              CupertinoIcons
                                                                  .location,
                                                              color: constants
                                                                  .darkRedColor,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          CustomTextFormField(
                                                            hintText: 'Email',
                                                            labelText: 'Email',
                                                            controller:
                                                                emailController,
                                                            prefixIcon: Icon(
                                                              CupertinoIcons
                                                                  .mail,
                                                              color: constants
                                                                  .darkRedColor,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          // CustomTextFormField(
                                                          //   hintText:
                                                          //       'Password',
                                                          //   labelText:
                                                          //       'Password',
                                                          //   controller:
                                                          //       emailController,
                                                          //   prefixIcon: Icon(
                                                          //     CupertinoIcons
                                                          //         .mail,
                                                          //     color: constants
                                                          //         .darkRedColor,
                                                          //   ),
                                                          // )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ));
                                          },
                                          radius: 10,
                                          backGroundColor:
                                              Constants().darkRedColor,
                                          fontSize: 17,
                                          fontFamily: 'JosefinSans',
                                        ),
                                      ),
                                    ])),
                                SizedBox(
                                    // height: 20,
                                    )
                              ]),
                            ),
                          ));
                        } else {
                          return Center(
                              child: CircularProgressIndicator(
                            color: Constants().darkRedColor,
                          ));
                        }

                      //  / bre;ak;
                      default:
                    }
                    return SizedBox();
                  }))),
    );
  }
}
