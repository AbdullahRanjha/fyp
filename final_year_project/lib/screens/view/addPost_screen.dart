// ignore_for_file: unused_import, file_names, prefer_const_constructors, unrelated_type_equality_checks, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers

import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/resources/widgets/textFormField.dart';
import 'package:final_year_project/services/post_services/post_services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:country_state_city_picker/country_state_city_picker.dart';

class AddPostScreen extends StatefulWidget {
  const AddPostScreen({super.key});

  @override
  State<AddPostScreen> createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  Constants constants = Constants();
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController organizationController = TextEditingController();
  TextEditingController serviceNameController = TextEditingController();
  final auth = FirebaseAuth.instance;

  final userRef = FirebaseFirestore.instance.collection('User');

  // Future<List<QueryDocumentSnapshot>> getUserData() async {
  //   final usersData =
  //       await userRef.where('userId', isEqualTo: auth.currentUser!.uid).get();
  //   return usersData.docs;
  // }

  // var userData;
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();

  //   userData = getUserData();
  // }

  // final userData = getUserData();
  LogicsController controller = Get.put(LogicsController());
  PostServices services = Get.put(PostServices());
  String countryValue = '';
  String stateValue = '';
  String cityValue = '';
  List<String> dropDownValues1 = ['New', 'Used'];
  bool tappedValue = false;
  String conditionDropDownValue = 'Condition';
  TimeOfDay selectAmTime = TimeOfDay.now();
  TimeOfDay selectPmTime = TimeOfDay.now();

  void dropDownFun2() {
    tappedValue = !tappedValue;
    print(tappedValue);
  }

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;

    return Obx(() {
      return ModalProgressHUD(
          inAsyncCall: controller.isLoading.value,
          color: constants.greyColor,
          progressIndicator: CircularProgressIndicator(
            color: constants.darkRedColor,
          ),
          child: Scaffold(
            backgroundColor: constants.whiteColor,
            // bottomNavigationBar: Padding(
            //   padding: const EdgeInsets.only(bottom: 10.0, top: 10),
            //   child: Container(
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: ),
            // ),
            appBar: CustomAppbar(
              title: 'Add Post',
              fontFamily: 'JosefinSans',
              textColor: constants.whiteColor,
              // leading: IconButton(
              //     onPressed: () {
              //       Get.back();
              //     },
              //     icon: Icon(
              //       CupertinoIcons.back,
              //       color: constants.whiteColor,
              //     )),
              automaticallyImplyLeading: false,
              backgroundColor: constants.darkRedColor,
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextFormField(
                      controller: nameController,
                      hintText: 'Title',
                      fontFamily: 'JosefinSans',
                      labelText: 'Ad Title',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Obx(() {
                      return InkWell(
                        onTap: () {
                          controller.dropdownFun();
                        },
                        child: Container(
                          height: height * 0.07,
                          width: width,
                          decoration: BoxDecoration(
                              color: constants.greyColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10))),
                          // borderRadius: BorderRadius.circular(10)),
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  controller.isSelectedValue.toString(),
                                  style: TextStyle(fontFamily: 'JosefinSans'),
                                ),
                                controller.isOpen == true
                                    ? Icon(Icons.keyboard_arrow_up)
                                    : Icon(Icons.keyboard_arrow_down_rounded)
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                    Obx(() {
                      if (controller.isOpen == true) {
                        return Container(
                          height: height * 0.19,
                          width: width,
                          decoration: BoxDecoration(
                              // color: constants.greyColor,
                              color: constants.lightGrey,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10))),
                          child: ListView.builder(
                              itemCount: controller.dropdownValues.length,
                              shrinkWrap: true,
                              primary: true,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 10),
                                  child: InkWell(
                                    onTap: () {
                                      controller.settingValues(index);
                                    },
                                    child: Text(
                                      controller.dropdownValues[index]
                                          .toString(),
                                      style:
                                          TextStyle(fontFamily: 'JosefinSans'),
                                    ),
                                  ),
                                );
                              }),
                        );
                      } else {
                        return SizedBox();
                      }
                    }),
                    SizedBox(
                      height: 10,
                    ),
                    // if (controller.isSelectedValue.value == 'Swapping')
                    Container(
                      child: Column(
                        children: [
                          Container(
                            child: SelectState(onCountryChanged: (countryVal) {
                              countryValue = countryVal;
                              setState(() {});
                            }, onStateChanged: (stateVal) {
                              setState(() {
                                stateValue = stateVal;
                              });
                            }, onCityChanged: (cityVal) {
                              setState(() {
                                cityValue = cityVal;
                              });
                            }),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    if (controller.isSelectedValue.value == 'Eco-Mart')
                      CustomTextFormField(
                        fontFamily: 'JosefinSans',
                        controller: priceController,
                        hintText: 'Price',
                        labelText: 'Price',
                      ),
                    if (controller.isSelectedValue.value == 'Eco-Mart')
                      SizedBox(
                        height: 10,
                      ),
                    if (controller.isSelectedValue.value == 'Re-Cycling')
                      CustomTextFormField(
                        fontFamily: 'JosefinSans',
                        controller: organizationController,
                        hintText: 'Organization',
                        labelText: 'Organization',
                      ),
                    if (controller.isSelectedValue.value == 'Re-Cycling')
                      SizedBox(
                        height: 10,
                      ),
                    if (controller.isSelectedValue.value == 'Re-Cycling')
                      CustomTextFormField(
                        fontFamily: 'JosefinSans',
                        controller: serviceNameController,
                        hintText: 'Service Name',
                        labelText: 'Service Name',
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    // if (logicsController().isSelectedValue == 'Eco-Mart')
                    if (controller.isSelectedValue.value == 'Re-Cycling')
                      Container(
                        decoration: BoxDecoration(
                            color: constants.greyColor,
                            borderRadius: BorderRadius.circular(10)),
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Select Availability'),
                        ),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    if (controller.isSelectedValue.value == 'Re-Cycling')
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () async {
                                final TimeOfDay? timeOfDay =
                                    await showTimePicker(
                                        context: context,
                                        initialTime: selectAmTime,
                                        initialEntryMode:
                                            TimePickerEntryMode.dial);
                                if (timeOfDay != null) {
                                  setState(() {
                                    selectAmTime = timeOfDay;
                                  });
                                }
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: constants.lightGrey),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                            '${selectAmTime.hour}:${selectAmTime.minute}'),
                                      ),
                                    ),
                                    Container(
                                        decoration: BoxDecoration(
                                            color: constants.greyColor),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('AM'),
                                        )),
                                    SizedBox(
                                      width: 40,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 40,
                            ),
                            InkWell(
                              onTap: () async {
                                //Select PM TIme
                                final TimeOfDay? timeOfDay =
                                    await showTimePicker(
                                        context: context,
                                        initialTime: selectAmTime,
                                        initialEntryMode:
                                            TimePickerEntryMode.dial);
                                if (timeOfDay != null) {
                                  setState(() {
                                    selectPmTime = timeOfDay;
                                  });
                                }
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: constants.lightGrey),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                            '${selectPmTime.hour}:${selectPmTime.minute}'),
                                      ),
                                    ),
                                    Container(
                                        decoration: BoxDecoration(
                                            color: constants.greyColor),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('PM'),
                                        )),
                                    SizedBox(
                                      width: 40,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        // controller.getGalleryImage();
                        Get.defaultDialog(
                            content: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                controller.getCameraImage();
                                Get.back();
                              },
                              child: Container(
                                decoration:
                                    BoxDecoration(color: Colors.blue[100]),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Camera"),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            InkWell(
                              onTap: () {
                                controller.getGalleryImage();
                                Get.back();
                              },
                              child: Container(
                                decoration:
                                    BoxDecoration(color: Colors.blue[100]),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Gallery"),
                                ),
                              ),
                            ),
                          ],
                        ));
                      },
                      child: Obx(() {
                        if (controller.image.value.isEmpty) {
                          return Container(
                            height: height * 0.35,
                            width: width,
                            decoration: BoxDecoration(
                                color: constants.greyColor,
                                borderRadius: BorderRadius.circular(10)),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    CupertinoIcons.photo,
                                    size: 40,
                                  ),
                                  Text(
                                    'No Image Selected',
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontFamily: 'JosefinSans'),
                                  ),
                                ],
                              ),
                            ),
                          );
                        } else {
                          return Container(
                            child: Image.file(
                              File(controller.image.value.toString()),
                              fit: BoxFit.fill,
                              height: height * 0.35,
                              width: width,
                            ),
                          );
                        }
                      }),
                    ),

                    SizedBox(
                      height: 10,
                    ),
                    CustomTextFormField(
                      controller: descriptionController,
                      hintText: 'Description',
                      fontFamily: 'JosefinSans',
                      labelText: 'Description',
                      maxLinse: 7,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    // if (controller.dropdownValue)
                    // if (controller.isSelectedValue.value == 'Swapping' ||
                    //     controller.isSelectedValue == 'Donation' ||
                    //     controller.isSelectedValue == 'Eco-Mart')
                    InkWell(
                      onTap: () {
                        tappedValue = !tappedValue;

                        setState(() {
                          // dropDownFun2();
                        }); // setState(() {});
                      },
                      child: Container(
                        color: constants.greyColor,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(conditionDropDownValue),
                              tappedValue == true
                                  ? Icon(Icons.keyboard_arrow_up)
                                  : Icon(Icons.keyboard_arrow_down)
                            ],
                          ),
                        ),
                      ),
                    ),
                    // if (controller.isSelectedValue.value == 'Swapping' ||
                    //     controller.isSelectedValue == 'Donation' ||
                    //     controller.isSelectedValue == 'Eco-Mart')
                    if (tappedValue == true)
                      Container(
                        child: ListView.builder(
                            primary: true,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: dropDownValues1.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  conditionDropDownValue =
                                      dropDownValues1[index].toString();
                                  tappedValue = !tappedValue;
                                  setState(() {});
                                },
                                child: Container(
                                  color: constants.lightGrey,
                                  // height: height * 0.3,
                                  width: width,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child:
                                        Text(dropDownValues1[index].toString()),
                                  ),
                                ),
                              );
                            }),
                      ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: StreamBuilder(
                          stream: userRef
                              .where('userId', isEqualTo: auth.currentUser!.uid)
                              .snapshots(),
                          builder: (context, snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                              case ConnectionState.none:
                                return Column(
                                  children: [
                                    Center(
                                        child: Text('Getting your details!')),
                                    Center(
                                      child: Text('Please Wait...'),
                                    )
                                  ],
                                );
                              case ConnectionState.active:
                              case ConnectionState.done:
                                var data = snapshot.data!.docs[0];
                                UsersModel usersModel = UsersModel(
                                    name: data['name'],
                                    email: data['email'],
                                    address: data['address'],
                                    phone: data['phone']);
                                return Container(
                                  height: height * 0.3,
                                  width: width,
                                  child: Card(
                                    // color: constants.greyColor,
                                    child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Name ',
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              Container(
                                                width: width * 0.35,
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  '${usersModel.name.toString()}',
                                                  style:
                                                      TextStyle(fontSize: 17),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Email ',
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              Container(
                                                width: width * 0.35,
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  '${usersModel.email.toString()}',
                                                  style:
                                                      TextStyle(fontSize: 17),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Phone ',
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              Container(
                                                width: width * 0.35,
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  '${usersModel.phone}',
                                                  style:
                                                      TextStyle(fontSize: 17),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Address ',
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              Container(
                                                width: width * 0.35,
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  '${usersModel.address.toString()}',
                                                  style:
                                                      TextStyle(fontSize: 17),
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                );

                              // break;
                              // default:
                            }
                          }),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomButton(
                      fontFamily: 'JosefinSans',
                      title: 'Upload',
                      onTap: () {
                        services.addPost(
                            nameController.text.toString(),
                            priceController.text.toString(),
                            controller.isSelectedValue.value.toString(),
                            descriptionController.text.toString(),
                            controller.image.value.toString(),
                            countryValue,
                            stateValue,
                            cityValue,
                            conditionDropDownValue,
                            organizationController.text.toString(),
                            serviceNameController.text.toString(),
                            selectAmTime.toString(),
                            selectPmTime.toString(),
                            1);

                        // services.addPost1(
                        //     nameController.text.toString(),
                        //     priceController.text.toString(),
                        //     controller.isSelectedValue.value.toString(),
                        //     descriptionController.text.toString(),
                        //     controller.image.value.toString(),
                        //     "email",
                        //     "userName",
                        //     APIs.auth!.uid.toString());
                      },
                      radius: 10,
                      backGroundColor: constants.darkRedColor,
                      buttonTextColor: constants.whiteColor,
                      width: width,
                      // isLoading: controller.isLoading.value,
                      fontSize: 18,
                      height: height * 0.06,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          ));
    });
  }
}
