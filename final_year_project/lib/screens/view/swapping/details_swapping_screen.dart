// ignore_for_file: unused_import, must_be_immutable, prefer_typing_uninitialized_variables, sized_box_for_whitespace, prefer_const_constructors

import 'package:cached_network_image/cached_network_image.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/screens/view/chat/messages.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:animated_rating_bar/animated_rating_bar.dart';
import 'package:google_maps_widget/google_maps_widget.dart';

class SwappingDetailsScreen extends StatefulWidget {
  var title;
  var price;
  var category;
  var image;
  var description;
  var productId;
  var userName;
  var email;
  // var rating;
  var userId;

  SwappingDetailsScreen(
      {super.key,
      required this.title,
      required this.price,
      required this.category,
      required this.image,
      required this.description,
      required this.userName,
      required this.email,
      // required this.rating,
      required this.productId,
      required this.userId});

  @override
  State<SwappingDetailsScreen> createState() => _SwappingDetailsScreenState();
}

class _SwappingDetailsScreenState extends State<SwappingDetailsScreen> {
  Constants constants = Constants();
  double height = Get.height;
  double width = Get.width;
  final ref = FirebaseDatabase.instance.ref("All Posts/Swapping");
  final userRef = FirebaseDatabase.instance.ref('User');
  LogicsController controller = LogicsController();
  final auth = FirebaseAuth.instance.currentUser;
  //----------------For add to favourite item--------------
  // String id = DateTime.now().millisecondsSinceEpoch.toString();
//   addFavouriteItems(var uid) async {
//     try {
//       final response1 =
//           await userRef.child('${auth!.uid}').child('Favourite Items/');
//       final response = await response1.child(widget.id).set({
//         'title': widget.title,
//         'price': widget.price,
//         'image': widget.image,
//         'description': widget.description,
//         'category': widget.category,
//         'isFavourite': controller.isFavourite.value,
//         'id': widget.id
//       }).then((value) {
//         Utils.toastMessages('Product added to favourite');
//       }).onError((error, stackTrace) {
//         Utils.toastMessages(error.toString());
//       });
//     } catch (e) {
//       Utils.toastMessages(e.toString());
//     }
//   }

// //--------------For remove to favourite items-------------

//   removeFavouriteItems(
//     var uid,
//   ) async {
//     try {
//       final response1 =
//           await userRef.child('${auth!.uid}').child('Favourite Items/');
//       final response = await response1
//           .child(widget.id)
//           .remove()
//           .then((value) => Utils.toastMessages('Product removed as favourite'))
//           .onError(
//               (error, stackTrace) => Utils.toastMessages(error.toString()));
//     } catch (e) {
//       Utils.toastMessages(e.toString());
//     }
//   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: constants.whiteColor,
      appBar: CustomAppbar(
        fontFamily: 'JosefinSans',
        backgroundColor: constants.darkRedColor,
        textColor: constants.whiteColor,
        title: 'Details',
        fontWeight: FontWeight.bold,
        fontsize: constants.appbarFontSize,
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              CupertinoIcons.back,
              color: constants.whiteColor,
            )),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 5,
              ),
              Container(
                height: height * 0.45,
                width: width,
                child: CachedNetworkImage(
                  imageUrl: widget.image,
                  fit: BoxFit.fill,
                  placeholder: (context, url) {
                    return Center(
                      child: CircularProgressIndicator(
                        color: constants.darkRedColor,
                      ),
                    );
                  },
                ),
              ),
              Container(
                height: height * 0.45,
                width: width,
                decoration: BoxDecoration(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${widget.title}',
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontWeight: FontWeight.bold,
                                fontSize: constants.detailsTitleFontSize),
                          ),
                          Text(
                            "\$${widget.price}",
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontWeight: FontWeight.bold,
                                fontSize: constants.detailsTitleFontSize),
                          ),
                        ],
                      ),
                      Container(
                        alignment: Alignment.topLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            AnimatedRatingBar(
                                activeFillColor: constants.yellowColor,
                                strokeColor: constants.blackColor,
                                onRatingUpdate: (animated) async {
                                  // await ref
                                  //     .child(widget.id)
                                  //     .update({"rating": animated}).then((value) {
                                  //   Utils.toastMessages("Rating updated");
                                  // }).onError((error, stackTrace) {
                                  //   Utils.toastMessages(error.toString());
                                  // });
                                  // debugPrint(animated.toString());
                                }),

                            InkWell(
                              onTap: () {
                                Get.to(() => MessageScreen(
                                      email: widget.email,
                                      recieverId: widget.userId,
                                      name: widget.userName,
                                    ));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    // color: constants.darkRedColor,
                                    border: Border.all(
                                        color: constants.darkRedColor,
                                        width: 2),
                                    boxShadow: [
                                      BoxShadow(
                                          blurRadius: 10,
                                          spreadRadius: 5,
                                          color: constants.darkRedColor
                                              .withOpacity(0.1))
                                    ],
                                    borderRadius: BorderRadius.circular(10)),
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    'Offers',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: constants.darkRedColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                            )
                            // Obx(() => IconButton(
                            //     onPressed: () {
                            //       controller.favouriteItem();
                            //       switch (controller.isFavourite.value) {
                            //         case true:
                            //           addFavouriteItems(auth!.uid.toString());
                            //         case false:
                            //           removeFavouriteItems(
                            //               auth!.uid.toString());
                            //         default:
                            //       }
                            //     },
                            //     icon: controller.isFavourite.value
                            //         ? Icon(
                            //             CupertinoIcons.heart_fill,
                            //             color: Colors.red,
                            //           )
                            //         : Icon(CupertinoIcons.heart)))
                          ],
                        ),
                      ),
                      Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Posted by:  ${widget.userName}',
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          )),
                      Container(
                          alignment: Alignment.topLeft,
                          child: Text('Email: ${widget.email}',
                              style: TextStyle(
                                  fontFamily: 'JosefinSans',
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '${widget.description}',
                              textWidthBasis: TextWidthBasis.longestLine,
                              style: TextStyle(
                                fontSize: constants.simpleFontSize,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              // Container(
              //   height: height * 0.6,
              //   width: width,
              //   child: GoogleMap(
              //     onMapCreated: (_) {},
              //     rotateGesturesEnabled: true,
              //     scrollGesturesEnabled: true,
              //     zoomControlsEnabled: true,
              //     zoomGesturesEnabled: true,
              //     compassEnabled: true,
              //     mapType: MapType.normal,
              //     markers: {},
              //     initialCameraPosition: CameraPosition(
              //       target: LatLng(
              //         40.484000837597925,
              //         -3.369978368282318,
              //       ),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
