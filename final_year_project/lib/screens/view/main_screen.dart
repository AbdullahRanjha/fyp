// ignore_for_file: unused_import, prefer_final_fields, prefer_const_constructors, sized_box_for_whitespace

import 'package:final_year_project/Admin%20Panel/view/all_users/all_users_screen.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/screens/view/Users/users_screen.dart';
import 'package:final_year_project/screens/view/addPost_screen.dart';
import 'package:final_year_project/screens/view/chat/chat_screen.dart';
import 'package:final_year_project/screens/view/dashboard/dashboardController.dart';
// import 'package:final_year_project/screens/view/favourite_screen.dart';
import 'package:final_year_project/screens/view/Users/profile_screen.dart';
import 'package:final_year_project/screens/view/setting_screen.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // int index = 0;
  Constants constants = Constants();
  @override
  initState() {
    super.initState;
    APIs().getUserCurrentData();
  }

  PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);

  List<PersistentBottomNavBarItem> navBarItems() {
    return [
      PersistentBottomNavBarItem(
          title: 'Home',
          inactiveColorPrimary: constants.blackColor,
          activeColorPrimary: constants.darkRedColor,
          inactiveIcon: Icon(
            CupertinoIcons.house,
            color: constants.blackColor,
          ),
          icon: Icon(CupertinoIcons.house_fill)),
      PersistentBottomNavBarItem(
          title: 'Users',
          inactiveColorPrimary: constants.blackColor,
          activeColorPrimary: constants.darkRedColor,
          inactiveIcon: Icon(
            CupertinoIcons.person_3,
            color: constants.blackColor,
          ),
          icon: Icon(CupertinoIcons.person_3_fill)),
      PersistentBottomNavBarItem(
          title: 'Add',
          inactiveColorPrimary: constants.blackColor,
          activeColorPrimary: constants.darkRedColor,
          inactiveIcon: Icon(
            CupertinoIcons.add,
            color: constants.whiteColor,
          ),
          icon: Icon(
            CupertinoIcons.add,
            color: constants.whiteColor,
          )),
      PersistentBottomNavBarItem(
          title: 'Profile',
          inactiveColorPrimary: constants.blackColor,
          activeColorPrimary: constants.darkRedColor,
          inactiveIcon: Icon(
            CupertinoIcons.person,
            color: constants.blackColor,
          ),
          icon: Icon(CupertinoIcons.person_fill)),
      PersistentBottomNavBarItem(
          title: 'Settings',
          activeColorPrimary: constants.darkRedColor,
          inactiveColorPrimary: constants.blackColor,
          inactiveIcon: Icon(
            CupertinoIcons.settings,
            color: constants.blackColor,
          ),
          icon: Icon(CupertinoIcons.settings_solid)),
    ];
  }

  List<Widget> buildScreen() {
    return [
      DashboardController(),
      UsersScreen(),
      AddPostScreen(),
      ProfileScreen(),
      SettingsScreen()
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
        controller: _controller,
        context,
        floatingActionButton: Container(
          height: 0,
          width: 0,
          child: FloatingActionButton(
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            mini: true,
            // shape: ,
            onPressed: () {},
            backgroundColor: Colors.transparent,
            disabledElevation: 0,
            child: Container(
              height: 0,
              width: 0,
            ),
          ),
        ),
        screens: buildScreen(),
        items: navBarItems(),
        stateManagement: true,
        hideNavigationBar: false,
        navBarStyle: NavBarStyle.style15);
    // body: screensList[index],
    // );
  }
}
