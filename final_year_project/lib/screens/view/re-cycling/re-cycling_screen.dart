import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Models/product_model.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:final_year_project/screens/view/donation/details_donationScreen.dart';
import 'package:final_year_project/screens/view/re-cycling/details_re-cyclingScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_card/image_card.dart';

import '../swapping/details_swapping_screen.dart';

class Re_CyclingScreen extends StatefulWidget {
  const Re_CyclingScreen({super.key});

  @override
  State<Re_CyclingScreen> createState() => _Re_CyclingScreenState();
}

class _Re_CyclingScreenState extends State<Re_CyclingScreen> {
  Constants constants = Constants();
  // final ref = FirebaseDatabase.instance.ref("All Posts/Re-Cycling");
  final swappingRef = FirebaseFirestore.instance
      .collection('All Posts')
      .where('category', isEqualTo: 'Re-Cycling');
  double height = Get.height;
  double width = Get.width;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: swappingRef.snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                color: constants.darkRedColor,
              ),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          // Map<dynamic, dynamic> map = snapshot.data!.snapshot.value as dynamic;
          // List<dynamic> list = [];
          // list.clear();
          // list = map.values.toList();
          var data = snapshot.data!.docs;
          return Container(
            height: height,
            width: width,
            child: Column(
              children: [
                Expanded(
                  child: GridView.builder(
                      itemCount: data.length,
                      scrollDirection: Axis.vertical,
                      physics: ScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          mainAxisExtent: 190,
                          crossAxisCount: 2),
                      itemBuilder: (context, index) {
                        ProductModel productModel = ProductModel(
                            image: data[index]['image'],
                            price: data[index]['price'],
                            name: data[index]['name'],
                            rating: (data[index]['rating'] as num).toDouble(),
                            description: data[index]['description'],
                            productId: data[index]['id'],
                            category: data[index]['category'],
                            userName: data[index]['userName'],
                            userId: data[index]['userId'],
                            email: data[index]['email'],
                            city: data[index]['city'],
                            state: data[index]['state'],
                            country: data[index]['country'],
                            conditionDropDownValue: data[index]['condition'],
                            organization: data[index]['organization'],
                            serviceName: data[index]['serviceName'],
                            amTime: data[index]['amTime'],
                            pmTime: data[index]['pmTime']);

                        // String title = list[index]['name'];
                        // String description = list[index]['description'];
                        // String id = list[index]['id'];
                        // String image = list[index]['image'];
                        // var price = list[index]['price'];
                        // String category = list[index]['category'];

                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4.0),
                          child: Container(
                            width: width * 0.35,
                            child: Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Get.to(() => DonationDetailsScreen(
                                            title: productModel.name,
                                            rating: productModel.rating,
                                            price: productModel.price,
                                            category: productModel.category,
                                            image: productModel.image,
                                            description:
                                                productModel.description,
                                            userName: productModel.userName,
                                            email: productModel.email,
                                            productId: productModel.productId,
                                            userId: productModel.userId,
                                            condition: productModel
                                                .conditionDropDownValue,
                                            country: productModel.country,
                                            city: productModel.city,
                                            state: productModel.state,
                                            organization:
                                                productModel.organization,
                                            serviceName:
                                                productModel.serviceName,
                                          ));
                                      // Get.toNamed(RouteName
                                      //     .swappingDetailsScreen);
                                    },
                                    child: Container(
                                      child: FillImageCard(
                                          borderRadius: 10,
                                          contentPadding:
                                              EdgeInsets.only(left: 5.0),
                                          height: 150,
                                          // footer:
                                          heightImage: 85,
                                          width: width * 0.35,
                                          title: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    width: width * 0.25,
                                                    child: Text(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      productModel.name,
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 7.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.topLeft,
                                                      height: height * 0.03,
                                                      width: width * 0.15,
                                                      child: AnimatedRatingBar(
                                                          initialRating:
                                                              productModel
                                                                  .rating
                                                                  .toDouble(),
                                                          activeFillColor:
                                                              constants
                                                                  .yellowColor,
                                                          strokeColor: constants
                                                              .blackColor,
                                                          onRatingUpdate:
                                                              (val) {
                                                            final auth =
                                                                FirebaseAuth
                                                                    .instance
                                                                    .currentUser;
                                                            final postRef = FirebaseFirestore
                                                                .instance
                                                                .collection(
                                                                    'All Posts')
                                                                .doc(productModel
                                                                    .productId)
                                                                .update({
                                                                  'rating': val
                                                                      .toInt()
                                                                })
                                                                .then((value) =>
                                                                    null)
                                                                .onError((error,
                                                                        stackTrace) =>
                                                                    null);
                                                          }),
                                                    ),
                                                    Container(
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        3),
                                                            color: Constants()
                                                                .darkRedColor),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(1.5),
                                                          child: Text(
                                                            '${productModel.category}',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 11,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          imageProvider:
                                              NetworkImage(productModel.image)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }
}
