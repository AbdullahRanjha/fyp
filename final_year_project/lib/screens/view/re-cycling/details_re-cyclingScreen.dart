import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/screens/view/chat/messages.dart';
import 'package:flutter/cupertino.dart';
// import 'package:firebase_chat_app/resources/components/appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../core/Utils/constants/colors.dart';

class Eco_MartDetailsScreen extends StatefulWidget {
  var title;
  var description;
  var image;
  var price;
  var userName;
  var email;
  var category;
  var productId;
  double rating;
  var userId;
  Eco_MartDetailsScreen(
      {super.key,
      required this.title,
      required this.description,
      required this.productId,
      required this.userName,
      required this.email,
      required this.image,
      required this.category,
      required this.rating,
      required this.userId,
      required this.price});

  @override
  State<Eco_MartDetailsScreen> createState() => _Eco_MartDetailsScreenState();
}

class _Eco_MartDetailsScreenState extends State<Eco_MartDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    Constants constants = Constants();
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      backgroundColor: constants.whiteColor,
      appBar: CustomAppbar(
        backgroundColor: constants.darkRedColor,
        textColor: constants.whiteColor,
        title: 'Details',
        fontFamily: 'JosefinSans',
        fontWeight: FontWeight.bold,
        fontsize: constants.appbarFontSize,
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              CupertinoIcons.back,
              color: constants.whiteColor,
            )),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 5,
              ),
              Container(
                height: height * 0.45,
                width: width,
                child: CachedNetworkImage(
                  imageUrl: widget.image,
                  fit: BoxFit.fill,
                  placeholder: (context, url) {
                    return Center(
                      child: CircularProgressIndicator(
                        color: constants.darkRedColor,
                      ),
                    );
                  },
                ),
              ),
              Container(
                height: height * 0.45,
                width: width,
                decoration: BoxDecoration(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "${widget.title}",
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontWeight: FontWeight.bold,
                                fontSize: constants.detailsTitleFontSize),
                          ),
                          Text(
                            "RS: ${widget.price}",
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontWeight: FontWeight.bold,
                                fontSize: constants.detailsTitleFontSize),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.topLeft,
                            child: AnimatedRatingBar(
                                initialRating: widget.rating,
                                activeFillColor: constants.yellowColor,
                                strokeColor: constants.blackColor,
                                onRatingUpdate: (animated) {
                                  debugPrint(animated.toString());
                                }),
                          ),
                          InkWell(
                            onTap: () {
                              Get.to(() => MessageScreen(
                                    email: widget.email,
                                    recieverId: widget.userId,
                                    name: widget.userName,
                                  ));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  // color: constants.darkRedColor,
                                  border: Border.all(
                                      color: constants.darkRedColor, width: 2),
                                  boxShadow: [
                                    BoxShadow(
                                        blurRadius: 10,
                                        spreadRadius: 5,
                                        color: constants.darkRedColor
                                            .withOpacity(0.1))
                                  ],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.mail_outline,
                                      color: constants.darkRedColor,
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Message',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: constants.darkRedColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Posted by:  ${widget.userName}',
                            style: TextStyle(
                                fontFamily: 'JosefinSans',
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          )),
                      Container(
                          alignment: Alignment.topLeft,
                          child: Text('Email: ${widget.email}',
                              style: TextStyle(
                                  fontFamily: 'JosefinSans',
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '${widget.description}',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: constants.simpleFontSize,
                                  fontFamily: 'JosefinSans'),
                              textWidthBasis: TextWidthBasis.longestLine,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              // Container(
              //   height: height * 0.6,
              //   width: width,
              //   child: GoogleMap(
              //     onMapCreated: (_) {},
              //     rotateGesturesEnabled: true,
              //     scrollGesturesEnabled: true,
              //     zoomControlsEnabled: true,
              //     zoomGesturesEnabled: true,
              //     compassEnabled: true,
              //     mapType: MapType.normal,
              //     markers: {},
              //     initialCameraPosition: CameraPosition(
              //       target: LatLng(
              //         40.484000837597925,
              //         -3.369978368282318,
              //       ),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
