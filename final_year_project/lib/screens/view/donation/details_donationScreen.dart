
import 'dart:developer';

import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/photo_viewr_scree.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
// import 'package:firebase_chat_app/resources/components/appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../core/Utils/constants/colors.dart';
import '../../../core/Utils/utils.dart';
import '../chat/messages.dart';

class DonationDetailsScreen extends StatefulWidget {
  var title;
  var image;
  var description;
  var productId;
  var price;
  var email;
  var userName;
  var category;
  var userId;
  double rating;
  var serviceName;
  var organization;
  var country;
  var condition;
  var city;
  var state;
  var amTime;
  var pmTime;

  DonationDetailsScreen(
      {super.key,
      required this.description,
      required this.title,
      required this.email,
      required this.image,
      required this.userName,
      required this.productId,
      required this.category,
      required this.userId,
      this.city,
      required this.rating,
      this.state,
      this.amTime,
      this.pmTime,
      // required this.image,
      required this.price,
      this.organization,
      this.country,
      this.serviceName,
      this.condition});

  @override
  State<DonationDetailsScreen> createState() => _DonationDetailsScreenState();
}

class _DonationDetailsScreenState extends State<DonationDetailsScreen> {
  final auth = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    Constants constants = Constants();
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      backgroundColor: constants.whiteColor,
      appBar: CustomAppbar(
        backgroundColor: constants.darkRedColor,
        textColor: constants.whiteColor,
        title: 'Details',
        fontFamily: 'JosefinSans',
        fontWeight: FontWeight.bold,
        fontsize: constants.appbarFontSize,
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              CupertinoIcons.back,
              color: constants.whiteColor,
            )),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 0,
              ),
              InkWell(
                onTap: () {
                  Get.to(() => PhotoViewrScreen(
                      image: widget.image, title: widget.title));
                },
                child: Container(
                  height: height * 0.45,
                  width: width,
                  // decoration: BoxDecoration(borderRadius: Border),
                  child: CachedNetworkImage(
                    imageUrl: widget.image,
                    fit: BoxFit.fill,
                    placeholder: (context, url) {
                      return Center(
                        child: CircularProgressIndicator(
                          color: constants.darkRedColor,
                        ),
                      );
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Column(
                  children: [
                    Container(
                      // height: height * 0.45,
                      width: width,
                      decoration: BoxDecoration(),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: width * 0.5,
                                  child: Text(
                                    overflow: TextOverflow.clip,
                                    '${widget.title}',
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            constants.detailsTitleFontSize),
                                  ),
                                ),
                                if (widget.category == 'Eco-Mart')
                                  Text(
                                    "RS: ${widget.price}",
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            constants.detailsTitleFontSize),
                                  ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: AnimatedRatingBar(
                                      initialRating: widget.rating.toDouble(),
                                      activeFillColor: constants.yellowColor,
                                      strokeColor: constants.blackColor,
                                      onRatingUpdate: (val) async {
                                        debugPrint(val.toString());
                                        final auth =
                                            FirebaseAuth.instance.currentUser;
                                        final postRef = await FirebaseFirestore
                                            .instance
                                            .collection('All Posts')
                                            .doc(widget.productId)
                                            .update({
                                          'rating': val.toInt()
                                        }).then((value) async {
                                          final auth2 =
                                              FirebaseFirestore.instance;
                                          await auth2
                                              .collection('User')
                                              .doc(widget.userId)
                                              .collection('myPosts')
                                              .doc(widget.productId)
                                              .update({'rating': val.toInt()});
                                        }).onError((error, stackTrace) => null);
                                      }),
                                ),
                                InkWell(
                                  onTap: () {
                                    Get.to(() => MessageScreen(
                                          email: widget.email,
                                          recieverId: widget.userId,
                                          name: widget.userName,
                                        ));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        // color: constants.darkRedColor,
                                        border: Border.all(
                                            color: constants.darkRedColor,
                                            width: 2),
                                        boxShadow: [
                                          BoxShadow(
                                              blurRadius: 10,
                                              spreadRadius: 5,
                                              color: constants.darkRedColor
                                                  .withOpacity(0.1))
                                        ],
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.mail_outline,
                                            color: constants.darkRedColor,
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            'Message',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: constants.darkRedColor,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Container(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  'Posted by:  ${widget.userName}',
                                  style: TextStyle(
                                      fontFamily: 'JosefinSans',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                )),

                            Container(
                                alignment: Alignment.topLeft,
                                child: Text('Email: ${widget.email}',
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold))),
                            // if(widget.category ==)
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                                alignment: Alignment.topLeft,
                                child: Text('Country: ${widget.country}',
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold))),
                            Container(
                                alignment: Alignment.topLeft,
                                child: Text('State: ${widget.state}',
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold))),

                            Container(
                                alignment: Alignment.topLeft,
                                child: Text('City: ${widget.city}',
                                    style: TextStyle(
                                        fontFamily: 'JosefinSans',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold))),
                            if (widget.condition == 'Swapping' ||
                                widget.condition == 'Re-Cycling' ||
                                widget.category == 'Donation')
                              Container(
                                  alignment: Alignment.topLeft,
                                  child: Text('Condition: ${widget.condition}',
                                      style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold))),
                            if (widget.category == 'Re-Cycling')
                              Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                      'Organization: ${widget.organization}',
                                      style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold))),
                            if (widget.category == 'Re-Cycling')
                              Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                      'Service Name: ${widget.serviceName}',
                                      style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold))),
                            if (widget.category == 'Re-Cycling')
                              Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                      'Availability:  ${widget.amTime} AM    to     ${widget.pmTime} PM',
                                      style: TextStyle(
                                          fontFamily: 'JosefinSans',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold))),

                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: double.infinity,
                              child: Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "${widget.description}",
                                    textWidthBasis: TextWidthBasis.longestLine,
                                    style: TextStyle(
                                      fontSize: constants.simpleFontSize,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
