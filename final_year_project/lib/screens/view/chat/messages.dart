import 'dart:io';

import 'package:final_year_project/main.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/screens/view/chat/messages_list.dart';
import 'package:final_year_project/services/chat_services/chat_sevices.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../resources/widgets/appbar.dart';

class MessageScreen extends StatefulWidget {
  String? name;
  String? email;
  String? image;

  String? recieverId;
  MessageScreen({
    super.key,
    this.email,
    this.name,
    this.recieverId,
    this.image,
  });

  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  final auth = FirebaseAuth.instance.currentUser;
  TextEditingController messageController = TextEditingController();

  Constants constants = Constants();

  File? image;
  ImagePicker picker = ImagePicker();
  void imagePicker() async {
    final pickedImage =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 70);
    if (pickedImage != null) {
      image = File(pickedImage.path);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // APIs().getUserCurrentData();
    ChatServices().getMessages(currentUser!.uid, widget.recieverId.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: constants.darkRedColor,
        leading: InkWell(
          onTap: () => Get.back(),
          child: Icon(
            CupertinoIcons.back,
            color: constants.whiteColor,
          ),
        ),
        elevation: 0,
        title: Row(
          children: [
            CircleAvatar(
              radius: 18,
              backgroundImage: NetworkImage(widget.image.toString()),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              widget.name.toString(),
              style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.normal,
                  color: constants.whiteColor,
                  fontFamily: 'JosefinSans'),
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Expanded(
              child: MessagesList(
            toId: widget.recieverId,
            fromId: currentUser!.uid,
            // recieverName: widget.name,
            // user: widget,
          )),
          // WriteMess
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 8,
                    surfaceTintColor: constants.darkRedColor,
                    shadowColor: constants.darkRedColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              spellCheckConfiguration: SpellCheckConfiguration(
                                misspelledSelectionColor:
                                    constants.lightBlueColor,
                                // spellCheckSuggestionsToolbarBuilder:
                                //     (context, editableTextState) {},
                              ),
                              controller: messageController,
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              decoration: InputDecoration(
                                  hintText: 'Write message...',
                                  border: InputBorder.none),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                ChatServices().sendCameraImages(
                                    widget.recieverId.toString(),
                                    widget.name.toString());
                              },
                              icon: Icon(
                                CupertinoIcons.camera,
                                color: constants.darkRedColor,
                              )),
                          IconButton(
                              onPressed: () {
                                ChatServices().sendImages(
                                    widget.recieverId.toString(),
                                    widget.name.toString());
                              },
                              icon: Icon(
                                Icons.image,
                                color: constants.darkRedColor,
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  if (widget.recieverId == currentUser!.uid) {
                    Utils.toastMessages(
                        'You cannot send message to your self\nTry send messages to different users');
                  } else if (messageController.text.isEmpty ||
                      messageController.text == '') {
                    Utils.toastMessages('Empty message cannot be send');
                  } else {
                    FocusScope.of(context).unfocus();

                    ChatServices().sendTextMessage(
                        widget.recieverId.toString(),
                        messageController.text.toString(),
                        widget.name.toString());
                  }
                  // setState(() {});
                  messageController.clear();
                  setState(() {});
                },
                child: CircleAvatar(
                  backgroundColor: constants.darkRedColor,
                  radius: 20,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 2.0, top: 3),
                      child: Icon(
                        color: constants.whiteColor,
                        CupertinoIcons.location,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
