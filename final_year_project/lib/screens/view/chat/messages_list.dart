import 'dart:convert';
import 'dart:developer';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/photo_viewr_scree.dart';
import 'package:final_year_project/main.dart';
import 'package:final_year_project/services/Apis/apis.dart';
// import 'package:final_year_project/Models/message_model.dart';
import 'package:final_year_project/Models/users_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
// import 'package:final_year_project/screens/view/chat/message_card.dart';
import 'package:final_year_project/services/chat_services/chat_sevices.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MessagesList extends StatefulWidget {
  var fromId;
  var toId;
  // UsersClass user;
  MessagesList({
    super.key,
    required this.fromId,
    required this.toId,
  });

  @override
  State<MessagesList> createState() => _MessagesListState();
}

class _MessagesListState extends State<MessagesList> {
  final auth = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ChatServices()
        .getMessages(currentUser!.uid.toString(), widget.toId.toString());
    print('called this function');
  }

  // List<Message> list = [];
  final fireStore = FirebaseFirestore.instance;
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [gettingMessages()],
    );
  }

  gettingMessages() {
    return Expanded(
        child: StreamBuilder(
      stream:
          ChatServices().getMessages(currentUser!.uid.toString(), widget.toId),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.none:
            return Column(
              children: [
                Center(child: SizedBox()),
              ],
            );
          case ConnectionState.active:
          case ConnectionState.done:
            return ListView(
              reverse: true,
              children: snapshot.data!.docs
                      .map((e) => buildMessageList(e))
                      .toList() ??
                  [],
            );
        }
      },
    ));
  }

  Widget buildMessageList(DocumentSnapshot data) {
    Map<String, dynamic> messageData = data.data() as Map<String, dynamic>;
    return messageData['senderId'] == currentUser!.uid.toString()
        ? greenMessage(messageData['message'], messageData['time'], messageData)
        : blueMessage(messageData['message'], messageData['time'], messageData);
  }

  Widget greenMessage(String message, var time, Map<String, dynamic> check) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(13.0),
          child: Text(time),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(13),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.green.shade600),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                    bottomLeft: Radius.circular(15)),
                color: Colors.green[100]),
            child: check['message'] == ''
                ? InkWell(
                    onTap: () {
                      Get.to(() =>
                          PhotoViewrScreen(image: check['type'], title: ''));
                    },
                    child: CachedNetworkImage(imageUrl: check['type']))
                : Text(
                    message == '' ? "" : message.toString(),
                    style: TextStyle(fontSize: 21, fontFamily: 'JosefinSans'),
                  ),
          ),
        ),
      ],
    );
  }

  //For reciever message

  Widget blueMessage(String message, var time, Map<String, dynamic> check) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(13),
            decoration: BoxDecoration(
                border: Border.all(color: constants.lightBlueColor),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                    bottomRight: Radius.circular(15)),
                color: Colors.blue[100]),
            child: check['message'] == ''
                ? InkWell(
                    onTap: () {
                      Get.to(() =>
                          PhotoViewrScreen(image: check['type'], title: ''));
                    },
                    child: CachedNetworkImage(
                      imageUrl: check['type'],
                    ),
                  )
                : Text(
                    message == '' ? "" : message.toString(),
                    style: TextStyle(fontSize: 21, fontFamily: 'JosefinSans'),
                  ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(13.0),
          child: Text('${time.toString()}'),
        )
      ],
    );
  }
}
