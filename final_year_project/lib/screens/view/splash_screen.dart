import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/notifications/notifications_servicies.dart';
import 'package:final_year_project/services/splash_services.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // SplashServices services = Get.put(SplashServices());
  Constants constants = Constants();
  SplashServices services = SplashServices();
  // NotificationsServices notificationsServicies = NotificationsServices();
  final auth = FirebaseAuth.instance;
  @override
  void initState() {
    // notificationsServicies.requestNotificationsPermission();
    // notificationsServicies.firebaseInit();
    // notificationsServicies
    //     .getDeviceToken()
    //     .then((val) => print("Device token: " + val.toString()));
    services.userData();

    services.isLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: constants.whiteColor,

      // appBar: CustomAppbar(title: 'S', automaticallyImplyLeading: automaticallyImplyLeading),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Text('Splash Screen'),
          SizedBox(
            height: 30,
          ),
          Image.asset(
            'assets/splash_logo.jpg',
            fit: BoxFit.cover,
          )
          // Container(
          //   child: LoadingAnimationWidget.discreteCircle(
          //       color: Colors.red,
          //       size: 50,
          //       secondRingColor: Colors.blue,
          //       thirdRingColor: Colors.green),
          // )
        ],
      ),
    );
  }
}
