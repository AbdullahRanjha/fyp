import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_year_project/Models/product_model.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/textFormField.dart';
import 'package:final_year_project/screens/view/donation/details_donationScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Constants constants = Constants();
  TextEditingController searchController = TextEditingController();
  bool isSearch = false;
  var data;
  Future<List<QueryDocumentSnapshot>> searchProduct(String city) async {
    final QuerySnapshot productRef = await FirebaseFirestore.instance
        .collection('All Posts')
        .where('city', isEqualTo: city)
        .get();

    return productRef.docs;
  }

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Search',
        automaticallyImplyLeading: true,
        backgroundColor: constants.darkRedColor,
        centerTile: true,
        textColor: constants.whiteColor,
        // fontWeight,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                        child: TextFormField(
                            controller: searchController,
                            onChanged: (value) {
                              isSearch = false;
                              setState(() {});
                            },
                            decoration: InputDecoration(
                                // suffixIcon: suffixIcon,
                                filled: true,
                                fillColor: constants.textFormFieldFilledColor,
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: 'Search',
                                label: Text('Search')))),
                  ),
                  InkWell(
                      onTap: () async {
                        isSearch = true;
                        data = await searchProduct(
                            searchController.text.toString());
                        setState(() {});
                      },
                      child: Container(
                          width: width * 0.12,
                          height: height * 0.067,
                          decoration: BoxDecoration(
                              color: constants.darkRedColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(5),
                                  bottomRight: Radius.circular(5))),
                          child: Icon(
                            Icons.search,
                            color: constants.whiteColor,
                          ))),
                  SizedBox(
                    height: 20,
                  ),
                  // IconButton(onPressed: (){}, icon: icon)
                ],
              ),
            ),
            if (isSearch == true)
              Center(
                child: StreamBuilder(
                    stream: FirebaseFirestore.instance
                        .collection('All Posts')
                        .where('city',
                            isEqualTo: searchController.text.toString())
                        .snapshots(),
                    builder: (context, snapshot) {
                      // return
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.none:
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Center(
                                child: CircularProgressIndicator(
                                  color: Constants().darkRedColor,
                                ),
                              ),
                            ],
                          );
                        case ConnectionState.active:
                        case ConnectionState.done:
                          return ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              shrinkWrap: true,
                              primary: true,
                              scrollDirection: Axis.vertical,
                              // scrollDirection: Axis.vertica,
                              physics: BouncingScrollPhysics(),
                              itemBuilder: (context, index) {
                                // final data = snapshot.data.
                                var data = snapshot.data!.docs[index];

                                ProductModel productModel = ProductModel(
                                    image: data['image'],
                                    price: data['price'],
                                    city: data['city'],
                                    name: data['name'],
                                    rating: (data['rating'] as num).toDouble(),
                                    description: data['description'],
                                    productId: data['id'],
                                    category: data['category'],
                                    userName: data['userName'],
                                    userId: data['userId'],
                                    state: data['state'],
                                    email: data['email'],
                                    country: data['country'],
                                    conditionDropDownValue: data['condition'],
                                    organization: data['organization'],
                                    serviceName: data['serviceName'],
                                    amTime: data['amTime'],
                                    pmTime: data['pmTime']);
                                return InkWell(
                                  onTap: () {
                                    Get.to(() => DonationDetailsScreen(
                                        description: productModel.description,
                                        title: productModel.name,
                                        email: productModel.email,
                                        image: productModel.image,
                                        userName: productModel.userName,
                                        productId: productModel.productId,
                                        category: productModel.category,
                                        userId: productModel.userId,
                                        amTime: productModel.amTime,
                                        city: productModel.city,
                                        serviceName: productModel.serviceName,
                                        state: productModel.state,
                                        condition:
                                            productModel.conditionDropDownValue,
                                        country: productModel.country,
                                        pmTime: productModel.pmTime,
                                        rating: productModel.rating,
                                        price: productModel.price));
                                  },
                                  child: Card(
                                    elevation: 2,
                                    // shadowColor:
                                    //     constants.darkRedColor.withOpacity(0.1),
                                    child: ListTile(
                                        trailing: Column(
                                          children: [
                                            SizedBox(
                                              height: Get.height * 0.017,
                                            ),
                                            Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            3),
                                                    color: Constants()
                                                        .darkRedColor),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(3.0),
                                                  child: Text(
                                                    // '${productModel.category}',
                                                    '${productModel.category}',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ))
                                          ],
                                        ),
                                        title: Text(
                                          '${productModel.name}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500),
                                        ),
                                        subtitle: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${productModel.description}',
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              // crossAxisAlignment:
                                              //     CrossAxisAlignment,
                                              children: [
                                                // Container(
                                                //   child: Text(
                                                //     // 'By: ${productModel.userName}',
                                                //     '${productModel.userName}',
                                                //     overflow: TextOverflow.ellipsis,
                                                //   ),
                                                // ),
                                              ],
                                            ),
                                            Text('City: ${productModel.city}')
                                          ],
                                        ),
                                        leading: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: NetworkImage(
                                                      productModel.image),
                                                  fit: BoxFit.cover),
                                              // color: Colors.grey,
                                              // color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          height: height,
                                          width: width * 0.28,
                                        )),
                                  ),
                                );
                              });
                      }
                    }),
              )
          ],
        ),
      ),
    );
  }
}
