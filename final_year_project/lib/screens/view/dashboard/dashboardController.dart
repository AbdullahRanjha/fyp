import 'package:animated_rating_bar/widgets/animated_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:final_year_project/Admin%20Panel/view/all_users/all_users_screen.dart';
import 'package:final_year_project/Admin%20Panel/view/analytics_screen.dart';
import 'package:final_year_project/screens/view/Users/complain_screen.dart';
import 'package:final_year_project/screens/view/Users/my_post_screen.dart';
import 'package:final_year_project/screens/view/chat/messages.dart';
import 'package:final_year_project/screens/view/dashboard/search_screen.dart';
import 'package:final_year_project/services/Apis/apis.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/screens/view/chat/chat_screen.dart';
import 'package:final_year_project/screens/view/dashboard/home_screen.dart';
import 'package:final_year_project/screens/view/donation/details_donationScreen.dart';
import 'package:final_year_project/screens/view/eco-mart/details_eco-martScreen.dart';
// import 'package:final_year_project/screens/view/favourite_screen.dart';
import 'package:final_year_project/screens/view/main_screen.dart';
import 'package:final_year_project/screens/view/Users/profile_screen.dart';
import 'package:final_year_project/screens/view/re-cycling/details_re-cyclingScreen.dart';
import 'package:final_year_project/screens/view/swapping/details_swapping_screen.dart';
import 'package:final_year_project/services/chat_services/chat_sevices.dart';
import 'package:final_year_project/services/firebase_services/Authentication/logout_services.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/widgets/appbar.dart';
import 'package:final_year_project/resources/widgets/custom_catagoryName.dart';
import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:final_year_project/screens/view/donation/donation_screen.dart';
import 'package:final_year_project/screens/view/eco-mart/eco-market_screen.dart';
import 'package:final_year_project/screens/view/re-cycling/re-cycling_screen.dart';
import 'package:final_year_project/screens/view/swapping/swapping_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class DashboardController extends StatefulWidget {
  const DashboardController({super.key});

  @override
  State<DashboardController> createState() => _DashboardControllerState();
}

class _DashboardControllerState extends State<DashboardController> {
  int index = 0;
  Constants constants = Constants();
  LogicsController controller = Get.put(LogicsController());

  UserCredential? credential;
  LogoutController logoutController = Get.put(LogoutController());
  final ref = FirebaseDatabase.instance.ref('User/');
  final auth = FirebaseAuth.instance.currentUser!;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    APIs().getUserCurrentData();
    // ChatServices().getMessages(auth.uid, otherUserId)
  }

  @override
  Widget build(BuildContext context) {
    // print('rebuild');
    double height = Get.height;
    double width = Get.width;

    return DefaultTabController(
      length: 5,
      child: Scaffold(
          backgroundColor: Colors.grey[100],
          drawer: Drawer(
            // width: width * 0.63,
            // surfaceTintColor: constants.whitweColor,
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/drawer_header.jpg'),
                        fit: BoxFit.cover),
                  ),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [],
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(CupertinoIcons.home),
                  title: Text(
                    'Home',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    // Handle item 1 tap
                    Get.toNamed(RouteName.mainScreen); // Close the drawer
                  },
                ),

                ListTile(
                  leading: Icon(
                    Icons.chat,
                    // color: constants.darkRedColor,
                  ),
                  title: Text(
                    'Chats ',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    // Get.toNamed(RouteName.allUsers);
                    Get.to(() => ChatScreen());
                  },
                ),

                ListTile(
                  leading: Icon(
                    Icons.newspaper,
                    // color: constants.darkRedColor,
                  ),
                  title: Text(
                    'My Posts ',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    PersistentNavBarNavigator.pushNewScreen(context,
                        screen: MyPostScreen());
                  },
                ),
                ListTile(
                  leading: Icon(
                      // C,
                      Icons.receipt_long_rounded
                      // color: constants.darkRedColor,
                      ),
                  title: Text(
                    'Complains ',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    Get.to(() => ComplainScreen());
                  },
                ),
                ListTile(
                  leading: Icon(CupertinoIcons.settings),
                  title: Text(
                    'Settings ',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    // Handle item 2 tap
                    Get.toNamed(RouteName.settingsScreen); // Close the drawer
                  },
                ),

                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text(
                    'Logout ',
                    style: TextStyle(
                      fontFamily: 'JosefinSans',
                    ),
                  ),
                  onTap: () {
                    logoutController.logoutUser();
                  },
                ),

                // Add more list items as needed
              ],
            ),
          ),
          // backgroundColor: constants.scaffoldBackgroundColor,
          appBar: AppBar(
            bottom: TabBar(
                automaticIndicatorColorAdjustment: true,
                indicatorWeight: 0.5,
                indicatorColor: constants.whiteColor,
                padding: EdgeInsets.only(bottom: 10),
                tabAlignment: TabAlignment.start,
                isScrollable: true,
                tabs: [
                  Text(
                    'All Products',
                    style: TextStyle(
                        fontFamily: 'JosefinSans',
                        color: constants.whiteColor,
                        fontSize: 18),
                  ),
                  Text(
                    'Swapping',
                    style: TextStyle(
                        fontFamily: 'JosefinSans',
                        color: constants.whiteColor,
                        fontSize: 18),
                  ),
                  Text(
                    'Re-Cycling',
                    style: TextStyle(
                        // decoration:,
                        fontFamily: 'JosefinSans',
                        color: constants.whiteColor,
                        fontSize: 18),
                  ),
                  Text(
                    'Donation',
                    style: TextStyle(
                        fontFamily: 'JosefinSans',
                        color: constants.whiteColor,
                        fontSize: 18),
                  ),
                  Text(
                    'Eco-Mart',
                    style: TextStyle(
                        fontFamily: 'JosefinSans',
                        color: constants.whiteColor,
                        fontSize: 18),
                  ),
                ]),
            actions: [
              SizedBox(
                width: 50,
              ),
              IconButton(
                  onPressed: () {
                    Get.to(() => SearchScreen());
                  },
                  icon: Icon(
                    CupertinoIcons.placemark,
                    color: constants.whiteColor,
                    size: 22,
                  )),
            ],
            centerTitle: true,
            iconTheme: IconThemeData(color: constants.whiteColor),
            title: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 40.0),
                child: Text(
                  'EcoSwap',
                  style: TextStyle(
                      color: constants.whiteColor,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'JosefinSans',
                      fontSize: constants.appbarFontSize),
                ),
              ),
            ),
            backgroundColor: constants.darkRedColor,
          ),
          body: WillPopScope(
              child: TabBarView(physics: BouncingScrollPhysics(), children: [
                HomeScreen(),
                SwappingScreen(),
                Re_CyclingScreen(),
                DonationScreen(),
                Eco_MarketScreen()
              ]),
              onWillPop: () async {
                SystemNavigator.pop();
                return false;
              })),
    );
  }
}
