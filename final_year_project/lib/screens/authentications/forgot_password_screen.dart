import 'package:final_year_project/core/Utils/constants/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/Utils/utils.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({super.key});

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  TextEditingController emailController = TextEditingController();
  // final ForgotPasswordController forgotPasswordController =
  //     Get.put(ForgotPasswordController());
  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Constants().darkRedColor,
        title: Text(
          'Forgot Password',
          style: TextStyle(
            color: Constants().whiteColor,
          ),
        ),
      ),
      body: Column(
        children: [
          // Lottie.asset('splash-icon.json'),
          // Lottie.asset("assets/splash-icon.json"),
          SizedBox(
            height: height * 0.1,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: TextFormField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              cursorColor: Constants().darkRedColor,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 2, left: 8),
                  hintText: 'Email',
                  prefixIcon: Icon(CupertinoIcons.mail),
                  border: OutlineInputBorder(
                      gapPadding: 10, borderRadius: BorderRadius.circular(10))),
            ),
          ),
          SizedBox(
            height: height * 0.03,
          ),
          Material(
            child: GestureDetector(
              onTap: () async {
                try {
                  String email = emailController.text.trim().toString();
                  if (email.isEmpty || email == '') {
                    Utils.snackBar('Error', "Please enter your email address");
                  } else {
                    final auth = FirebaseAuth.instance;
                    await auth.sendPasswordResetEmail(email: email).then((val) {
                      Utils.snackBar('Forgot Password',
                          'Link has sent to your gmail check your email');
                    }).onError((error, stackTrace) {
                      // EasyLoading.dismiss();

                      Utils.snackBar('Error', error.toString());
                    });
                    // forgotPasswordController.forgotPassword(email);
                  }
                } catch (e) {}
              },
              child: Container(
                height: height * 0.06,
                width: width * 0.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Constants().darkRedColor,
                ),
                child: Center(
                    child: Text(
                  'Forgot',
                  style: TextStyle(color: Constants().whiteColor),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
