import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/screens/authentications/forgot_password_screen.dart';
import 'package:final_year_project/services/firebase_services/Authentication/login_services.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import '../../resources/widgets/appbar.dart';
import '../../resources/widgets/customText.dart';
import '../../resources/widgets/textFormField.dart';
import '../../core/Utils/constants/colors.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Constants constants = Constants();
  LoginController controller = Get.put(LoginController());
  bool isValue = false;

  void susbmit() {
    try {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();
      }
      controller.login(
          emailController.text.toString(), passwordController.text.toString());
    } catch (e) {}
  }

  LogicsController logicsController = Get.put(LogicsController());
  // Logics logicsController = Get.put(logicsController());

  @override
  // @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;

    return Scaffold(
      backgroundColor: constants.whiteColor,
      appBar: CustomAppbar(
        fontFamily: 'JosefinSans',
        automaticallyImplyLeading: false,
        title: 'EcoSwap',
        fontsize: constants.appbarFontSize,
        fontWeight: FontWeight.bold,
        backgroundColor: constants.darkRedColor,
        textColor: constants.whiteColor,
      ),
      body: WillPopScope(onWillPop: () async {
        SystemNavigator.pop();
        return false;
      }, child: Obx(() {
        return ModalProgressHUD(
          inAsyncCall: controller.isLoading.value,
          color: constants.whiteColor,
          progressIndicator: CircularProgressIndicator(
            color: constants.darkRedColor,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: height * 0.3,
                      width: width,
                      child: Image.asset(
                        'assets/splash_logo.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                    Text(
                      'Login',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 33,
                          fontFamily: 'JosefinSans'),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomTextFormField(
                      fontFamily: 'JosefinSans',
                      controller: emailController,
                      prefixIcon: Icon(
                        Icons.person,
                        color: constants.darkRedColor,
                      ),
                      validator: (value) {
                        if (value == null ||
                            !value.contains("@") ||
                            value.trim().isEmpty) {
                          return "Please enter valid email";
                        } else {
                          return null;
                        }
                      },
                      hintText: 'Email',
                      labelText: 'Email',
                      filledColor: constants.textFormFieldFilledColor,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Obx(() {
                      return CustomTextFormField(
                        maxLinse: 1,
                        prefixIcon: Icon(
                          Icons.key_rounded,
                          color: constants.darkRedColor,
                        ),
                        fontFamily: 'JosefinSans',
                        obscureText: logicsController.isVissiblePassword.value,
                        controller: passwordController,
                        hintText: 'Password',
                        suffixIcon: InkWell(
                            onTap: () {
                              logicsController.isVissiblePassword.toggle();
                              // print(isValue.toString());
                              setState(() {});
                            },
                            child: logicsController.isVissiblePassword.value ==
                                    true
                                ? Icon(
                                    CupertinoIcons.eye_fill,
                                    color: constants.darkRedColor,
                                  )
                                : Icon(
                                    CupertinoIcons.eye_slash_fill,
                                    color: constants.darkRedColor,
                                  )),
                        labelText: 'Password',
                        validator: (value) {
                          if (value!.trim().isEmpty || value == '') {
                            return "Please enter password";
                          } else {
                            return null;
                          }
                        },
                      );
                    }),
                    SizedBox(
                      height: 15,
                    ),
                    CustomButton(
                        fontFamily: 'JosefinSans',
                        title: 'Login',
                        height: height * 0.06,
                        fontSize: 20,
                        width: width,
                        backGroundColor: constants.darkRedColor,
                        radius: 10,
                        buttonTextColor: constants.whiteColor,
                        onTap: susbmit),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        Get.to(() => ForgotPasswordScreen());
                      },
                      child: CustomText(
                        fontSize: 17,
                        alignmet: Alignment.centerRight,
                        title: 'Forgot Password?',
                        fontFamily: 'JosefinSans',
                        textDecoration: TextDecoration.underline,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          fontSize: 17,
                          title: 'Dont have an account?',
                          fontFamily: 'JosefinSans',
                        ),
                        TextButton(
                            onPressed: () {
                              Get.toNamed(RouteName.signupScreen);
                            },
                            child: Text(
                              'Signup',
                              style: TextStyle(fontFamily: 'JosefinSans'),
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      })),
    );
  }
  // Widget customAppbar(){}
}
