import 'dart:io';

import 'package:final_year_project/core/Utils/utils.dart';
import 'package:final_year_project/getx_logics/logics.dart';
import 'package:final_year_project/resources/widgets/image_picker.dart';
import 'package:final_year_project/services/firebase_services/Authentication/signup_services.dart';
import 'package:final_year_project/resources/widgets/roundButton.dart';
import 'package:final_year_project/resources/routes/routes_name.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import '../../resources/widgets/appbar.dart';
import '../../resources/widgets/customText.dart';
import '../../resources/widgets/textFormField.dart';
import '../../core/Utils/constants/colors.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  final FirebaseAuth auth = FirebaseAuth.instance;
  Constants constants = Constants();

  SignupControllers controller = Get.put(SignupControllers());
  final formKey = GlobalKey<FormState>();
  void checkValidateForm() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      return;
    } else {}
  }

  LogicsController logicsController = Get.put(LogicsController());
  File? imageFile;

  @override
  Widget build(BuildContext context) {
    double height = Get.height;
    double width = Get.width;
    return Scaffold(
        backgroundColor: constants.whiteColor,
        appBar: CustomAppbar(
          title: 'EcoSwap',
          automaticallyImplyLeading: false,
          fontsize: constants.appbarFontSize,
          fontFamily: 'JosefinSans',
          fontWeight: FontWeight.bold,
          backgroundColor: constants.darkRedColor,
          textColor: constants.whiteColor,
        ),
        body: WillPopScope(onWillPop: () async {
          SystemNavigator.pop();
          return false;
        }, child: Obx(() {
          return ModalProgressHUD(
            color: constants.whiteColor,
            progressIndicator: CircularProgressIndicator(
              color: constants.darkRedColor,
            ),
            inAsyncCall: controller.isLoading.value,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: SingleChildScrollView(
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            Image(
                              image: AssetImage(
                                'assets/signup.jpg',
                              ),
                              fit: BoxFit.fill,
                              width: width,
                              height: height * 0.28,
                            ),
                            Text(
                              'Sign up',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30,
                                  fontFamily: 'JosefinSans'),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            // Container(
                            //   // color: Colors.deepOrange,
                            //   height: height * 0.31,
                            //   child: ListView(children: [
                            CustomTextFormField(
                              prefixIcon: Icon(
                                Icons.person,
                                color: constants.darkRedColor,
                              ),
                              controller: nameController,
                              filledColor: constants.textFormFieldFilledColor,
                              hintText: 'Name',
                              fontFamily: 'JosefinSans',
                              labelText: 'Name',
                              validator: (value) {
                                if (value!.isEmpty || value == '') {
                                  return 'Name is required';
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CustomTextFormField(
                              controller: emailController,
                              hintText: 'Email',
                              prefixIcon: Icon(
                                Icons.email,
                                color: constants.darkRedColor,
                              ),
                              fontFamily: 'JosefinSans',
                              labelText: 'Email',
                              filledColor: constants.textFormFieldFilledColor,
                              validator: (value) {
                                if (value!.isEmpty || value == '') {
                                  return 'Email is required';
                                } else if (!value.contains("@")) {
                                  return 'Please enter valid email';
                                }
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CustomTextFormField(
                              fontFamily: 'JosefinSans',
                              controller: phoneNumberController,
                              prefixIcon: Icon(
                                Icons.phone,
                                color: constants.darkRedColor,
                              ),
                              filledColor: constants.textFormFieldFilledColor,
                              hintText: '03004512345',
                              labelText: 'Phone',
                              validator: (value) {
                                if (value!.isEmpty || value == '') {
                                  return 'Phone is required';
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CustomTextFormField(
                              controller: addressController,
                              filledColor: constants.textFormFieldFilledColor,
                              hintText: 'Address',
                              fontFamily: 'JosefinSans',
                              labelText: 'Address',
                              prefixIcon: Icon(
                                CupertinoIcons.location_solid,
                                color: constants.darkRedColor,
                              ),
                              validator: (value) {
                                if (value!.isEmpty || value == '') {
                                  return 'Address is required';
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Obx(() {
                              return CustomTextFormField(
                                obscureText:
                                    logicsController.isVissiblePassword.value,
                                maxLinse: 1,
                                fontFamily: 'JosefinSans',
                                controller: passwordController,
                                hintText: 'Password',
                                prefixIcon: Icon(
                                  Icons.key,
                                  color: constants.darkRedColor,
                                ),
                                suffixIcon: InkWell(
                                    onTap: () {
                                      logicsController.isVissiblePassword
                                          .toggle();
                                      // print(isValue.toString());
                                      setState(() {});
                                    },
                                    child: logicsController
                                                .isVissiblePassword.value ==
                                            true
                                        ? Icon(
                                            CupertinoIcons.eye_fill,
                                            color: constants.darkRedColor,
                                          )
                                        : Icon(
                                            CupertinoIcons.eye_slash_fill,
                                            color: constants.darkRedColor,
                                          )),
                                labelText: 'Password',
                                validator: (value) {
                                  if (value!.isEmpty || value == '') {
                                    return 'Password is required';
                                  } else {
                                    return null;
                                  }
                                },
                              );
                            }),
                            SizedBox(
                              height: 140,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: height * 0.76,
                    left: 15,
                    right: 15,
                    child: Column(
                      children: [
                        CustomButton(
                          title: 'Sign up',
                          height: height * 0.06,
                          fontFamily: 'JosefinSans',

                          fontSize: 20,
                          width: width * 0.95,
                          // isLoading: controller.isLoading.value,
                          backGroundColor: constants.darkRedColor,
                          radius: 10,
                          buttonTextColor: constants.whiteColor,
                          onTap: () {
                            checkValidateForm();
                            controller.signUp(
                                nameController.text.toString(),
                                emailController.text.toString(),
                                phoneNumberController.text.toString(),
                                addressController.text.toString(),
                                passwordController.text.toString());
                            print('object');
                          },
                        ),
                        Row(
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomText(
                              fontFamily: 'JosefinSans',
                              fontSize: 17,
                              title: 'Dont have an account?',
                            ),
                            TextButton(
                                onPressed: () {
                                  // Get.to(LoginScreen())ssss;
                                  Get.toNamed(RouteName.loginScreen);
                                },
                                child: Text(
                                  'Login',
                                  style: TextStyle(fontFamily: 'JosefinSans'),
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        })));
  }
  // Widget customAppbar(){}
}

/*
  PickImageWidget(
                              onPickedImage: (fileImage) async {
                                imageFile = fileImage;

                                final postReference = FirebaseDatabase.instance
                                    .ref("User/${auth!.uid}/profile");

                                firebase_storage.Reference reference =
                                    firebase_storage.FirebaseStorage.instance
                                        .ref("User/Profile" +
                                            "${auth!.uid.toString()}");
                                try {
                                  // controller.isLoading.value = true;
                                  firebase_storage.UploadTask uploadTask =
                                      reference.putFile(
                                          File(imageFile!.path.toString()));
                                  await Future.value(uploadTask)
                                      .then((value) {})
                                      .onError((error, stackTrace) {
                                    print(error.toString());

                                    Utils.snackBar(
                                        "Image", "${error.toString()}");
                                  });
                                  var newUrl = await reference.getDownloadURL();
                                  // controller.isLoading.value = true;
                                  var response = postReference.update({
                                    "image": newUrl.toString(),
                                  }).then((value) {
                                    // controller.isLoading.value = false;

                                    Utils.snackBar("Post",
                                        "Profile Picture update Successfully😍");
                                    // navigator!.pushReplacement(
                                    //     MaterialPageRoute(builder: (context) => MainScreen()));
                                  }).onError((error, stackTrace) {
                                    // controller.isLoading.value = false;
                                    print(error.toString());

                                    Utils.snackBar("Post", error.toString());
                                  });
                                } catch (e) {
                                  // controller.isLoading.value = false;
                                  print(e.toString());
                                  Utils.snackBar("Post", e.toString());
                                }
                                setState(() {});
                              },
                            ),
                       
*/

/*
 CustomButton(
                      title: 'Sign up',
                      height: height * 0.06,
                      fontFamily: 'JosefinSans',

                      fontSize: 20,
                      width: width,
                      // isLoading: controller.isLoading.value,
                      backGroundColor: constants.darkRedColor,
                      radius: 10,
                      buttonTextColor: constants.whiteColor,
                      onTap: () {
                        controller.signUp(
                            nameController.text.toString(),
                            emailController.text.toString(),
                            phoneNumberController.text.toString(),
                            addressController.text.toString(),
                            passwordController.text.toString());
                        print('object');
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          fontFamily: 'JosefinSans',
                          fontSize: 17,
                          title: 'Dont have an account?',
                        ),
                        TextButton(
                            onPressed: () {
                              // Get.to(LoginScreen())ssss;
                              Get.toNamed(RouteName.loginScreen);
                            },
                            child: Text(
                              'Login',
                              style: TextStyle(fontFamily: 'JosefinSans'),
                            ))
                      ],
                    )
*/
